#!/bin/sh
## zoomasm -- zoom video assembler
## (c) 2019,2020,2021,2022,2023 Claude Heiland-Allen
## SPDX-License-Identifier: AGPL-3.0-only
set -e
make zoomasm-source.7z
VERSION="$(cat VERSION.txt | head -n 1)"
DATE="$(cat VERSION.txt | tail -n+2 | head -n 1)"
rm -rf "release/${VERSION}"
mkdir -p "release/${VERSION}"
cd "release/${VERSION}"
7zr x ../../zoomasm-source.7z
ln -s ../../../imgui
ln -s ../../../imgui-filebrowser
ln -s ../../../tomlplusplus
cd "zoomasm-${VERSION}"
make SYSTEM=windows-i686 -j$(nproc)
cp -avf build/windows-i686/zoomasm.i686.exe ..
make SYSTEM=windows-x86_64 -j$(nproc)
cp -avf build/windows-x86_64/zoomasm.x86_64.exe ..
make SYSTEM=posix -j$(nproc)
cp -avf build/posix/zoomasm ..
mv zoomasm-source.7z "../zoomasm-${VERSION}.7z"
cd ..
strip --strip-unneeded zoomasm.i686.exe
strip --strip-unneeded zoomasm.x86_64.exe
strip --strip-unneeded zoomasm
cp -avf "zoomasm-${VERSION}/doc/"* .
pandoc zoomasm.md --metadata="title=zoomasm-${VERSION}" --metadata="date=${DATE}" -o "zoomasm-${VERSION}.pdf"
pandoc zoomasm.md --metadata="title=zoomasm-${VERSION}" --metadata="date=${DATE}" --standalone -c "zoomasm.css" --toc -o "index.html"
rm zoomasm.md
sed -i "s/href=.zoomasm.css./href='zoomasm-${VERSION}\\/zoomasm.css'/g" index.html
sed -i "s/img src=.\([^'\"]*\)./img src='zoomasm-${VERSION}\\/\1'/g" index.html
gzip -9 -k -f zoomasm.css
mv index.html "zoomasm-${VERSION}.html"
gzip -9 -k -f "zoomasm-${VERSION}.html"
mkdir "zoomasm-${VERSION}-win"
cp -avf zoomasm.i686.exe "zoomasm-${VERSION}-win/zoomasm-${VERSION}.i686.exe"
cp -avf zoomasm.x86_64.exe "zoomasm-${VERSION}-win/zoomasm-${VERSION}.x86_64.exe"
cp -avf "zoomasm-${VERSION}.pdf" "zoomasm-${VERSION}-win/zoomasm-${VERSION}.pdf"
cp -avf "zoomasm-${VERSION}/presets" "zoomasm-${VERSION}-win/presets"
7zr a -t7z -m0=lzma -mx=9 -mfb=64 -md=32m -ms=on "zoomasm-${VERSION}-win.7z" "zoomasm-${VERSION}-win/"
rm -rf "zoomasm-${VERSION}" "zoomasm-${VERSION}-win" zoomasm.i686.exe zoomasm.x86_64.exe
mkdir "zoomasm-${VERSION}"
mv *.png *.css* "zoomasm-${VERSION}"
rm -f imgui imgui-filebrowser tomlplusplus
gpg -u zoomasm -b "zoomasm-${VERSION}.7z"
gpg -u zoomasm -b "zoomasm-${VERSION}-win.7z"
ls -1sh
cd ../..
