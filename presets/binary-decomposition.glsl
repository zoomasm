// zoomasm -- zoom video assembler
// (c) 2019,2020,2021,2022,2023 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-only

// recommended KF bailout settings: linear smoothing, custom radius 25
vec3 colour(void)
{
  if (getInterior())
  {
    return vec3(1.0, 0.0, 0.0);
  }
  bool decomp = getT() < 0.5;
  return vec3(decomp ? 0.0 : 1.0);
}
