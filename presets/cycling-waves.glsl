// zoomasm -- zoom video assembler
// (c) 2019,2020,2021,2022,2023 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-only

float wave(float p)
{
  return 0.5 + 0.5 * cos(6.283185307179586 * p);
}

vec3 colour(void)
{
  float time = getTime();
  if (getInterior())
  {
    return vec3(0.0);
  }
  float49 NNF = getN();
  // colour cycling
  float C0 = mod(time * 48000.0 / 20197.0 / 128.0, 1.0);
  float C1 = mod(time * 48000.0 / 20197.0 / 512.0, 1.0);
  float C2 = mod(time * 48000.0 / 20197.0 / 256.0, 1.0);
  float C3 = mod(time * 48000.0 / 20197.0 /   4.0, 1.0);
  // repeating cycles
  float N0 = to_float(wrap(div(floor(add(NNF, C0 * 257.0)), 257.0)));
  float N1 = to_float(wrap(div(floor(add(NNF, C1 * 126.0)), 126.0)));
  float N2 = to_float(wrap(div(floor(add(NNF, C2 *  26.0)),  26.0)));
  float N3 = to_float(wrap(div(floor(add(NNF, C3 *   5.0)),   5.0)));
  // base colour
  vec3 b = getRGB();
  // infinite waves colour
  vec3 w = hsv2rgb(vec3
    ( wave(N1)
    , wave(N2)
    , wave(N3)
    ));
  // blend
  vec3 h = mix(b, w, 0.5);
  // slopes
  vec2 DE = getDE();
  float slope = dot(normalize(DE), vec2(1.0, 1.0));
  float strength = abs(slope) / (1.0 + length(DE));
  if (slope < 0.0) h = mix(h, vec3(0.0), vec3(clamp(strength, 0.0, 1.0)));
  if (slope > 0.0) h = mix(h, vec3(1.0), vec3(clamp(strength, 0.0, 1.0)));
  return h;
}
