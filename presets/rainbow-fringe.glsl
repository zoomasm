// zoomasm -- zoom video assembler
// (c) 2019,2020,2021,2022,2023 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-only

// black on white with a rainbow fringe
vec3 colour(void)
{
  vec2 DE = getDE();
  float hue = atan(DE.y, DE.x) / 6.283185307179586;
  hue -= floor(hue);
  float sat = clamp(1.0 / (1.0 + length(DE)), 0.0, 1.0);
  float val = clamp(length(DE), 0.0, 1.0);
  return hsv2rgb(vec3(hue, sat, val));
}
