// zoomasm -- zoom video assembler
// (c) 2019,2020,2021,2022,2023 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-only

uniform float Gamma;

uniform float WarpDepth;
uniform float WarpSpeed;
uniform float WarpFreq;

vec3 colour(void)
{
  float Time = getTime();
  vec2 Coord = getCoord();
  Coord -= ImageSize / 2.0;
  float Distance = length(Coord) / length(ImageSize / 2.0);
  float Warp = pow(2.0, WarpDepth * sin(2.0 * pi *
    (WarpSpeed * Time + WarpFreq * Distance)));
  Coord *= Warp;
  Coord += ImageSize / 2.0;
  vec2 DE = getDEAt(Coord);
  return vec3(pow(clamp(length(DE), 0.0, 1.0), 1.0 / Gamma));
}
