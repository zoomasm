---
author: Claude Heiland-Allen
toc: true
geometry:
- margin=1in
...

# *zoomasm*

Zoom videos are a genre of 2D fractal animation. The rendering of the
final video can be accelerated by computing exponentially spaced rings
around the zoom center, before reprojecting to a sequence of flat
images.

Some fractal software supports rendering EXR keyframes in exponential
map form, which *zoomasm* can assemble into a zoom video.  *zoomasm*
works from EXR, including raw iteration data, and colouring algorithms
can be written in OpenGL shader source code fragments.

Home: <https://mathr.co.uk/zoomasm>

Code: <https://code.mathr.co.uk/zoomasm>

Support: <mailto:zoomasm@mathr.co.uk?subject=zoomasm>

Donate: <https://ko-fi.com/claudeha>

See [doc/zoomasm.md](doc/zoomasm.md) for documentation.

---
<https://mathr.co.uk>
