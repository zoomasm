#pragma once
// zoomasm -- zoom video assembler
// (c) 2019,2020,2021,2022,2023 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-only

#include <filesystem>
#include <set>
#include <string>

struct input;

extern struct input *input_new(void);
extern void input_delete(struct input *in);

extern int input_set_phase(struct input *in, double phase, double speed);

extern int input_get_count(const struct input *in);
extern int input_get_width(const struct input *in);
extern int input_get_height(const struct input *in);
extern double input_get_invert_radius(const struct input *in);

extern void input_display_early(struct input *in);
extern void input_display_late(struct input *in);

extern bool input_set_input(struct input *in, const std::filesystem::path &dir);
extern std::filesystem::path input_get_input(const struct input *in);

extern bool input_set_wanted_channels(struct input *in, const std::set<std::string> &channels);
extern std::set<std::string> input_get_wanted_channels(const struct input *in);

extern bool input_set_vram_override(struct input *in, bool vram_override);
extern bool input_get_vram_override(const struct input *in);

extern bool input_set_reverse(struct input *in, bool reverse);
extern bool input_get_reverse(const struct input *in);

extern bool input_set_flip(struct input *in, bool flip);
extern bool input_get_flip(const struct input *in);

extern bool input_set_invert(struct input *in, bool invert);
extern bool input_get_invert(const struct input *in);

extern bool input_set_layers(struct input *in, int layers);
extern int input_get_layers(const struct input *in);
