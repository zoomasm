#pragma once
// zoomasm -- zoom video assembler
// (c) 2019,2020,2021,2022,2023 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-only

#include <set>
#include <string>
#include <iostream>

extern bool starts_with(const std::string &haystack, const std::string &needle);
extern bool ends_with(const std::string &haystack, const std::string &needle);

extern bool contains(const std::set<std::string> &haystack, const std::string &needle);
extern std::set<std::string> intersection(const std::set<std::string> &s1, const std::set<std::string> &s2);

extern char *mystrdup(const char* s);

extern std::istream& getline(std::istream& is, std::string& t);

extern void seconds_to_timespec(double time, int timespec[4]);
extern double timespec_to_seconds(const int timespec[4]);
extern std::string format_timespec(const int timespec[4]);
extern std::string format_time(double time);

extern double get_time();
