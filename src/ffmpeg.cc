// zoomasm -- zoom video assembler
// (c) 2019,2020,2021,2022,2023 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-only

#include "ffmpeg.h"

#include <thread>
#include <mutex>
#include <cstring>
#include <cmath>
#include <fstream>
#include <iomanip>

#include "imgui.h"

#include "process.h"
#include "resource.h"
#include "utility.h"

struct ffmpeg
{
  // configuration
  std::filesystem::path ffexe;
  enum ffmpeg_format format;
  int width;
  int height;
  double fps;
  // simple mode
  int video_crf;
  double audio_bitrate;
  bool overwrite;
  std::filesystem::path output;
  bool output_exists;
  // advanced mode
  bool advanced;
  bool twopass;
  std::string pass_1;
  std::string pass_2;
  // detected ffmpeg version
  std::string version;
  // output stuff
  char *header_data;
  ssize_t header_bytes;
  ssize_t frame_bytes;
  // image sequence
  bool recording;
  int frame;
  // subprocess communication
  struct process *proc;
  std::mutex err_mutex;
  bool err_scroll;
  ImGuiTextBuffer err, err_lastline[2];
  std::thread err_reader[2];
};

static void ffmpeg_reader(struct ffmpeg *enc, int which)
{
  bool running = true;
  bool cr = false;
  while (running)
  {
    if (enc->proc)
    {
      char buffer[1 + 1];
      // blocking IO
      ssize_t read_bytes = (which ? process_stderr : process_stdout)
        (enc->proc, buffer, sizeof buffer - 1);
      if (0 <= read_bytes && read_bytes < (ssize_t) sizeof buffer)
      {
        buffer[read_bytes] = 0;
        enc->err_mutex.lock();
        if (buffer[0] == '\r')
        {
          cr = true;
        }
        else if (buffer[0] == '\n')
        {
          enc->err_lastline[which].append(buffer);
          enc->err.append(enc->err_lastline[which].begin(), enc->err_lastline[which].end());
          enc->err_lastline[which].clear();
          enc->err_scroll = true;
          cr = false;
        }
        else
        {
          if (cr)
          {
            enc->err_lastline[which].clear();
          }
          enc->err_lastline[which].append(buffer);
          cr = false;
        }
        enc->err_mutex.unlock();
      }
      else
      {
        running = false;
      }
    }
    else
    {
      running = false;
    }
  }
}

extern struct ffmpeg *ffmpeg_new(void)
{
  struct ffmpeg *enc = new ffmpeg();
#ifdef _WIN32
  ffmpeg_set_ffmpeg(enc, "ffmpeg.exe");
#else
  ffmpeg_set_ffmpeg(enc, "ffmpeg");
#endif
  enc->format = ffmpeg_format_srgb8;
  // dimensions must be different from default in output.cc, so that it reallocates the PBOs
  enc->width = 640;
  enc->height = 360;
  enc->fps = 25;
  enc->video_crf = 20;
  enc->audio_bitrate = 384;
  enc->overwrite = false;
  return enc;
}

extern void ffmpeg_delete(struct ffmpeg *enc)
{
  if (! enc)
  {
    return;
  }
  if (enc->proc)
  {
    ffmpeg_stop(enc);
  }
  delete enc;
}

extern bool ffmpeg_set_ffmpeg(struct ffmpeg *enc, const std::filesystem::path &ffexe)
{
  if (enc)
  {
    if (! enc->proc)
    {
      char ffversion[4096 + 1];
      struct process *ffver = process_open({ ffexe.string(), "-version" });
      if (ffver)
      {
        ssize_t bytes = process_stdout(ffver, ffversion, sizeof ffversion - 1);
        process_close(ffver);
        if (0 <= bytes && bytes < (ssize_t) sizeof ffversion)
        {
          ffversion[bytes] = 0;
          char *newline = std::strchr(ffversion, '\n');
          if (newline)
          {
            *newline = 0;
          }
          std::string version = ffversion;
          if (starts_with(version, "ffmpeg version "))
          {
            enc->version = version;
            enc->ffexe = ffexe;
            return true;
          }
        }
      }
    }
  }
  return false;
}

extern bool ffmpeg_set_fps(struct ffmpeg *enc, double fps)
{
  if (enc)
  {
    if (! enc->proc)
    {
      if (0 < fps)
      {
        changed |= (fps != enc->fps);
        enc->fps = fps;
        return true;
      }
    }
  }
  return false;
}

extern bool ffmpeg_set_format(struct ffmpeg *enc, enum ffmpeg_format format)
{
  if (enc)
  {
    if (! enc->proc)
    {
      if (ffmpeg_format_srgb8 == format)
      {
        enc->format = format;
        return true;
      }
    }
  }
  return false;
}

extern bool ffmpeg_set_width(struct ffmpeg *enc, int width)
{
  if (enc)
  {
    if (! enc->proc)
    {
      if (0 < width)
      {
        changed |= (width != enc->width);
        enc->width = width;
        return true;
      }
    }
  }
  return false;
}

extern bool ffmpeg_set_height(struct ffmpeg *enc, int height)
{
  if (enc)
  {
    if (! enc->proc)
    {
      if (0 < height)
      {
        changed |= (height != enc->height);
        enc->height = height;
        return true;
      }
    }
  }
  return false;
}

extern bool ffmpeg_set_overwrite(struct ffmpeg *enc, bool overwrite)
{
  if (enc)
  {
    if (! enc->proc)
    {
      enc->overwrite = overwrite;
      return true;
    }
  }
  return false;
}

extern std::filesystem::path ffmpeg_image_filename(const struct ffmpeg *enc, int frame)
{
  const int digits = 8; // FIXME hardcoded
  std::ostringstream number;
  number << std::setw(digits) << std::setfill('0') << frame;
  std::filesystem::path filename = enc->output.stem();
  filename += number.str();
  filename += enc->output.extension();
  std::filesystem::path outfile = enc->output;
  outfile.replace_filename(filename);
  return outfile;
}

extern bool ffmpeg_set_output(struct ffmpeg *enc, const std::filesystem::path &output)
{
  if (enc)
  {
    if (! enc->proc)
    {
      enc->output = output;
      if (output.extension().string() == ".ppm")
      {
        enc->output_exists = std::filesystem::exists(ffmpeg_image_filename(enc, 0));
      }
      else
      {
        enc->output_exists = std::filesystem::exists(output);
      }
      return true;
    }
  }
  return false;
}

extern bool ffmpeg_set_video_crf(struct ffmpeg *enc, int crf)
{
  if (enc)
  {
    if (! enc->proc)
    {
      enc->video_crf = std::min(std::max(0, crf), 51);
      return true;
    }
  }
  return false;
}

extern bool ffmpeg_set_audio_kbps(struct ffmpeg *enc, double kbps)
{
  if (enc)
  {
    if (! enc->proc)
    {
      enc->audio_bitrate = kbps;
      return true;
    }
  }
  return false;
}

extern std::filesystem::path ffmpeg_get_ffmpeg(const struct ffmpeg *enc)
{
  return enc->ffexe;
}

extern std::string ffmpeg_get_version(const struct ffmpeg *enc)
{
  return enc->version;
}

extern enum ffmpeg_format ffmpeg_get_format(const struct ffmpeg *enc)
{
  return enc->format;
}

extern int ffmpeg_get_width(const struct ffmpeg *enc)
{
  return enc->width;
}

extern int ffmpeg_get_height(const struct ffmpeg *enc)
{
  return enc->height;
}

extern double ffmpeg_get_fps(const struct ffmpeg *enc)
{
  return enc->fps;
}

extern int ffmpeg_get_video_crf(const struct ffmpeg *enc)
{
  return enc->video_crf;
}

extern double ffmpeg_get_audio_kbps(const struct ffmpeg *enc)
{
  return enc->audio_bitrate;
}

extern bool ffmpeg_get_overwrite(const struct ffmpeg *enc)
{
  return enc->overwrite;
}

extern std::filesystem::path ffmpeg_get_output(const struct ffmpeg *enc)
{
  return enc->output;
}

extern bool ffmpeg_get_output_exists(const struct ffmpeg *enc)
{
  // may be stale if something else created it behind our back
  return enc->output_exists;
}

extern bool ffmpeg_set_advanced(struct ffmpeg *enc, bool advanced)
{
  if (enc)
  {
    enc->advanced = advanced;
    return true;
  }
  return false;
}

extern bool ffmpeg_set_twopass(struct ffmpeg *enc, bool twopass)
{
  if (enc)
  {
    enc->twopass = twopass;
    return true;
  }
  return false;
}

extern bool ffmpeg_set_pass_1(struct ffmpeg *enc, const std::string &pass1)
{
  enc->pass_1 = pass1;
  return true;
}

extern bool ffmpeg_set_pass_2(struct ffmpeg *enc, const std::string &pass2)
{
  enc->pass_2 = pass2;
  return true;
}

extern bool ffmpeg_get_advanced(const struct ffmpeg *enc)
{
  return enc->advanced;
}

extern bool ffmpeg_get_twopass(const struct ffmpeg *enc)
{
  return enc->twopass;
}

extern std::string *ffmpeg_get_pass_1(struct ffmpeg *enc)
{
  return &enc->pass_1;
}

extern std::string *ffmpeg_get_pass_2(struct ffmpeg *enc)
{
  return &enc->pass_2;
}

extern bool ffmpeg_ready(const ffmpeg *enc)
{
  if (enc)
  {
    if (! enc->proc)
    {
      if (ffmpeg_format_srgb8 == enc->format)
      {
        if (0 < enc->width)
        {
          if (0 < enc->height)
          {
            if (0 < enc->fps)
            {
              if (enc->advanced)
              {
                // assume the user knows what they are doing
                return true;
              }
              else
              {
                if (starts_with(enc->version, "ffmpeg version ") ||
                    ends_with(enc->output.string(), ".ppm"))
                {
                  if (ends_with(enc->output.string(), ".mp4") ||
                      ends_with(enc->output.string(), ".mkv") ||
                      ends_with(enc->output.string(), ".mov") ||
                      ends_with(enc->output.string(), ".ppm"))
                  {
                    if (enc->overwrite || ! ffmpeg_get_output_exists(enc))
                    {
                      return true;
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  return false;
}

extern bool ffmpeg_recording(const struct ffmpeg *enc)
{
  if (enc)
  {
    if (enc->proc)
    {
      return true;
    }
    else
    {
      return enc->recording;
    }
  }
  return false;
}

// https://en.wikipedia.org/wiki/Advanced_Video_Coding#Levels
struct avc_level
{
  std::string name;
  int speed; // macroblocks/s
  int size; // macroblocks
  int bitrate; // kbits/s
};
#define AVC_LEVELS 20
static const struct avc_level avc_levels[AVC_LEVELS] =
{ { "1", 1485, 99, 64 }
, { "1b", 1485, 99, 128 }
, { "1.1", 3000, 396, 192 }
, { "1.2", 6000, 396, 384 }
, { "1.3", 11880, 396, 768 }
, { "2", 11880, 396, 2000 }
, { "2.1", 19800, 792, 4000 }
, { "2.2", 20250, 1620, 4000 }
, { "3", 40500, 1620, 10000 }
, { "3.1", 108000, 3600, 14000 }
, { "3.2", 216000, 5120, 20000 }
, { "4", 245760, 8192, 20000 }
, { "4.1", 245760, 8192, 50000 }
, { "4.2", 522240, 8704, 50000 }
, { "5", 589824, 22080, 135000 }
, { "5.1", 983040, 36864, 240000 }
, { "5.2", 2073600, 36864, 240000 }
, { "6", 4177920, 139264, 240000 }
, { "6.1", 8355840, 139264, 480000 }
, { "6.2", 16711680, 139264, 800000 }
};

static std::string ffmpeg_choose_level(int width, int height, double fps)
{
  int size = (width + 15) / 16 * (height + 15) / 16;
  int speed = std::ceil(size * fps);
  for (int level = 0; level < AVC_LEVELS; ++level)
  {
    if (avc_levels[level].speed >= speed && avc_levels[level].size >= size)
    {
      return avc_levels[level].name;
    }
  }
  std::fprintf(stderr, "FFMPEG ERROR: could not find level for %dx%dp%.3f", width, height, fps);
  return avc_levels[AVC_LEVELS - 1].name; // FIXME
}

extern int ffmpeg_record(struct ffmpeg *enc, const std::filesystem::path &soundtrack_file, int pass)
{
  bool image_sequence = ends_with(enc->output.string(), ".ppm");
  if (ffmpeg_ready(enc) || image_sequence)
  {
    if (enc->header_data)
    {
      std::free(enc->header_data);
      enc->header_data = nullptr;
    }
    std::ostringstream s;
    s << "P6\n" << enc->width << " " << enc->height << "\n" << 255 << "\n";
    enc->header_data = mystrdup(s.str().c_str());
    if (enc->header_data)
    {
      enc->header_bytes = strlen(enc->header_data);
      enc->frame_bytes = 0;
      switch (enc->format)
      {
        case ffmpeg_format_srgb8:
          enc->frame_bytes = (ssize_t) 3 * enc->width * enc->height;
          break;
      }
      if (image_sequence)
      {
        enc->err_mutex.lock();
        enc->err.clear();
        enc->err_lastline[0].clear();
        enc->err_lastline[1].clear();
        enc->err_mutex.unlock();
        enc->recording = true;
      }
      else
      {
        std::vector<std::string> cmd = { enc->ffexe.string() };
        if (enc->advanced)
        {
          std::string arguments;
          if (pass == 1)
          {
            arguments = enc->pass_1;
          }
          else
          {
            arguments = enc->pass_2;
          }
          std::stringstream ss(arguments);
          std::string word;
          while (std::getline(ss, word, ' ')) // FIXME escape?
          {
            cmd.push_back(word);
          }
        }
        else
        {
          std::ostringstream fps;
          fps << enc->fps;
          std::ostringstream crf;
          crf << enc->video_crf;
          cmd.push_back("-framerate");
          cmd.push_back(fps.str());
          cmd.push_back("-i");
          cmd.push_back("-");
          if (! soundtrack_file.empty())
          {
            cmd.push_back("-i");
            cmd.push_back(soundtrack_file.string());
          }
          if (enc->video_crf > 0)
          {
            cmd.push_back("-pix_fmt");
            cmd.push_back("yuv420p");
            cmd.push_back("-profile:v");
            cmd.push_back("high");
            cmd.push_back("-level:v");
            cmd.push_back(ffmpeg_choose_level(enc->width, enc->height, enc->fps));
            cmd.push_back("-crf:v");
            cmd.push_back(crf.str());
          }
          else
          {
            // lossless, may be incompatible with everything but ffmpeg
            cmd.push_back("-crf:v");
            cmd.push_back("0");
          }
          if (! soundtrack_file.empty())
          {
            if (enc->audio_bitrate > 0)
            {
              std::ostringstream kbps;
              kbps << enc->audio_bitrate << "k";
              cmd.push_back("-b:a");
              cmd.push_back(kbps.str());
            }
            else
            {
              cmd.push_back("-codec:a");
              cmd.push_back("copy");
            }
            // avoid including whole soundtrack if recording is stopped prematurely
            cmd.push_back("-shortest");
          }
          // optimize for streaming
          cmd.push_back("-movflags");
          cmd.push_back("+faststart");
          if (enc->overwrite)
          {
            cmd.push_back("-y");
          }
          cmd.push_back(enc->output.string());
        }
        enc->proc = process_open(cmd);
        if (enc->proc)
        {
          enc->err_mutex.lock();
          enc->err.clear();
          // add command line
          enc->err.append("encoder:");
          for (auto arg : cmd)
          {
            enc->err.append(" ");
            enc->err.append(arg.c_str());
          }
          enc->err.append("\n");
          enc->err_lastline[0].clear();
          enc->err_lastline[1].clear();
          enc->err_mutex.unlock();
          enc->err_reader[0] = std::thread(ffmpeg_reader, enc, 0);
          enc->err_reader[1] = std::thread(ffmpeg_reader, enc, 1);
          return 0;
        }
      }
    }
  }
  return 1;
}

extern int ffmpeg_stop(struct ffmpeg *enc)
{
  if (enc)
  {
    if (enc->proc) // PPM stream to subprocess
    {
      process_close(enc->proc);
      enc->proc = nullptr;
      enc->err_reader[0].join();
      enc->err_reader[1].join();
      return 0;
    }
    else // image file sequence
    {
      enc->recording = false;
      return 0;
    }
  }
  return 1;
}

extern int ffmpeg_write(struct ffmpeg *enc, const void *data, ssize_t bytes)
{
  if (enc)
  {
    if (enc->proc) // PPM stream to subprocess
    {
      if (enc->frame_bytes == bytes)
      {
        if (enc->header_bytes == process_stdin(enc->proc, enc->header_data, enc->header_bytes))
        {
          if (bytes == process_stdin(enc->proc, data, bytes))
          {
            return 0;
          }
        }
      }
    }
    else // PPM image file sequence
    {
      std::filesystem::path outfile = ffmpeg_image_filename(enc, enc->frame++);
      enc->err_mutex.lock();
      enc->err_lastline[0].clear();
      enc->err_lastline[0].append("FRAME: ");
      enc->err_lastline[0].append(outfile.string().c_str());
      enc->err_lastline[0].append(" ...");
      enc->err_mutex.unlock();
      if (! enc->overwrite && std::filesystem::exists(outfile))
      {
        enc->err_mutex.lock();
        enc->err.append(enc->err_lastline[0].begin(), enc->err_lastline[0].end());
        enc->err.append("\n");
        enc->err.append("ERROR: file exists: ");
        enc->err.append(outfile.string().c_str());
        enc->err.append("\n");
        enc->err_mutex.unlock();
        return 1;
      }
      std::ofstream out(outfile, std::ios::out | std::ios::binary);
      if (! out)
      {
        enc->err_mutex.lock();
        enc->err.append(enc->err_lastline[0].begin(), enc->err_lastline[0].end());
        enc->err.append("\n");
        enc->err.append("ERROR: open failed: ");
        enc->err.append(outfile.string().c_str());
        enc->err.append("\n");
        enc->err_mutex.unlock();
        return 1;
      }
      out.write(enc->header_data, enc->header_bytes);
      out.write((const char *) data, bytes);
      out.close();
      if (! out.good())
      {
        enc->err_mutex.lock();
        enc->err.append(enc->err_lastline[0].begin(), enc->err_lastline[0].end());
        enc->err.append("\n");
        enc->err.append("ERROR: write failed: ");
        enc->err.append(outfile.string().c_str());
        enc->err.append("\n");
        enc->err_mutex.unlock();
        return 1;
      }
      enc->err_mutex.lock();
      enc->err_lastline[0].append(" OK");
      enc->err_mutex.unlock();
      return 0;
    }
  }
  return 1;
}

extern void ffmpeg_display(struct ffmpeg *enc, bool embedded)
{
  enc->err_mutex.lock();
  if (embedded)
  {
    ImGui::BeginChild("Encoder", ImVec2(480, 270), false, ImGuiWindowFlags_HorizontalScrollbar);
  }
  else
  {
    ImGui::Begin("Encoder", nullptr, ImGuiWindowFlags_HorizontalScrollbar);
  }
  ImGui::TextUnformatted(enc->err.begin(), enc->err.end());
  ImGui::TextUnformatted(enc->err_lastline[0].begin(), enc->err_lastline[0].end());
  ImGui::TextUnformatted(enc->err_lastline[1].begin(), enc->err_lastline[1].end());
  if (enc->err_scroll)
  {
    ImGui::SetScrollHereY(1.0f);
    enc->err_scroll = false;
  }
  if (embedded)
  {
    ImGui::EndChild();
  }
  else
  {
    ImGui::End();
  }
  enc->err_mutex.unlock();
}
