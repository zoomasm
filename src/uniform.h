#pragma once
// zoomasm -- zoom video assembler
// (c) 2021,2022,2023 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-only

#include <map>
#include <string>
#include <vector>

#include <GL/glew.h>

#include <fstream>
#include "toml.hpp"

struct audio;

struct uniforms;

extern struct uniforms *uniforms_new(void);
extern void uniforms_delete(struct uniforms *u);

extern void uniforms_set_program(struct uniforms *u, GLuint program);
extern void uniforms_set_keyframes(struct uniforms *u, int keyframes);

extern double uniforms_get_z(const struct uniforms *u, double fraction);

extern void uniforms_upload(const struct uniforms *u);

extern bool uniforms_display(struct uniforms *u, struct audio *au, bool show_gui);
extern void uniforms_display_late(struct uniforms *u, double fps, double duration);

extern toml::array uniforms_save_session(const struct uniforms *u);
extern bool uniforms_load_session(struct uniforms *u, const toml::array &r);
