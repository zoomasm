#pragma once
// zoomasm -- zoom video assembler
// (c) 2019,2020,2021,2022,2023 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-only

#include "imgui.h"

enum TEXTURE_UNIT {
  TEXTURE_OUTPUT_RGB = 12,
  TEXTURE_OUTPUT_YUV = 1,
  TEXTURE_R = 2,
  TEXTURE_G = 3,
  TEXTURE_B = 4,
  TEXTURE_N0 = 5,
  TEXTURE_N1 = 6,
  TEXTURE_NF = 7,
  TEXTURE_DEX = 8,
  TEXTURE_DEY = 9,
  TEXTURE_T = 10,
  TEXTURE_PALETTE = 11
};

const ImVec4 button_red = { 0.5f, 0.1f, 0.1f, 1.0f };

#define projection_rectangular2d 0
#define projection_equirectangular360 1

extern bool changed;
