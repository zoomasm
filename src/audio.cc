// zoomasm -- zoom video assembler
// (c) 2019,2020,2021,2022,2023 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-only

#include "audio.h"

#include "imgui.h"
#include "imfilebrowser.h"

#include <cmath>
#include <cstring>
#include <complex>
#include <filesystem>
#include <thread>

#include <fftw3.h>
#include <SDL2/SDL.h>

#include "resource.h"
#include "ringbuffer.h"

#define AUDIO_CHANNELS_MAX 8

static inline float dbfstorms(float dbfs)
{
  return std::pow(10.0f, dbfs / 10.0f);
}

static inline float rmstodbfs(float rms)
{
  return 10.0f * std::log10(rms);
}

static inline std::complex<double> normalize(std::complex<double> x)
{
  x /= std::abs(x);
  if (std::isnan(std::real(x)) || std::isnan(std::imag(x)) || std::isinf(std::real(x)) || std::isinf(std::imag(x)))
  {
    return 1;
  }
  return x;
}

static inline std::complex<double> to_std_complex(fftw_complex x)
{
  return std::complex<double>(x[0], x[1]);
}

static inline void to_fftw_complex(std::complex<double> x, fftw_complex y)
{
  y[0] = std::real(x);
  y[1] = std::imag(x);
}

// requests from audio callback to decoder

enum request_type
{
  request_type_time
};

// player is playing at this timestamp (informational)
struct request_time
{
  double time;
};

struct request
{
  enum request_type type;
  union
  {
    struct request_time time;
  } u;
};

// responses from decoder to audio callback

enum response_type
{
  response_type_volume,
  response_type_speed,
  response_type_time,
  response_type_loop,
  response_type_quit
};

// set output volume [0..1]
struct response_volume
{
  double volume;
};

// set playback speed [-10..10] (too fast might glitch)
struct response_speed
{
  double speed;
};

// set playback time (manual seek, might not be instant)
struct response_time
{
  double time;
};

// set playback looping
// FIXME TODO sample-accurate seamless looping
struct response_loop
{
  bool loop;
};

struct response
{
  enum response_type type;
  union
  {
    struct response_volume volume;
    struct response_speed speed;
    struct response_time time;
    struct response_loop loop;
  } u;
};

#define AUDIO_OVERLAP 4
#define AUDIO_HOP_SIZE 256
#define AUDIO_FFT_SIZE 1024

struct audio_rt
{
  ringbuffer<request, 256> *requests;
  ringbuffer<response, 256> *responses;
  int channels; // 0 for silence (playhead scheduler only)
  float *data; // audio data (interleaved)
  uint64_t duration; // frames
  std::thread *thread;
  double sample_rate;
  double time; // seconds
  double speed;
  bool loop;
  double volume_start;
  double volume_target;
  double volume_ramp;
  double *window; // [AUDIO_FFT_SIZE]
  double *fft_in; // [AUDIO_FFT_SIZE]
  fftw_complex *fft_out[2]; // [AUDIO_FFT_SIZE]
  fftw_complex **ifft_in; // [channels][AUDIO_FFT_SIZE]
  double *ifft_out; // [AUDIO_FFT_SIZE]
  float *outbuf; // [channels * AUDIO_HOP_SIZE * AUDIO_OVERLAP]
  fftw_plan fft[2];
  fftw_plan *ifft; // [channels]
  int frame; // [0..AUDIO_HOP_SIZE]
  uint64_t errs_request_overrun;
  uint64_t errs_response_overrun;

};

static int audio_rt_response_quit(struct audio_rt *art);
static void audio_rt_silence(struct audio_rt *art);

static void audio_rt_uninit(struct audio_rt *art)
{
  if (! art)
  {
    return;
  }
  if (art->thread)
  {
    // signal quit, join thread
    audio_rt_response_quit(art);
    art->thread->join();
    delete art->thread;
    art->thread = nullptr;
  }
  if (art->requests)
  {
    delete art->requests;
    art->requests = nullptr;
  }
  if (art->responses)
  {
    delete art->responses;
    art->responses = nullptr;
  }
  if (art->data)
  {
    SDL_free(art->data);
    art->data = nullptr;
  }
#define F(P) \
  if (art->P) \
  { \
    fftw_free(art->P); \
    art->P = nullptr; \
  }
  F(window)
  F(fft_in)
  F(fft_out[0])
  F(fft_out[1])
  if (art->ifft_in)
  {
    for (int c = 0; c < art->channels; ++c)
    {
      F(ifft_in[c])
    }
    free(art->ifft_in);
    art->ifft_in = nullptr;
  }
  F(ifft_out)
  F(outbuf)
#undef F
#define F(P) \
  if (art->P) \
  { \
    fftw_destroy_plan(art->P); \
    art->P = nullptr; \
  }
  F(fft[0])
  F(fft[1])
  if (art->ifft)
  {
    for (int c = 0; c < art->channels; ++c)
    {
      F(ifft[c])
    }
    free(art->ifft);
    art->ifft = nullptr;
  }
#undef F
}

static int audio_rt_init(struct audio_rt *art, int channels, double sample_rate, uint64_t duration, float *data)
{
  int res = 0;
  std::memset(art, 0, sizeof(*art));
  art->requests = new ringbuffer<request, 256>();
  art->responses = new ringbuffer<response, 256>();
  art->channels = channels;
  art->sample_rate = sample_rate;
  art->duration = duration;
  art->frame = 0;
  art->time = 0;
  art->speed = 0;
  art->volume_start = 0;
  art->volume_target = 1;
  art->volume_ramp = 1;
  if (channels == 0)
  {
    art->data = nullptr;
    art->thread = new std::thread(audio_rt_silence, art);
  }
  else
  {
    art->data = data;
    // allocate FFT stuff
    art->window = fftw_alloc_real(AUDIO_FFT_SIZE);
    if (! art->window)
    {
      res = 1;
    }
    double gain = 0.5 / std::sqrt(AUDIO_FFT_SIZE);
    for (int i = 0; i < AUDIO_FFT_SIZE; ++i)
    {
      art->window[i] = gain * (1 - std::cos(i * 6.283185307179586 / AUDIO_FFT_SIZE));
    }
    art->fft_in = fftw_alloc_real(AUDIO_FFT_SIZE);
    if (! art->fft_in)
    {
      res = 1;
    }
    art->fft_out[0] = fftw_alloc_complex(AUDIO_FFT_SIZE);
    if (! art->fft_out[0])
    {
      res = 1;
    }
    art->fft_out[1] = fftw_alloc_complex(AUDIO_FFT_SIZE);
    if (! art->fft_out[1])
    {
      res = 1;
    }
    art->ifft_in = (fftw_complex **) std::calloc(1, channels * sizeof(*art->ifft_in));
    if (art->fft_in)
    {
      for (int c = 0; c < channels; ++c)
      {
        art->ifft_in[c] = fftw_alloc_complex(AUDIO_FFT_SIZE);
        if (! art->ifft_in[c])
        {
          res = 1;
        }
      }
    }
    else
    {
      res = 1;
    }
    art->ifft_out = fftw_alloc_real(AUDIO_FFT_SIZE);
    if (! art->ifft_out)
    {
      res = 1;
    }
    art->outbuf = (float *) fftw_malloc(sizeof(*art->outbuf) * channels * AUDIO_FFT_SIZE);
  //std::fprintf(stderr, "%p\n", art->outbuf);
    if (! art->outbuf)
    {
      res = 1;
    }
    art->ifft = (fftw_plan *) fftw_malloc(sizeof(*art->ifft) * channels);
    if (! art->ifft)
    {
      res = 1;
    }
    if (res == 0)
    {
      art->fft[0] = fftw_plan_dft_r2c_1d(AUDIO_FFT_SIZE, art->fft_in, art->fft_out[0], FFTW_PATIENT | FFTW_DESTROY_INPUT);
      if (! art->fft[0])
      {
        res = 1;
      }
      art->fft[1] = fftw_plan_dft_r2c_1d(AUDIO_FFT_SIZE, art->fft_in, art->fft_out[1], FFTW_PATIENT | FFTW_DESTROY_INPUT);
      if (! art->fft[1])
      {
        res = 1;
      }
      for (int c = 0; c < channels; ++c)
      {
        art->ifft[c] = fftw_plan_dft_c2r_1d(AUDIO_FFT_SIZE, art->ifft_in[c], art->ifft_out, FFTW_PATIENT | FFTW_PRESERVE_INPUT);
        if (! art->ifft[c])
        {
          res = 1;
        }
      }
    }
  }
  if (res != 0)
  {
    audio_rt_uninit(art);
  }
  else
  {
    if (channels > 0)
    {
      std::memset(art->fft_in, 0, AUDIO_FFT_SIZE * sizeof(*art->fft_in));
      std::memset(art->fft_out[0], 0, AUDIO_FFT_SIZE * sizeof(*art->fft_out[0]));
      std::memset(art->fft_out[1], 0, AUDIO_FFT_SIZE * sizeof(*art->fft_out[1]));
      for (int c = 0; c < channels; ++c)
      {
        std::memset(art->ifft_in[c], 0, AUDIO_FFT_SIZE * sizeof(*art->ifft_in[c]));
      }
      std::memset(art->ifft_out, 0, AUDIO_FFT_SIZE * sizeof(*art->ifft_out));
      std::memset(art->outbuf, 0, sizeof(*art->outbuf) * channels * AUDIO_FFT_SIZE);
    }
  }
  return res;
}

static int audio_rt_request_time(struct audio_rt *art, double time)
{
  struct request req;
  req.type = request_type_time;
  req.u.time.time = time;
  if (! art->requests->write(req))
  {
    art->errs_request_overrun++;
    return 1;
  }
  return 0;
}

static int audio_rt_seek_to(struct audio_rt *art, double time)
{
//std::fprintf(stderr, "[ SEEK\n");
  int res = 0;
  int64_t frame = std::floor(time * art->sample_rate);
  double fraction = (time * art->sample_rate) / (art->duration + ! art->duration);
  if (art->loop && art->duration > 0)
  {
    frame %= (int64_t) art->duration;
    frame += (int64_t) art->duration;
    frame %= (int64_t) art->duration;
    fraction -= std::floor(fraction);
  }
  else
  {
    frame = std::min(std::max((int64_t) 0, frame), (int64_t) art->duration);
    fraction = std::min(std::max(0.0, fraction), 1.0);
  }
  time = fraction * art->duration / art->sample_rate;
//std::fprintf(stderr, "frame %lld\n", frame);
  art->time = time;
//std::fprintf(stderr, "] SEEK\n");
  return res;
}

static int audio_rt_response_quit(struct audio_rt *art)
{
  struct response resp;
  resp.type = response_type_quit;
  if (! art->responses->write(resp))
  {
    art->errs_response_overrun++;
    return 1;
  }
  return 0;
}

static void audio_rt_silence(struct audio_rt *art)
{
  if (! art)
  {
    return;
  }
  bool running = true;
  while (running)
  {
    // handle messages from decoder
    while (running && art->responses->available() > 0)
    {
      struct response resp = art->responses->read();
      switch (resp.type)
      {
        case response_type_volume:
        {
          double volume = art->volume_target + art->volume_ramp * (art->volume_start - art->volume_target);
          art->volume_start = volume;
          art->volume_target = resp.u.volume.volume;
          art->volume_ramp = 1;
          break;
        }
        case response_type_speed:
        {
          art->speed = resp.u.speed.speed; // FIXME TODO fade
          break;
        }
        case response_type_loop:
        {
          art->loop = resp.u.loop.loop;
          break;
        }
        case response_type_time:
        {
          audio_rt_seek_to(art, resp.u.time.time);
          break;
        }
        case response_type_quit:
        {
          running = false;
          break;
        }
      }
    }
    // playback
    double dt = AUDIO_HOP_SIZE / art->sample_rate;
    std::this_thread::sleep_for(std::chrono::nanoseconds((int64_t) std::floor(1.0e9 * dt)));
    audio_rt_seek_to(art, art->time + art->speed * dt);
    // send playback position
    audio_rt_request_time(art, art->time);
  }
}

static void audio_rt_callback_1(struct audio_rt *art, float *out)
{
  if ((art->frame % AUDIO_HOP_SIZE) == 0)
  {
//std::fprintf(stderr, "[ SHUNT\n");
    // shunt output buffers
//std::fprintf(stderr, "memmove %p %p %lld\n", art->outbuf, art->outbuf + art->channels * AUDIO_HOP_SIZE, sizeof(*art->outbuf) * art->channels * AUDIO_HOP_SIZE * (AUDIO_OVERLAP - 1));
    std::memmove(art->outbuf, art->outbuf + art->channels * AUDIO_HOP_SIZE, sizeof(*art->outbuf) * art->channels * AUDIO_HOP_SIZE * (AUDIO_OVERLAP - 1));
//std::fprintf(stderr, "memset p %d %lld\n", art->outbuf + art->channels * AUDIO_HOP_SIZE * (AUDIO_OVERLAP - 1), 0, sizeof(*art->outbuf) * art->channels * AUDIO_HOP_SIZE);
    std::memset(art->outbuf + art->channels * AUDIO_HOP_SIZE * (AUDIO_OVERLAP - 1), 0, sizeof(*art->outbuf) * art->channels * AUDIO_HOP_SIZE);
//std::fprintf(stderr, "] SHUNT\n", art->outbuf, art->outbuf + art->channels * AUDIO_HOP_SIZE, sizeof(*art->outbuf) * art->channels * AUDIO_HOP_SIZE * (AUDIO_OVERLAP - 1));
    int64_t current_frame = std::floor(art->time * art->sample_rate);
    for (int c = 0; c < art->channels; ++c)
    {
      // phase vocoder
      for (int o = 0; o < 2; ++o)
      {
        int64_t offset = o;
        if (art->speed < 0)
        {
          offset = -offset;
        }
        // deinterleave channel from buffer(s)
        for (int i = 0; i < AUDIO_FFT_SIZE; ++i)
        {
          int64_t frame = current_frame + i + offset;
          if (art->loop && art->duration > 0)
          {
            frame %= art->duration;
            frame += art->duration;
            frame %= art->duration;
          }
          double sample = 0;
          if (0 <= frame && frame < (int64_t) art->duration && art->data)
          {
            sample = art->data[frame * art->channels + c];
          }
          art->fft_in[i] = art->window[i] * sample;
        }
        // do FFT of source audio
        fftw_execute(art->fft[o]);
      }
      // compute phase advance
      for (int bin = 0; bin < AUDIO_FFT_SIZE; ++bin)
      {
        std::complex<double> then = to_std_complex(art->ifft_in[c][bin]);
        std::complex<double> now = to_std_complex(art->fft_out[0][bin]);
        std::complex<double> next = to_std_complex(art->fft_out[1][bin]);
        std::complex<double> advance = normalize(next / now);
        for (int power = 1; power < AUDIO_HOP_SIZE; power <<= 1)
        {
          advance *= advance;
        }
        then = normalize(then) * std::abs(now) * advance;
        to_fftw_complex(then, art->ifft_in[c][bin]);
      }
      // do IFFT
      fftw_execute(art->ifft[c]);
      // interleave channels to output buffers
      for (int i = 0; i < AUDIO_FFT_SIZE; ++i)
      {
        art->outbuf[i * art->channels + c] += art->window[i] * art->ifft_out[i];
      }
    }
    art->frame = 0;
  }
  // copy data
  double volume = art->volume_target + art->volume_ramp * (art->volume_start - art->volume_target);
  art->volume_ramp = std::max(0.0, art->volume_ramp - 1/0.01 / art->sample_rate); // 10ms
  for (int c = 0; c < art->channels; ++c)
  {
    out[c] = volume * art->outbuf[art->frame * art->channels + c];
  }
  // advance
  art->frame++;
  audio_rt_seek_to(art, art->time + art->speed / art->sample_rate);
}

static void audio_rt_callback(void *userdata, Uint8 *stream, int len)
{
  struct audio_rt *art = (struct audio_rt *) userdata;
  assert(art);
  // handle messages from decoder
  while (art->responses->available() > 0)
  {
    struct response resp = art->responses->read();
    switch (resp.type)
    {
      case response_type_volume:
      {
        double volume = art->volume_target + art->volume_ramp * (art->volume_start - art->volume_target);
        art->volume_start = volume;
        art->volume_target = resp.u.volume.volume;
        art->volume_ramp = 1;
        break;
      }
      case response_type_speed:
      {
        //std::fprintf(stderr, "RT: speed := %f\n", resp->u.speed.speed);
        art->speed = resp.u.speed.speed; // FIXME TODO fade
        break;
      }
      case response_type_loop:
      {
        //std::fprintf(stderr, "RT: loop := %d\n", (int) resp->u.loop.loop);
        art->loop = resp.u.loop.loop;
        break;
      }
      case response_type_time:
      {
        //std::fprintf(stderr, "RT: seek to %.3f\n", resp->u.time.time);
        audio_rt_seek_to(art, resp.u.time.time);
        break;
      }
      case response_type_quit:
      {
        // not applicable to this rt mode
        break;
      }
    }
  }
  // process audio
  int channels = art->channels;
  assert(channels > 0);
  assert(channels <= AUDIO_CHANNELS_MAX);
  float *b = (float *) stream;
  int m = len / sizeof(float) / channels;
  int k = 0;
  for (int i = 0; i < m; ++i)
  {
    float out[AUDIO_CHANNELS_MAX];
    audio_rt_callback_1(art, out);
    for (int j = 0; j < channels; ++j)
    {
      b[k++] = out[j];
    }
  }
  audio_rt_request_time(art, art->time);
}

struct audio
{
  // non-realtime
  double total_time;
  double sample_rate;
  int channels;
  bool playing;
  bool playback_loop;
  double playback_speed;
  double playback_time;
  bool playback_mute;
  double playback_volume;
  uint64_t total_frames;
  ImGui::FileBrowser *browser;
  std::filesystem::path *soundtrack_file;
  SDL_AudioDeviceID device;
  // realtime
  bool have_rt;
  struct audio_rt art;
};

static int audio_open(struct audio *au, const std::filesystem::path &file, double duration = 0);

extern struct audio *audio_new(void)
{
  struct audio *au = (struct audio *) std::calloc(1, sizeof(*au));
  if (! au)
  {
    return nullptr;
  }
  au->playback_volume = 1;
  au->playback_speed = 1;
  return au;
}

static void audio_close(struct audio *au)
{
  if (au->device)
  {
    SDL_CloseAudioDevice(au->device);
    au->device = 0;
  }
  // deinit rt
  if (au->have_rt)
  {
    audio_rt_uninit(&au->art);
    au->have_rt = false;
  }
  au->total_frames = 0;
  au->sample_rate = 0;
  au->channels = 0;
  if (au->soundtrack_file)
  {
    delete au->soundtrack_file;
    au->soundtrack_file = nullptr;
  };
}

extern void audio_delete(struct audio *au)
{
  if (! au)
  {
    return;
  }
  audio_close(au);
  if (au->browser)
  {
    delete au->browser;
    au->browser = nullptr;
  }
}

static int audio_playback_volume(struct audio *au, double volume);
static int audio_playback_loop(struct audio *au, bool loop);

static int audio_open(struct audio *au, const std::filesystem::path &file, double duration)
{
  // FIXME TODO only close/reopen device if channels/samplerate changed
  double now = au->playback_time;
  audio_close(au);
  int res = 0;
  if (file.empty())
  {
    // init rt for silence (0 channels)
    double sample_rate = 48000;
    au->total_frames = std::max(1.0, std::round(sample_rate * duration));
    res = audio_rt_init(&au->art, 0, sample_rate, au->total_frames, nullptr);
    if (res != 0)
    {
      audio_close(au);
      return res;
    }
    au->have_rt = true;
    au->soundtrack_file = new std::filesystem::path(file);
    res = 0;
  }
  else
  {
    // open file
    SDL_AudioSpec wav_spec;
    Uint32 wav_length = 0;
    Uint8 *wav_buffer = nullptr;
    if (SDL_LoadWAV(file.string().c_str(), &wav_spec, &wav_buffer, &wav_length) == nullptr)
    {
      fprintf(stderr, "SDL_LoadWAV %s\n", SDL_GetError());
      return 1;
    }
    au->channels = wav_spec.channels;
    au->sample_rate = wav_spec.freq;
    SDL_AudioCVT cvt;
    SDL_BuildAudioCVT(&cvt, wav_spec.format, wav_spec.channels, wav_spec.freq, AUDIO_F32SYS, au->channels, au->sample_rate);
    cvt.len = wav_length;
    cvt.buf = (Uint8 *) SDL_malloc(cvt.len * cvt.len_mult);
    std::memcpy(cvt.buf, wav_buffer, wav_length);
    SDL_FreeWAV(wav_buffer);
    wav_buffer = nullptr;
    wav_length = 0;
    SDL_ConvertAudio(&cvt);
    au->total_frames = cvt.len_cvt / (au->channels * sizeof(float));
    // init rt
    res = audio_rt_init(&au->art, au->channels, au->sample_rate, au->total_frames, (float *) cvt.buf);
    if (res != 0)
    {
      fprintf(stderr, "audio_rt_init\n");
      audio_close(au);
      return res;
    }
    au->have_rt = true;
    // open audio device
    SDL_AudioSpec want, have;
    SDL_memset(&want, 0, sizeof(want)); /* or SDL_zero(want) */
    want.freq = au->sample_rate;
    want.channels = au->channels;
    want.format = AUDIO_F32SYS;
    want.samples = 1 << 12;
    want.callback = audio_rt_callback;
    want.userdata = &au->art;
    au->device = SDL_OpenAudioDevice(nullptr, 0, &want, &have, 0);
    if (au->device == 0)
    {
      fprintf(stderr, "SDL_OpenAudioDevice %s\n", SDL_GetError());
      audio_close(au);
      return 1;
    }
    au->soundtrack_file = new std::filesystem::path(file);
    SDL_PauseAudioDevice(au->device, 0);
    res = 0;
  }
  if (res == 0)
  {
    audio_set_time(au, now);
    audio_set_playing(au, audio_get_playing(au));
    audio_playback_loop(au, au->playback_loop);
    audio_set_speed(au, audio_get_speed(au));
    audio_playback_volume(au, au->playback_mute ? 0 : au->playback_volume);
  }
  return res;
}

extern bool audio_set_time(struct audio *au, double time)
{
  changed = true;
  if (au->have_rt)
  {
    struct response resp;
    resp.type = response_type_time;
    resp.u.time.time = time;
    if (! au->art.responses->write(resp))
    {
      au->art.errs_response_overrun++;
      return 1;
    }
    return 0;
  }
  return 1;
}

static int audio_playback_speed(struct audio *au, double speed)
{
  if (au->have_rt)
  {
    struct response resp;
    resp.type = response_type_speed;
    resp.u.speed.speed = speed;
    if (! au->art.responses->write(resp))
    {
      au->art.errs_response_overrun++;
      return 1;
    }
    return 0;
  }
  return 1;
}

static int audio_playback_loop(struct audio *au, bool loop)
{
  if (au->have_rt)
  {
    struct response resp;
    resp.type = response_type_loop;
    resp.u.loop.loop = loop;
    if (! au->art.responses->write(resp))
    {
      au->art.errs_response_overrun++;
      return 1;
    }
    return 0;
  }
  return 1;
}

static int audio_playback_volume(struct audio *au, double volume)
{
  if (au->have_rt)
  {
    struct response resp;
    resp.type = response_type_volume;
    resp.u.volume.volume = volume;
    if (! au->art.responses->write(resp))
    {
      au->art.errs_response_overrun++;
      return 1;
    }
    return 0;
  }
  return 1;
}

extern void audio_nrt(struct audio *au)
{
  if (au->have_rt)
  {
    // handle messages from audio rt
    while (au->art.requests->available() > 0)
    {
      struct request req = au->art.requests->read();
      switch (req.type)
      {
        case request_type_time:
        {
          //std::fprintf(stderr, "NRT: time %f\n", req->u.time.time);
          au->playback_time = req.u.time.time;
          break;
        }
      }
    }
  }
}

extern bool audio_display_early(struct audio *au)
{
  bool new_soundtrack = false;
  if (! au->browser)
  {
    au->browser = new ImGui::FileBrowser(ImGuiFileBrowserFlags_CloseOnEsc);
    au->browser->SetTitle("Soundtrack Audio File");
    au->browser->SetTypeFilters({ ".wav" });
  }
  ImGui::Begin("Audio", nullptr, ImGuiWindowFlags_HorizontalScrollbar);
  bool red = ! au->have_rt;
  if (red)
  {
    ImGui::PushStyleColor(ImGuiCol_Button, button_red);
  }
  bool soundtrack_is_empty = true;
  if (au->soundtrack_file && ! au->soundtrack_file->empty())
  {
    soundtrack_is_empty = false;
  }
  int audio_preset = soundtrack_is_empty ? 0 : 1;
  const char *audio_presets[] = { "Silence", "Soundtrack" };
  ImGui::PushItemWidth(100);
  bool audio_preset_changed = false;
  double seconds = 0;
  if (au->have_rt)
  {
    seconds = au->art.duration / au->art.sample_rate;
  }
  if (ImGui::Combo("##Audio", &audio_preset, audio_presets, IM_ARRAYSIZE(audio_presets)))
  {
    if (audio_preset == 1)
    {
      au->browser->Open();
    }
    audio_preset_changed = true;
  }
  if (red)
  {
    ImGui::PopStyleColor(1);
  }
  ImGui::PopItemWidth();
  if (ImGui::IsItemHovered())
  {
    ImGui::SetTooltip("Set audio source.");
  }
  if (audio_preset == 0)
  {
    ImGui::SameLine();
    ImGui::PushItemWidth(200);
    if (ImGui::InputDouble("Duration", &seconds, 1, 60, "%.3fs") || audio_preset_changed)
    {
      if (seconds > 0)
      {
        std::filesystem::path silence;
        audio_open(au, silence, seconds);
        new_soundtrack = true;
      }
    }
    ImGui::PopItemWidth();
    if (ImGui::IsItemHovered())
    {
      ImGui::SetTooltip("Set duration in seconds.");
    }
  }
  if (audio_preset == 1 && au->soundtrack_file)
  {
    ImGui::SameLine();
    ImGui::Text("%s", au->soundtrack_file->string().c_str());
  }
  if (audio_preset == 1 && au->have_rt)
  {
    ImGui::Text("Sample Rate: %.0fHz", au->art.sample_rate);
    ImGui::SameLine();
    ImGui::Text("Channels: %d", au->art.channels);
    ImGui::SameLine();
    ImGui::Text("Duration: %.3fs", au->art.duration / au->art.sample_rate);
  }
  if (au->have_rt)
  {
    float seek = au->playback_time;
    if (ImGui::SliderFloat("Time", &seek, 0, au->art.duration / au->art.sample_rate, "%.3fs"))
    {
      audio_set_time(au, seek);
    }
    if (ImGui::IsItemHovered())
    {
      ImGui::SetTooltip("Seek to time in soundtrack.");
    }
    if (ImGui::Button("Rewind"))
    {
      audio_set_time(au, 0);
    }
    if (ImGui::IsItemHovered())
    {
      ImGui::SetTooltip("Seek to the start of the soundtrack.");
    }
    ImGui::SameLine();
    if (ImGui::Button("Stop"))
    {
      au->playing = false;
      audio_playback_speed(au, 0);
    }
    if (ImGui::IsItemHovered())
    {
      ImGui::SetTooltip("Disable soundtrack play head advance.");
    }
    ImGui::SameLine();
    if (ImGui::Button("Play"))
    {
      au->playing = true;
      audio_playback_speed(au, au->playback_speed);
    }
    if (ImGui::IsItemHovered())
    {
      ImGui::SetTooltip("Enable soundtrack play head advance.");
    }
    ImGui::SameLine();
    if (ImGui::Checkbox("Loop", &au->playback_loop))
    {
      audio_playback_loop(au, au->playback_loop);
    }
    if (ImGui::IsItemHovered())
    {
      ImGui::SetTooltip("Enable soundtrack play head loop around ends.");
    }
    ImGui::SameLine();
    if (ImGui::Button("1x"))
    {
      au->playback_speed = 1;
      if (au->playing)
      {
        audio_playback_speed(au, au->playback_speed);
      }
    }
    if (ImGui::IsItemHovered())
    {
      ImGui::SetTooltip("Set playback speed to 1x (normal speed).");
    }
    ImGui::SameLine();
    float speed = au->playback_speed;
    ImGui::PushItemWidth(100);
    if (ImGui::SliderFloat("##Speed", &speed, -16, 16, "%.3fx"))
    {
      au->playback_speed = speed;
      if (au->playing)
      {
        audio_playback_speed(au, au->playback_speed);
      }
    }
    if (ImGui::IsItemHovered())
    {
      ImGui::SetTooltip("Set playback speed (negative plays backwards).");
    }
    ImGui::PopItemWidth();
    ImGui::SameLine();
    if (ImGui::Checkbox("##Mute", &au->playback_mute))
    {
      audio_playback_volume(au, au->playback_mute ? 0 : au->playback_volume);
    }
    if (ImGui::IsItemHovered())
    {
      ImGui::SetTooltip("Mute audio (set volume to -inf dB, 0.0).");
    }
    ImGui::SameLine();
    ImGui::PushItemWidth(100);
    float volume = rmstodbfs(au->playback_volume);
    if (ImGui::SliderFloat("##Volume", &volume, -60, 10, "%.1fdB"))
    {
      au->playback_mute = false;
      au->playback_volume = dbfstorms(volume);
      audio_playback_volume(au, au->playback_mute ? 0 : au->playback_volume);
    }
    if (ImGui::IsItemHovered())
    {
      ImGui::SetTooltip("Set playback volume in dB (0 is full scale).");
    }
    ImGui::PopItemWidth();
    uint64_t errs_request_overrun = au->art.errs_request_overrun;
    uint64_t errs_response_overrun = au->art.errs_response_overrun;
    if (errs_request_overrun || errs_response_overrun)
    {
      ImGui::SameLine();
      ImGui::Text("Errors: %u / %u", (unsigned int) errs_request_overrun, (unsigned int) errs_response_overrun);
      ImGui::SameLine();
      if (ImGui::Button("Reset"))
      {
        au->art.errs_request_overrun = 0;
        au->art.errs_response_overrun = 0;
      }
    }
  }
  ImGui::End();
  return new_soundtrack;
}

extern bool audio_display_late(struct audio *au)
{
  if (! au)
  {
    return false;
  }
  if (! au->browser)
  {
    return false;
  }
  au->browser->Display();
  if (au->browser->HasSelected())
  {
    audio_open(au, au->browser->GetSelected());
    au->browser->ClearSelected();
    if (au->have_rt)
    {
      return true;
    }
  }
  return false;
}

extern double audio_get_duration(const struct audio *au)
{
  if (! au)
  {
    return 0;
  }
  if (! au->have_rt)
  {
    return 0;
  }
  return au->art.duration / au->art.sample_rate;
}

extern double audio_get_time(const struct audio *au)
{
  if (! au)
  {
    return 0;
  }
  if (! au->have_rt)
  {
    return 0;
  }
  return au->playback_time;
}

extern bool audio_set_playing(struct audio *au, bool playing)
{
  if (au)
  {
    au->playing = playing;
    return 0 == audio_playback_speed(au, au->playing ? au->playback_speed : 0);
  }
  return false;
}

extern bool audio_get_playing(const struct audio *au)
{
  return au->playing;
}

extern bool audio_set_speed(struct audio *au, double speed)
{
  if (au)
  {
    changed |= (speed != au->playback_speed);
    au->playback_speed = speed;
    return 0 == audio_playback_speed(au, au->playing ? au->playback_speed : 0);
  }
  return false;
}

extern double audio_get_speed(const struct audio *au)
{
  return au->playback_speed;
}

extern std::filesystem::path audio_get_soundtrack(const struct audio *au)
{
  if (au)
  {
    if (au->soundtrack_file)
    {
      return *au->soundtrack_file;
    }
  }
  return std::filesystem::path();
}

extern enum audio_type audio_get_type(const struct audio *au)
{
  if (au)
  {
    if (au->soundtrack_file)
    {
      if (! au->soundtrack_file->empty())
      {
        return audio_type_soundtrack;
      }
    }
  }
  return audio_type_silence;
}

extern bool audio_set_volume(struct audio *au, double volume)
{
  if (au)
  {
    au->playback_volume = volume;
    return 0 == audio_playback_volume(au, au->playback_mute ? 0 : au->playback_volume);
  }
  return false;
}

extern double audio_get_volume(const struct audio *au)
{
  return au->playback_volume;
}

extern bool audio_set_mute(struct audio *au, bool mute)
{
  if (au)
  {
    au->playback_mute = mute;
    return 0 == audio_playback_volume(au, au->playback_mute ? 0 : au->playback_volume);
  }
  return false;
}

extern bool audio_get_mute(const struct audio *au)
{
  return au->playback_mute;
}

extern bool audio_set_loop(struct audio *au, bool loop)
{
  if (au)
  {
    au->playback_loop = loop;
    return 0 == audio_playback_loop(au, au->playback_loop);
  }
  return false;
}

extern bool audio_get_loop(const struct audio *au)
{
  return au->playback_loop;
}

extern bool audio_set_soundtrack(struct audio *au, const std::filesystem::path &file)
{
  return 0 == audio_open(au, file);
}

extern bool audio_set_silence(struct audio *au, double duration)
{
  return 0 == audio_open(au, std::filesystem::path(), duration);
}
