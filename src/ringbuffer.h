#pragma once
// zoomasm -- zoom video assembler
// (c) 2019,2020,2021,2022,2023 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-only

#include <atomic>
#include <cstdint>

template <typename T, uint64_t N>
struct ringbuffer // FIXME: this will crash when uint64_t overflows
{
  T data[N];
  std::atomic<uint64_t> read_ptr;
  std::atomic<uint64_t> write_ptr;
  ringbuffer()
  {
    read_ptr = 0;
    write_ptr = 0;
  }
  bool available()
  {
    return read_ptr < write_ptr;
  }
  T read()
  {
    // only valid if available()
    return data[read_ptr++ % N];
  }
  bool write(const T &item)
  {
    if (write_ptr < read_ptr + N)
    {
      data[write_ptr++ % N] = item;
      return true;
    }
    else
    {
      return false;
    }
  }
};
