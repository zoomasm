#!/bin/bash
## zoomasm -- zoom video assembler
## (c) 2019,2020,2021,2022,2023 Claude Heiland-Allen
## SPDX-License-Identifier: AGPL-3.0-only
echo "const char *$(basename "${1}") ="
sed 's|"||g' |
sed 's|^|"|' |
sed 's|$|\\n"|'
echo "\"\\n\""
echo ";"
