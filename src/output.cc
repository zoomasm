// zoomasm -- zoom video assembler
// (c) 2019,2020,2021,2022,2023 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-only

#include "output.h"

#include "imgui.h"
#include "imfilebrowser.h"
#include "imgui_stdlib.h"

#include <cmath>

#include <GL/glew.h>

#include "resource.h"
#include "utility.h"

static const int pipeline = 4;
static GLuint pbo[4] = { 0, 0, 0, 0 };
static ssize_t bytes = 0;
static bool first_run = true;

static const char* size_presets[] = { "Custom", "640x360", "1024x576", "1280x720", "1920x1080", "2560x1440", "3840x2160", "1024x512", "2048x1024", "4096x2048", "8192x4096" };
static const char *fps_presets[] = { "Custom", "24", "25", "29.97", "30", "50", "60", "100", "120", "144" };
static const char* audio_bitrate_presets[] = { "Custom", "64", "96", "128", "192", "256", "384", "512", "Copy" };

static int size_preset = 1;
static int fps_preset = 2;
static int audio_bitrate_preset = 6;
static int output_frame_width_preset[] = { 1600, 640, 1024, 1280, 1920, 2560, 3840, 1024, 2048, 4096, 8192 };
static int output_frame_height_preset[] = { 900, 360,  576,  720, 1080, 1440, 2160, 512, 1024, 2048, 4096 };
static double output_fps_preset[] = { 10, 24, 25, 29.97, 30, 50, 60, 100, 120, 144 };
static double output_audio_bitrate_preset[] = { 320, 64, 96, 128, 192, 256, 384, 512, -1 };
static ImGui::FileBrowser *ffbrowser = nullptr;
static ImGui::FileBrowser *outbrowser = nullptr;

extern bool output_display_early(struct ffmpeg *enc, int keyframe_width, bool can_record, int projection, bool &emergency_stop)
{
  bool start_recording = false;
  if (! ffbrowser)
  {
    ffbrowser = new ImGui::FileBrowser(ImGuiFileBrowserFlags_CloseOnEsc);
    ffbrowser->SetTitle("FFmpeg Program Binary");
#ifdef _WIN32
    ffbrowser->SetTypeFilters({ ".exe" });
#endif
  }
  if (! outbrowser)
  {
    outbrowser = new ImGui::FileBrowser(ImGuiFileBrowserFlags_EnterNewFilename | ImGuiFileBrowserFlags_CreateNewDir);
    outbrowser->SetTitle("Output Video File");
    outbrowser->SetTypeFilters({ ".mp4", ".mkv", ".mov", ".ppm" });
  }
  ImGui::Begin("Output", nullptr, ImGuiWindowFlags_HorizontalScrollbar);
  // status
  if (can_record)
  {
    ImGui::PushStyleColor(ImGuiCol_Button, button_red);
    if (ImGui::Button("Record"))
    {
      start_recording = true;
    }
    if (ImGui::IsItemHovered())
    {
      ImGui::SetTooltip("Start rendering and encoding output video file.");
    }
    ImGui::PopStyleColor(1);
    ImGui::SameLine();
  }
  float fps = ImGui::GetIO().Framerate;
  ImGui::Text("Real-time %.1f mspf, %.1f fps", 1000.0f / fps, fps);
  emergency_stop = fps < 2.0f;
  bool need_ffmpeg
    = ! starts_with(ffmpeg_get_version(enc), "ffmpeg version ")
    && ffmpeg_get_output(enc).extension() != ".ppm";
  if (need_ffmpeg)
  {
    ImGui::PushStyleColor(ImGuiCol_Button, button_red);
  }
  if (ImGui::Button("FFmpeg"))
  {
    ffbrowser->Open();
  }
  if (ImGui::IsItemHovered())
  {
    ImGui::SetTooltip("Select FFmpeg program binary.");
  }
  if (need_ffmpeg)
  {
    ImGui::PopStyleColor(1);
  }
  ImGui::SameLine();
  ImGui::Text("%s", ffmpeg_get_ffmpeg(enc).string().c_str());
  ImGui::Text("%s", ffmpeg_get_version(enc).c_str());

  bool advanced = ffmpeg_get_advanced(enc);
  if (ImGui::Checkbox("Advanced", &advanced))
  {
    ffmpeg_set_advanced(enc, advanced);
  }
  if (ImGui::IsItemHovered())
  {
    ImGui::SetTooltip("Advanced FFmpeg settings.");
  }
  if (ffmpeg_get_advanced(enc))
  {
    bool twopass = ffmpeg_get_twopass(enc);
    if (ImGui::Checkbox("Two Pass", &twopass))
    {
      ffmpeg_set_twopass(enc, twopass);
    }
    if (ImGui::IsItemHovered())
    {
      ImGui::SetTooltip("Use two-pass FFmpeg encoding.");
    }
  }
  else
  {
    if (ffmpeg_get_output(enc).empty())
    {
      ImGui::PushStyleColor(ImGuiCol_Button, button_red);
    }
    if (ImGui::Button("Output"))
    {
      outbrowser->Open();
    }
    if (ImGui::IsItemHovered())
    {
      ImGui::SetTooltip("Select output video file.");
    }
    if (ffmpeg_get_output(enc).empty())
    {
      ImGui::PopStyleColor(1);
    }
    ImGui::SameLine();
    bool overwrite = ffmpeg_get_overwrite(enc);
    bool red = ! overwrite && ffmpeg_get_output_exists(enc);
    if (red)
    {
      ImGui::PushStyleColor(ImGuiCol_FrameBg, button_red);
    }
    if (ImGui::Checkbox("Overwrite", &overwrite))
    {
      ffmpeg_set_overwrite(enc, overwrite);
    }
    if (ImGui::IsItemHovered())
    {
      ImGui::SetTooltip("Force overwrite of any existing file without warning.");
    }
    if (red)
    {
      ImGui::PopStyleColor(1);
    }
    ImGui::SameLine();
    ImGui::Text("%s", ffmpeg_get_output(enc).string().c_str());
  }
  bool resolution_changed = false;
  ImGui::PushItemWidth(100);
  // find resolution preset
  int old_preset = size_preset;
  int old_width = output_frame_width_preset[old_preset];
  int old_height = output_frame_height_preset[old_preset];
  bool found_preset = false;
  for (int preset = 0; preset < IM_ARRAYSIZE(size_presets); ++preset)
  {
    if (output_frame_width_preset[preset] == ffmpeg_get_width(enc) &&
        output_frame_height_preset[preset] == ffmpeg_get_height(enc))
    {
      size_preset = preset;
      found_preset = true;
      break;
    }
  }
  if (! found_preset)
  {
    size_preset = 0;
    output_frame_width_preset[0] = ffmpeg_get_width(enc);
    output_frame_height_preset[0] = ffmpeg_get_height(enc);
  }
  resolution_changed |= old_width != output_frame_width_preset[size_preset];
  resolution_changed |= old_height != output_frame_height_preset[size_preset];
  ImGui::Combo("Resolution", &size_preset, size_presets, IM_ARRAYSIZE(size_presets));
  if (ImGui::IsItemHovered())
  {
    ImGui::SetTooltip("Set output video frame size preset.");
  }
  if (size_preset == 0)
  {
    ImGui::SameLine();
    static int dim[2] = { output_frame_width_preset[0], output_frame_height_preset[0] };
    ImGui::InputInt2("##dim", &dim[0]);
    if (ImGui::IsItemHovered())
    {
      ImGui::SetTooltip("Set custom output video frame size.");
    }
    output_frame_width_preset[0] = dim[0];
    output_frame_height_preset[0] = dim[1];
  }
  ImGui::PopItemWidth();
  if (ffmpeg_get_width(enc) != output_frame_width_preset[size_preset])
  {
    resolution_changed = true;
  }
  if (ffmpeg_get_height(enc) != output_frame_height_preset[size_preset])
  {
    resolution_changed = true;
  }
  if (resolution_changed || first_run)
  {
    first_run = false;
    int w = output_frame_width_preset[size_preset];
    int h = output_frame_height_preset[size_preset];
    if (0 < w && 0 < h)
    {
      glTexImage2D(GL_PROXY_TEXTURE_2D, 0, GL_RGBA8, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
      GLint ok;
      glGetTexLevelParameteriv(GL_PROXY_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &ok);
      if (ok)
      {
        // resize output textures and buffers
        glActiveTexture(GL_TEXTURE0 + TEXTURE_OUTPUT_RGB);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
        glGenerateMipmap(GL_TEXTURE_2D);
        bytes = w * h * 3;
        if (pbo[0])
        {
          glDeleteBuffers(pipeline, &pbo[0]);
          for (int i = 0; i < pipeline; ++i)
          {
            pbo[i] = 0;
          }
        }
        glGenBuffers(pipeline, &pbo[0]);
        for (int i = 0; i < pipeline; ++i) {
          glBindBuffer(GL_PIXEL_PACK_BUFFER, pbo[i]);
          if (false)
          {
            // OpenGL >= 4.4
            glBufferStorage(GL_PIXEL_PACK_BUFFER, bytes, 0, GL_MAP_READ_BIT);
          }
          else
          {
            // OpenGL >= 2.0
            glBufferData(GL_PIXEL_PACK_BUFFER, bytes, 0, GL_DYNAMIC_READ);
          }
        }
        ffmpeg_set_width(enc, w);
        ffmpeg_set_height(enc, h);
      }
      else
      {
        ImGui::Text("Invalid output resolution (too big)");
      }
    }
    else
    {
      ImGui::Text("Invalid output resolution (too small)");
    }
  }
  if (projection == projection_rectangular2d)
  {
    double output_diagonal = std::hypot(double(ffmpeg_get_width(enc)), double(ffmpeg_get_height(enc)));
    double circumference = 3.141592653589793 * output_diagonal;
    double aa = double(keyframe_width) / circumference;
    ImGui::Text("Antialiasing: %.3g texels per pixel", aa * aa);
    int w = std::round(circumference);
    int h = std::round(circumference / 9.0);
    ImGui::Text("Keyframe resolution for AA=1: %dx%d", w, h);
    double diameter = double(keyframe_width) / (3.141592653589793 * 1.0);
    w = std::round(diameter * double(ffmpeg_get_width(enc)) / output_diagonal);
    h = std::round(diameter * double(ffmpeg_get_height(enc)) / output_diagonal);
    ImGui::Text("Output resolution for AA=1: %dx%d", w, h);
  }
  else if (projection == projection_equirectangular360)
  {
    double aa = double(keyframe_width) / double(ffmpeg_get_width(enc));
    ImGui::Text("Antialiasing: %.3g texels per pixel", aa * aa);
    ImGui::Text("Keyframe resolution for AA=1: %dx%d", ffmpeg_get_width(enc), int(std::round(ffmpeg_get_width(enc) / 9.0 / 16.0)) * 16);
    ImGui::Text("Output resolution for AA=1: %dx%d", keyframe_width, keyframe_width / 2);
  }
  ImGui::PushItemWidth(100);
  // find fps preset
  old_preset = fps_preset;
  found_preset = false;
  for (int preset = 0; preset < IM_ARRAYSIZE(fps_presets); ++preset)
  {
    if (output_fps_preset[preset] == ffmpeg_get_fps(enc))
    {
      fps_preset = preset;
      found_preset = true;
      break;
    }
  }
  if (! found_preset)
  {
    fps_preset = 0;
    output_fps_preset[0] = ffmpeg_get_fps(enc);
  }
  ImGui::Combo("FPS", &fps_preset, fps_presets, IM_ARRAYSIZE(fps_presets));
  if (ImGui::IsItemHovered())
  {
    ImGui::SetTooltip("Set output video frame rate preset.");
  }
  if (fps_preset == 0)
  {
    ImGui::SameLine();
    ImGui::InputDouble("fps", &output_fps_preset[0], 0.0f, 0.0f, "%.8f");
    if (ImGui::IsItemHovered())
    {
      ImGui::SetTooltip("Set custom output video frame rate.");
    }
  }
  if (ffmpeg_get_fps(enc) != output_fps_preset[fps_preset])
  {
    ffmpeg_set_fps(enc, output_fps_preset[fps_preset]);
  }
  ImGui::PopItemWidth();
  if (ffmpeg_get_advanced(enc))
  {
    if (ImGui::InputText("Pass 1", ffmpeg_get_pass_1(enc)))
    {
      // nop
    }
    if (ImGui::IsItemHovered())
    {
      ImGui::SetTooltip("FFmpeg first pass encoder command line arguments.");
    }
    if (ffmpeg_get_twopass(enc))
    {
      if (ImGui::InputText("Pass 2", ffmpeg_get_pass_2(enc)))
      {
        // nop
      }
      if (ImGui::IsItemHovered())
      {
        ImGui::SetTooltip("FFmpeg second pass encoder command line arguments.");
      }
    }
  }
  else
  {
    ImGui::PushItemWidth(100);
    int crf = ffmpeg_get_video_crf(enc);
    if (ImGui::InputInt("Video CRF", &crf, 1, 10))
    {
      crf = std::min(std::max(crf, 0), 51); // FIXME would be 63 for 10bit, but we don't support that yet
      ffmpeg_set_video_crf(enc, crf);
    }
    if (ImGui::IsItemHovered())
    {
      ImGui::SetTooltip("Set video quality/size:\n0 lossless/incompatible\n1 best/largest\n20 good/large\n51 worst/smallest");
    }
    ImGui::PopItemWidth();
    ImGui::SameLine();
    ImGui::PushItemWidth(100);
    // find bitrate preset
    old_preset = audio_bitrate_preset;
    found_preset = false;
    for (int preset = 0; preset < IM_ARRAYSIZE(audio_bitrate_presets); ++preset)
    {
      if (output_audio_bitrate_preset[preset] == ffmpeg_get_audio_kbps(enc))
      {
        audio_bitrate_preset = preset;
        found_preset = true;
        break;
      }
    }
    if (! found_preset)
    {
      audio_bitrate_preset = 0;
      output_audio_bitrate_preset[0] = ffmpeg_get_audio_kbps(enc);
    }
    if (ImGui::Combo("Audio Bitrate", &audio_bitrate_preset, audio_bitrate_presets, IM_ARRAYSIZE(audio_bitrate_presets)))
    {
      ffmpeg_set_audio_kbps(enc, output_audio_bitrate_preset[audio_bitrate_preset]);
    }
    if (ImGui::IsItemHovered())
    {
      ImGui::SetTooltip("Set output video audio bitrate preset.");
    }
    if (audio_bitrate_preset == 0)
    {
      ImGui::SameLine();
      if (ImGui::InputDouble("kbps", &output_audio_bitrate_preset[0], 0, 0, "%.3f"))
      {
        ffmpeg_set_audio_kbps(enc, output_audio_bitrate_preset[audio_bitrate_preset]);
      }
      if (ImGui::IsItemHovered())
      {
        ImGui::SetTooltip("Set custom output video audio bitrate.");
      }
    }
    ImGui::PopItemWidth();
  }
  ImGui::Text("Encoder log:");
  ffmpeg_display(enc, true);
  ImGui::End();
  return start_recording;
}

extern void output_display_late(struct ffmpeg *enc)
{
  ffbrowser->Display();
  if (ffbrowser->HasSelected())
  {
    ffmpeg_set_ffmpeg(enc, ffbrowser->GetSelected());
    ffbrowser->ClearSelected();
  }
  outbrowser->Display();
  if (outbrowser->HasSelected())
  {
    ffmpeg_set_output(enc, outbrowser->GetSelected());
    outbrowser->ClearSelected();
  }
}

extern int output_record(struct ffmpeg *enc, int output_frame_index, bool draining)
{
  int k = output_frame_index % pipeline;
  glBindBuffer(GL_PIXEL_PACK_BUFFER, pbo[k]);
  if (output_frame_index - pipeline >= 0) {
    void *buf = glMapBuffer(GL_PIXEL_PACK_BUFFER, GL_READ_ONLY);
    if (! buf)
    {
      std::fprintf(stderr, "OUTPUT ERROR: could not map buffer\n");
      return 1;
    }
    if (ffmpeg_write(enc, buf, bytes))
    {
      std::fprintf(stderr, "OUTPUT ERROR: could not write buffer\n");
      glUnmapBuffer(GL_PIXEL_PACK_BUFFER);
      return 1;
    }
    glUnmapBuffer(GL_PIXEL_PACK_BUFFER);
  }
  if (! draining)
  {
    glReadPixels(0, 0, ffmpeg_get_width(enc), ffmpeg_get_height(enc), GL_RGB, GL_UNSIGNED_BYTE, 0);
  }
  return 0;
}

extern int output_get_pipeline(void)
{
  return pipeline;
}

struct output
{
  GLuint tex;
  GLuint fbo;
};

extern struct output *output_new(void)
{
  struct output *out = (struct output *) std::calloc(1, sizeof *out);
  glGenTextures(1, &out->tex);
  glActiveTexture(GL_TEXTURE0 + TEXTURE_OUTPUT_RGB);
  glBindTexture(GL_TEXTURE_2D, out->tex);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, 1, 1, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glGenFramebuffers(1, &out->fbo);
  glBindFramebuffer(GL_FRAMEBUFFER, out->fbo);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, out->tex, 0);
  return out;
}

extern void output_delete(struct output *out)
{
  if (! out)
  {
    return;
  }
  glActiveTexture(GL_TEXTURE0 + TEXTURE_OUTPUT_RGB);
  glBindTexture(GL_TEXTURE_2D, 0);
  glDeleteTextures(1, &out->tex);
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  glDeleteFramebuffers(1, &out->fbo);
  std::free(out);
}

extern void output_bind_framebuffer(const struct output *out)
{
  glBindFramebuffer(GL_FRAMEBUFFER, out->fbo);
}
