// zoomasm -- zoom video assembler
// (c) 2019,2020,2021,2022,2023 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-only

#include "process.h"

#include <cstring>

#include <spawn.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "utility.h"

// reference: <https://stackoverflow.com/a/27328610>

struct process
{
  int argc;
  char **argp;
  bool stdin_closed;
  int stdin_pipe[2];
  int stdout_pipe[2];
  int stderr_pipe[2];
  posix_spawn_file_actions_t files;
  pid_t pid;
};

extern struct process *process_open(const std::vector<std::string> &args)
{
  struct process *proc = (struct process *) std::calloc(1, sizeof *proc);
  if (proc)
  {
    proc->argc = args.size();
    proc->argp = (char **) std::calloc(proc->argc + 1, sizeof *proc->argp);
  if (proc->argp)
  {
    for (int i = 0; i < proc->argc; ++i)
    {
      proc->argp[i] = mystrdup(args[i].c_str());
      if (! proc->argp[i])
      {
        for (int j = 0; j < i; ++j)
        {
          std::free(proc->argp[j]);
        }
        std::free(proc->argp);
        std::free(proc);
        return nullptr;
      }
    }
    proc->argp[proc->argc] = nullptr;
  if (! pipe(&proc->stdin_pipe[0]))
  {
  if (! pipe(&proc->stdout_pipe[0]))
  {
  if (! pipe(&proc->stderr_pipe[0]))
  {
    posix_spawn_file_actions_init(&proc->files);
    posix_spawn_file_actions_addclose(&proc->files, proc->stdin_pipe[1]);
    posix_spawn_file_actions_addclose(&proc->files, proc->stdout_pipe[0]);
    posix_spawn_file_actions_addclose(&proc->files, proc->stderr_pipe[0]);
    posix_spawn_file_actions_adddup2(&proc->files, proc->stdin_pipe[0], 0);
    posix_spawn_file_actions_adddup2(&proc->files, proc->stdout_pipe[1], 1);
    posix_spawn_file_actions_adddup2(&proc->files, proc->stderr_pipe[1], 2);
    posix_spawn_file_actions_addclose(&proc->files, proc->stdin_pipe[0]);
    posix_spawn_file_actions_addclose(&proc->files, proc->stdout_pipe[1]);
    posix_spawn_file_actions_addclose(&proc->files, proc->stderr_pipe[1]);
  if (! posix_spawnp(&proc->pid, proc->argp[0], &proc->files, nullptr, &proc->argp[0], nullptr))
  {
    close(proc->stdin_pipe[0]);
    close(proc->stdout_pipe[1]);
    close(proc->stderr_pipe[1]);
    return proc;
  }
    // spawnp failed
    posix_spawn_file_actions_destroy(&proc->files);
    close(proc->stderr_pipe[0]);
    close(proc->stderr_pipe[1]);
  }
    // pipe stderr failed
    close(proc->stdout_pipe[0]);
    close(proc->stdout_pipe[1]);
  }
    // pipe stdout failed
    close(proc->stdin_pipe[0]);
    close(proc->stdin_pipe[1]);
  }
    // pipe stdin failed
    for (int i = 0; i < proc->argc; ++i)
    {
      if (proc->argp[i])
      {
        std::free(proc->argp[i]);
      }
    }
    std::free(proc->argp);
  }
    // calloc argp failed
    std::free(proc);
  }
  // calloc proc failed
  return nullptr;
}

extern void process_close(struct process *proc)
{
  if (! proc->stdin_closed)
  {
    close(proc->stdin_pipe[1]);
    proc->stdin_closed = true;
  }
  while (true)
  {
    int status = 0;
    int w = waitpid(proc->pid, &status, WUNTRACED | WCONTINUED);
    if (w == -1)
    {
      break;
    }
    if (WIFEXITED(status))
    {
      break;
    }
    else if (WIFSIGNALED(status))
    {
      break;
    }
  }
  posix_spawn_file_actions_destroy(&proc->files);
  close(proc->stdout_pipe[0]);
  close(proc->stderr_pipe[0]);
  for (int i = 0; i < proc->argc; ++i)
  {
    if (proc->argp[i])
    {
      free(proc->argp[i]);
    }
  }
  std::free(proc->argp);
  std::free(proc);
}

extern void process_stdin_eof(struct process *proc)
{
  close(proc->stdin_pipe[1]);
  proc->stdin_closed = true;
}

extern ssize_t process_stdin(struct process *proc, const void *data, ssize_t bytes)
{
  return write(proc->stdin_pipe[1], data, bytes);
}

extern ssize_t process_stdout(struct process *proc, void *data, ssize_t bytes)
{
  return read(proc->stdout_pipe[0], data, bytes);
}

extern ssize_t process_stderr(struct process *proc, void *data, ssize_t bytes)
{
  return read(proc->stderr_pipe[0], data, bytes);
}
