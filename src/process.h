#pragma once
// zoomasm -- zoom video assembler
// (c) 2019,2020,2021,2022,2023 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-only

#include <string>
#include <vector>

struct process;
extern struct process *process_open(const std::vector<std::string> &command_line);
extern void process_close(struct process *proc);
extern void process_stdin_eof(struct process *proc);
extern ssize_t process_stdin(struct process *proc, const void *data, ssize_t bytes);
extern ssize_t process_stdout(struct process *proc, void *data, ssize_t bytes);
extern ssize_t process_stderr(struct process *proc, void *data, ssize_t bytes);
