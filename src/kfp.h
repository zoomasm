#pragma once
// zoomasm -- zoom video assembler
// (c) 2021,2022,2023 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-only

#include <string>
#include <GL/glew.h>

struct kfp;

extern struct kfp *kfp_new(const std::string &s, double IterationsBias);
extern void kfp_delete(struct kfp *p);

extern std::string kfp_get_glsl(struct kfp *p);
extern void kfp_set_uniforms(GLuint program, const struct kfp *p);
