#pragma once
// zoomasm -- zoom video assembler
// (c) 2019,2020,2021,2022,2023 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-only

#include <filesystem>
#include <string>

enum ffmpeg_format
{
  ffmpeg_format_srgb8 = 0
};

struct ffmpeg;

extern struct ffmpeg *ffmpeg_new(void);
extern void ffmpeg_delete(struct ffmpeg *enc);

extern bool ffmpeg_set_ffmpeg(struct ffmpeg *enc, const std::filesystem::path &ffexe);
extern bool ffmpeg_set_format(struct ffmpeg *enc, enum ffmpeg_format format);
extern bool ffmpeg_set_width(struct ffmpeg *enc, int width);
extern bool ffmpeg_set_height(struct ffmpeg *enc, int height);
extern bool ffmpeg_set_fps(struct ffmpeg *enc, double fps);

extern std::filesystem::path ffmpeg_get_ffmpeg(const struct ffmpeg *enc);
extern std::string ffmpeg_get_version(const struct ffmpeg *enc);
extern enum ffmpeg_format ffmpeg_get_format(const struct ffmpeg *enc);
extern int ffmpeg_get_width(const struct ffmpeg *enc);
extern int ffmpeg_get_height(const struct ffmpeg *enc);
extern double ffmpeg_get_fps(const struct ffmpeg *enc);

// simple mode

extern bool ffmpeg_set_overwrite(struct ffmpeg *enc, bool overwrite);
extern bool ffmpeg_set_output(struct ffmpeg *enc, const std::filesystem::path &output);
extern bool ffmpeg_set_video_crf(struct ffmpeg *enc, int crf);
extern bool ffmpeg_set_audio_kbps(struct ffmpeg *enc, double kbps);

extern bool ffmpeg_get_overwrite(const struct ffmpeg *enc);
extern std::filesystem::path ffmpeg_get_output(const struct ffmpeg *enc);
extern int ffmpeg_get_video_crf(const struct ffmpeg *enc);
extern double ffmpeg_get_audio_kbps(const struct ffmpeg *enc);
extern bool ffmpeg_get_output_exists(const struct ffmpeg *enc);

// advanced mode

extern bool ffmpeg_set_advanced(struct ffmpeg *enc, bool advanced);
extern bool ffmpeg_set_twopass(struct ffmpeg *enc, bool twopass);
extern bool ffmpeg_set_pass_1(struct ffmpeg *enc, const std::string &pass1);
extern bool ffmpeg_set_pass_2(struct ffmpeg *enc, const std::string &pass2);

extern bool ffmpeg_get_advanced(const struct ffmpeg *enc);
extern bool ffmpeg_get_twopass(const struct ffmpeg *enc);
extern std::string *ffmpeg_get_pass_1(struct ffmpeg *enc);
extern std::string *ffmpeg_get_pass_2(struct ffmpeg *enc);

extern bool ffmpeg_ready(const struct ffmpeg *enc);
extern bool ffmpeg_recording(const struct ffmpeg *enc);

extern int ffmpeg_record(struct ffmpeg *enc, const std::filesystem::path &soundtrack_file, int pass);
extern int ffmpeg_stop(struct ffmpeg *enc);

extern int ffmpeg_write(struct ffmpeg *enc, const void *data, ssize_t bytes);

extern void ffmpeg_display(struct ffmpeg *enc, bool embedded);
