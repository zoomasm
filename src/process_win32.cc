// zoomasm -- zoom video assembler
// (c) 2019,2020,2021,2022,2023 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-only

#include "process.h"

#include <sstream>
#include <cstring>
#include <cstdlib>

#include <io.h>
#include <fcntl.h>
#include <process.h>

#include "utility.h"

struct process
{
  char *command;
  char **command_line;
  size_t nargs;
  intptr_t pid;
  int stdin_pipe[2];
  int stdout_pipe[2];
  int stderr_pipe[2];
};

static std::string quote(const std::string &arg)
{
  std::ostringstream o;
  bool backslash = false;
  if (arg.find(' ') != std::string::npos || arg.find('\t') != std::string::npos)
  {
    o << '"';
  }
  for (auto c : arg)
  {
    if (c == '"')
    {
      if (backslash)
      {
        o << '\\';
        o << '\\';
      }
      else
      {
        o << '\\';
      }
    }
    o << c;
    backslash = c == '\\';
  }
  if (arg.find(' ') != std::string::npos || arg.find('\t') != std::string::npos)
  {
    if (backslash)
    {
      o << '\\';
    }
    o << '"';
  }
  return o.str();
}

extern struct process *process_open(const std::vector<std::string> &args)
{
  struct process *proc = (struct process *) std::calloc(1, sizeof *proc);
  if (proc)
  {
  proc->nargs = args.size();
  proc->command_line = (char **) std::calloc(args.size() + 1, sizeof(*proc->command_line));
  if (proc->command_line)
  {
  // don't quote EXE, but do quote arguments:
  bool ok = true;
  proc->command = mystrdup(args[0].c_str());
  ok &= proc->command != nullptr;
  for (size_t i = 0; i < args.size(); ++i)
  {
    proc->command_line[i] = mystrdup(quote(args[i]).c_str());
    ok &= proc->command_line[i] != nullptr;
  }
  if (ok)
  {
  // stdin
  if (! (_pipe(proc->stdin_pipe, 65536, O_BINARY | O_NOINHERIT) < 0))
  {
  // stdout
  if (! (_pipe(proc->stdout_pipe, 65536, O_BINARY | O_NOINHERIT) < 0))
  {
  // stderr
  if (! (_pipe(proc->stderr_pipe, 65536, O_BINARY | O_NOINHERIT) < 0))
  {
  // multi-thread race condition here?
  int stdin_old = _dup(0);
  int stdout_old = _dup(1);
  int stderr_old = _dup(2);
  _dup2(proc->stdin_pipe[0], 0);
  _dup2(proc->stdout_pipe[1], 1);
  _dup2(proc->stderr_pipe[1], 2);
  proc->pid = _spawnv(P_NOWAIT, proc->command, proc->command_line);
  _dup2(stdin_old, 0);
  _dup2(stdout_old, 1);
  _dup2(stderr_old, 2);
  _close(stdin_old);
  _close(stdout_old);
  _close(stderr_old);
  _close(proc->stdin_pipe[0]);
  _close(proc->stdout_pipe[1]);
  _close(proc->stderr_pipe[1]);
  if (! (proc->pid < 0))
  {
    // success
    return proc;
  }
  else
  {
    // _spawnv error
  }
  }
  else
  {
    // _pipe stderr error
  }
  }
  else
  {
    // _pipe stdout error
  }
  }
  else
  {
    // _pipe stdin error
  }
  }
  else
  {
    // strdups failed
    if (proc->command)
    {
      std::free(proc->command);
      proc->command = nullptr;
    }
    for (size_t i = 0; i < proc->nargs; ++i)
    {
      if (proc->command_line[i])
      {
        std::free(proc->command_line[i]);
        proc->command_line[i] = nullptr;
      }
    }
  }
  }
  else
  {
    std::free(proc);
  }
  }
  else
  {
    // calloc failed
  }
  return nullptr;
}

extern void process_close(struct process *proc)
{
  if (proc->stdin_pipe[1] >= 0)
  {
    _close(proc->stdin_pipe[1]);
    proc->stdin_pipe[1] = -1;
  }
  if (proc->stdout_pipe[0] >= 0)
  {
    _close(proc->stdout_pipe[0]);
    proc->stdout_pipe[0] = -1;
  }
  if (proc->stderr_pipe[0] >= 0)
  {
    _close(proc->stderr_pipe[0]);
    proc->stderr_pipe[0] = -1;
  }
  if (proc->pid >= 0)
  {
    int termstat;
    _cwait(&termstat, proc->pid, WAIT_CHILD);
    proc->pid = -1;
  }
  if (proc->command_line)
  {
    for (size_t i = 0; i < proc->nargs; ++i)
    {
      if (proc->command_line[i])
      {
        free(proc->command_line[i]);
        proc->command_line[i] = nullptr;
      }
    }
    free(proc->command_line);
    proc->command_line = nullptr;
  }
  if (proc->command)
  {
    free(proc->command);
    proc->command = nullptr;
  }
  free(proc);
}

extern void process_stdin_eof(struct process *proc)
{
  if (proc->stdin_pipe[1] != -1)
  {
    _close(proc->stdin_pipe[1]);
    proc->stdin_pipe[1] = -1;
  }
  else
  {
    // error
  }
}

extern ssize_t process_stdin(struct process *proc, const void *data, ssize_t bytes)
{
  return _write(proc->stdin_pipe[1], data, bytes);
}

extern ssize_t process_stdout(struct process *proc, void *data, ssize_t bytes)
{
  return _read(proc->stdout_pipe[0], data, bytes);
}

extern ssize_t process_stderr(struct process *proc, void *data, ssize_t bytes)
{
  return _read(proc->stderr_pipe[0], data, bytes);
}
