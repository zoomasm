// zoomasm -- zoom video assembler
// (c) 2019,2020,2021,2022,2023 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-only

#include "imgui.h"
#include "imgui_impl_sdl2.h"
#include "imgui_impl_opengl3.h"
#include "imfilebrowser.h"

const float hue_red = 0;
const float hue_orange = 0.1;
const float hue_green = 0.3;
const float hue_blue = 0.6;
const float alpha_strong = 1.0;
const float alpha_blend = 0.7;

static void imgui_theme_from_hue(float hue, float alpha)
{
  const float h = hue, s = 1, v = 1, a = alpha;
  ImVec4 *c = ImGui::GetStyle().Colors;
#define C(tag, dh, ds, dv, a) \
  { \
    float r, g, b; \
    ImGui::ColorConvertHSVtoRGB(h + dh, s * ds, v * dv, r, g, b); \
    c[tag] = ImVec4(r, g, b, a); \
  }
  C(ImGuiCol_Text,                 0, 0.1, 1.0, 1.0)
  C(ImGuiCol_TextDisabled,         0, 0.1, 0.8, 1.0)
  C(ImGuiCol_WindowBg,             0, 0.1, 0.2, a)
  C(ImGuiCol_ChildBg,              0, 0.1, 0.2, a)
  C(ImGuiCol_PopupBg,              0, 0.2, 0.3, a)
  C(ImGuiCol_Border,               0, 0.5, 0.5, 1.0)
  C(ImGuiCol_BorderShadow,         0, 0.4, 0.3, a*a)
  C(ImGuiCol_FrameBg,              0, 0.6, 0.5, a)
  C(ImGuiCol_FrameBgHovered,       0, 0.6, 0.7, 1.0)
  C(ImGuiCol_FrameBgActive,        0, 0.8, 0.7, a)
  C(ImGuiCol_TitleBg,              0, 1.0, 0.5, a)
  C(ImGuiCol_TitleBgActive,        0, 1.0, 1.0, a)
  C(ImGuiCol_TitleBgCollapsed,     0, 0.5, 1.0, a)
  C(ImGuiCol_MenuBarBg,            0, 0.6, 0.5, a)
  C(ImGuiCol_ScrollbarBg,          0, 0.5, 0.2, a)
  C(ImGuiCol_ScrollbarGrab,        0, 0.5, 0.5, a)
  C(ImGuiCol_ScrollbarGrabHovered, 0, 0.5, 0.7, 1.0)
  C(ImGuiCol_ScrollbarGrabActive,  0, 0.7, 0.7, a)
  C(ImGuiCol_CheckMark,            0, 1.0, 1.0, 1.0)
  C(ImGuiCol_SliderGrab,           0, 0.8, 1.0, 1.0)
  C(ImGuiCol_SliderGrabActive,     0, 1.0, 1.0, 1.0)
  C(ImGuiCol_Button,               0, 0.5, 0.5, a)
  C(ImGuiCol_ButtonHovered,        0, 0.5, 0.7, 1.0)
  C(ImGuiCol_ButtonActive,         0, 0.7, 0.7, a)
  C(ImGuiCol_Header,               0, 0.5, 0.5, a)
  C(ImGuiCol_HeaderHovered,        0, 0.5, 0.7, 1.0)
  C(ImGuiCol_HeaderActive,         0, 0.7, 0.7, a)
  C(ImGuiCol_Separator,            0, 0.5, 0.5, a)
  C(ImGuiCol_SeparatorHovered,     0, 0.5, 0.7, 1.0)
  C(ImGuiCol_SeparatorActive,      0, 0.7, 0.7, a)
  C(ImGuiCol_ResizeGrip,           0, 0.5, 0.5, a)
  C(ImGuiCol_ResizeGripHovered,    0, 0.5, 0.7, 1.0)
  C(ImGuiCol_ResizeGripActive,     0, 0.7, 0.7, 1.0)
  C(ImGuiCol_Tab,                  0, 0.5, 0.5, a)
  C(ImGuiCol_TabHovered,           0, 0.7, 0.7, 1.0)
  C(ImGuiCol_TabActive,            0, 0.7, 0.7, a)
  C(ImGuiCol_TabUnfocused,         0, 0.5, 0.5, a*a)
  C(ImGuiCol_TabUnfocusedActive,   0, 0.7, 0.7, a*a)
  C(ImGuiCol_PlotLines,            0, 0.7, 1.0, a)
  C(ImGuiCol_PlotLinesHovered,     0, 0.7, 1.0, 1.0)
  C(ImGuiCol_PlotHistogram,        0, 0.7, 1.0, a)
  C(ImGuiCol_PlotHistogramHovered, 0, 0.7, 1.0, 1.0)
  C(ImGuiCol_TextSelectedBg,       0.5, 0.5, 0.3, 1.0)
  C(ImGuiCol_DragDropTarget,       0.1, 0.5, 0.5, 1.0)
  C(ImGuiCol_NavHighlight,         0.1, 0.5, 0.5, 1.0)
  C(ImGuiCol_NavWindowingHighlight,0.1, 0.5, 0.5, 1.0)
  C(ImGuiCol_NavWindowingDimBg,    0.1, 0.2, 0.2, a)
  C(ImGuiCol_ModalWindowDimBg,     0.1, 0.2, 0.2, a)
#undef C
}

#include <GL/glew.h>
#include <SDL2/SDL.h>

#include <getopt.h>
#include <signal.h>

#include <cmath>

#include "resource.h"
#include "session.h"
#include "audio.h"
#include "input.h"
#include "colour.h"
#include "output.h"
#include "ffmpeg.h"
#include "uniform.h"
#include "utility.h"

bool changed = true;

static bool recording_progress(struct ffmpeg *enc, bool recording, double time, double duration, double start_time, int record_pass)
{
  if (recording)
  {
    ImGui::OpenPopup("Recording");
  }
  ImVec2 center(ImGui::GetIO().DisplaySize.x * 0.5f, ImGui::GetIO().DisplaySize.y * 0.5f);
  ImGui::SetNextWindowPos(center, ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));
  if (ImGui::BeginPopupModal("Recording", NULL, ImGuiWindowFlags_AlwaysAutoResize))
  {
    char overlay[100];
    std::snprintf(overlay, 100, "%.3fs / %.3fs", time, duration);
    ImGui::ProgressBar(float(time / duration), ImVec2(-1,0), &overlay[0]);
    ImGui::Text("Speed: %.1f mspf, %.1f fps", 1000.0 / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
    ImGui::Text("Pass: %d", record_pass);
    {
      double now = get_time();
      double time_so_far = now - start_time;
      double time_total = record_pass == 1
        ? time_so_far / (time / (ffmpeg_get_advanced(enc) && ffmpeg_get_twopass(enc) ? 2 * duration : duration))
        : time_so_far / ((time + duration) / (2 * duration));
      double time_eta = time_total - time_so_far;
      std::string elapsed = format_time(time_so_far);
      std::string eta = format_time(std::max(0.0, time_eta));
      ImGui::Text("Elapsed: %s", elapsed.c_str());
      ImGui::Text("Remaining: %s", eta.c_str());
    }
    ffmpeg_display(enc, true);
    if (ImGui::Button("Stop") || ! recording)
    {
      recording = false;
      ImGui::CloseCurrentPopup();
    }
    if (ImGui::IsItemHovered())
    {
      ImGui::SetTooltip("Stop recording (output file will be truncated).");
    }
    ImGui::EndPopup();
  }
  return recording;
}

static void GLAPIENTRY opengl_debug_callback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam)
{
#ifdef _WIN32
  // don't do anything in Windows
  // in Wine printing happens in multiple threads at once (?) and crashes
  return;
#endif
  (void) userParam;
  (void) length;
  if (source == 33350 && type == 33361 && id == 131185 && severity == 33387)
  {
    // silence extremely verbose message from NVIDIA driver about buffer memory
    return;
  }
  if (source == GL_DEBUG_SOURCE_SHADER_COMPILER && type == GL_DEBUG_TYPE_OTHER && severity == GL_DEBUG_SEVERITY_NOTIFICATION)
  {
    // silence verbose message from Mesa/AMDGPU driver about shader compiling
    return;
  }
  const char *source_name = "unknown";
  const char *type_name = "unknown";
  const char *severity_name = "unknown";
  switch (source)
  {
    case GL_DEBUG_SOURCE_API: source_name = "API"; break;
    case GL_DEBUG_SOURCE_WINDOW_SYSTEM: source_name = "Window System"; break;
    case GL_DEBUG_SOURCE_SHADER_COMPILER: source_name = "Shader Compiler"; break;
    case GL_DEBUG_SOURCE_THIRD_PARTY: source_name = "Third Party"; break;
    case GL_DEBUG_SOURCE_APPLICATION: source_name = "Application"; break;
    case GL_DEBUG_SOURCE_OTHER: source_name = "Other"; break;
  }
  switch (type)
  {
    case GL_DEBUG_TYPE_ERROR: type_name = "Error"; break;
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: type_name = "Deprecated Behaviour"; break;
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR: type_name = "Undefined Behaviour"; break;
    case GL_DEBUG_TYPE_PORTABILITY: type_name = "Portability"; break;
    case GL_DEBUG_TYPE_PERFORMANCE: type_name = "Performance"; break;
    case GL_DEBUG_TYPE_MARKER: type_name = "Marker"; break;
    case GL_DEBUG_TYPE_PUSH_GROUP: type_name = "Push Group"; break;
    case GL_DEBUG_TYPE_POP_GROUP: type_name = "Pop Group"; break;
    case GL_DEBUG_TYPE_OTHER: type_name = "Other"; break;
  }
  switch (severity)
  {
    case GL_DEBUG_SEVERITY_HIGH: severity_name = "High"; break;
    case GL_DEBUG_SEVERITY_MEDIUM: severity_name = "Medium"; break;
    case GL_DEBUG_SEVERITY_LOW: severity_name = "Low"; break;
    case GL_DEBUG_SEVERITY_NOTIFICATION: severity_name = "Notification"; break;
  }
  std::fprintf(stderr, "OpenGL : %s  %s  %s : %d : %s\n", severity_name, type_name, source_name, id, message);
}

static SDL_Window *create_window(int major, int minor)
{
  const size_t output_window_width = 1024;
  const size_t output_window_height = 576;
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, major);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, minor);
  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
  SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
  SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
  SDL_WindowFlags window_flags = (SDL_WindowFlags)(SDL_WINDOW_OPENGL /* | SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI */);
  SDL_Window *window = SDL_CreateWindow("zoomasm", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, output_window_width, output_window_height, window_flags);
  if (! window)
  {
    return nullptr;
  }
  SDL_GLContext gl_context = SDL_GL_CreateContext(window);
  if (! gl_context)
  {
    SDL_DestroyWindow(window);
    return nullptr;
  }
  SDL_GL_MakeCurrent(window, gl_context);
  return window;
}

extern int main(int argc, char **argv)
{
  // parse command line arguments
  bool start_fullscreen = false;
  bool show_gui = true;
  bool do_record = false;
  while (1)
  {
    int option_index = 0;
    static struct option long_options[] =
      { { "help",          no_argument, 0, 'h' }
      , { "version",       no_argument, 0, 'v' }
      , { "source",        no_argument, 0, 'S' }
      , { "fullscreen",    no_argument, 0, 'f' }
      , { "no-fullscreen", no_argument, 0, 'F' }
      , { "gui",           no_argument, 0, 'g' }
      , { "no-gui",        no_argument, 0, 'G' }
      , { "record",        no_argument, 0, 'r' }
      , { "no-record",     no_argument, 0, 'R' }
      , { 0,               0,           0,  0  }
      };
    int opt = getopt_long(argc, argv, "hH?vVSfFgGrR", long_options, &option_index);
    if (opt == -1) break;
    switch (opt)
    {
      case 'h':
      case 'H':
      case '?':
        std::printf(
          "zoomasm -- zoom video assembler\n"
          "Copyright (C) 2019,2020,2021,2022,2023 Claude Heiland-Allen\n"
          "License: GNU AGPLv3\n"
          "\n"
          "Usage:\n"
          "    %s [options...] [session.toml]\n"
          "\n"
          "Options:\n"
          "    -?,-h,-H,--help\n"
          "        this message\n"
          "    -v,-V,--version\n"
          "        output version string\n"
          "    -S,--source\n"
          "        output %s's source code to the current working directory\n"
          "        files written: %s\n"
          "    -f,--fullscreen\n"
          "        start in fullscreen mode (on primary monitor)\n"
          "    -F,--no-fullscreen\n"
          "        start in windowed mode (default)\n"
          "    -g,--gui\n"
          "        start with GUI display visible (default)\n"
          "    -G,--no-gui\n"
          "        start with GUI display hidden\n"
          "    -r,--record\n"
          "        start recording and exit when done\n"
          "    -R,--no-record\n"
          "        start in normal interactive mode (default)\n"
          "\n"
          "Keys:\n"
          "    Space          toggle playback\n"
          "    Backspace      reverse playback\n"
          "    F9             toggle GUI transparency\n"
          "    F10            toggle GUI display\n"
          "    F11            toggle fullscreen\n"
          , argv[0], argv[0], zoomasm_source_7z
          );
        return 0;
      case 'v':
      case 'V':
        std::printf("%s", zoomasm_version().c_str());
        return 0;
      case 'S':
      {
        return ! zoomasm_source(zoomasm_source_7z);
      }
      case 'f':
      {
        start_fullscreen = true;
        break;
      }
      case 'F':
      {
        start_fullscreen = false;
        break;
      }
      case 'g':
      {
        show_gui = true;
        break;
      }
      case 'G':
      {
        show_gui = false;
        break;
      }
      case 'r':
      {
        do_record = true;
        break;
      }
      case 'R':
      {
        do_record = false;
        break;
      }
    }
  }
  if (optind + 1 < argc)
  {
    std::fprintf(stderr, "%s: error: too many arguments\n", argv[0]);
    return 1;
  }
  bool do_load_settings = false;
  std::filesystem::path load_settings;
  if (optind < argc)
  {
    do_load_settings = true;
    load_settings = argv[optind];
  }

#ifndef _WIN32
  signal(SIGPIPE, SIG_IGN);
#endif

  // initialize OpenGL
  if (SDL_Init(SDL_INIT_AUDIO | SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_GAMECONTROLLER) != 0)
  {
    const std::string message = "SDL_Init: " + std::string(SDL_GetError());
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "zoomasm", message.c_str(), nullptr);
    return 1;
  }
  // try OpenGL 4 first
  const char *glsl_version = "#version 400 core";
  SDL_Window *window = create_window(4, 0);
  if (! window)
  {
    // try OpenGL 3
    glsl_version = "#version 330 core";
    window = create_window(3, 3);
  }
  if (! window)
  {
    // give up
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "zoomasm", "could not initialize OpenGL 4.0 or OpenGL 3.3", window);
    return 1;
  }
  SDL_GL_SetSwapInterval(1);
  glewExperimental = GL_TRUE;
  if (glewInit() != GLEW_OK)
  {
    std::fprintf(stderr, "glew error\n");
    return 1;
  }
  glGetError(); // discard common error from glew
  if (glDebugMessageCallback)
  {
    glDebugMessageCallback(opengl_debug_callback, nullptr);
    glEnable(GL_DEBUG_OUTPUT);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
  }

  // set up Dear ImGui context
  IMGUI_CHECKVERSION();
  ImGui::CreateContext();
  ImGui::GetIO().ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
  ImGui::GetIO().IniFilename = nullptr;
  ImGui::StyleColorsDark();
  static float theme_hue_old = hue_orange, theme_hue_new = hue_orange;
  static float theme_alpha_old = alpha_blend, theme_alpha_new = alpha_blend;
  imgui_theme_from_hue(theme_hue_new, theme_alpha_new);
  ImGui_ImplSDL2_InitForOpenGL(window, SDL_GL_GetCurrentContext());
  ImGui_ImplOpenGL3_Init(glsl_version);

  session *ses = session_new();
  assert(ses);
  input *in = input_new();
  assert(in);
  colour *clr = colour_new(glsl_version);
  assert(clr);
  output *out = output_new();
  assert(out);
  ffmpeg *enc = ffmpeg_new();
  assert(enc);
  uniforms *un = uniforms_new();
  assert(un);
  audio *au = audio_new();
  assert(au);

  // set up vertex data
  glDisable(GL_DEPTH_TEST);
  glDisable(GL_BLEND);
  glClearColor(0.5, 0.5, 0.5, 1);
  glClear(GL_COLOR_BUFFER_BIT);

  GLint maximum_texture_size = 0;
  glGetIntegerv(GL_MAX_TEXTURE_SIZE, &maximum_texture_size);

  glUseProgram(0);
  glBindFramebuffer(GL_FRAMEBUFFER, 0);

  int output_frame_count = 0;
  int output_frame_index = 0;
  int record_pass = 0;

  bool session_loaded = false;
  if (do_load_settings)
  {
    bool ok = session_load(load_settings, au, in, clr, un, enc);
    if (! ok)
    {
      std::fprintf(stderr, "load settings error?\n");
    }
    session_loaded = true;
  }

  bool running = true;
  while (running)
  {
    SDL_Event e;
    while (SDL_PollEvent(&e))
    {
      if (e.type == SDL_QUIT)
      {
        running = false;
        break;
      }
      ImGui_ImplSDL2_ProcessEvent(&e);
    }
    if (! running)
    {
      break;
    }
    audio_nrt(au);

    {
      static bool fullscreen = false;
      static bool f11 = false;
      bool toggled = false;
      if (ImGui::IsKeyPressed(ImGuiKey_F11))
      {
        if (! f11)
        {
          toggled = true;
          fullscreen = ! fullscreen;
        }
        f11 = true;
      }
      else
      {
        f11 = false;
      }
      // FIXME actually start in fullscreen instead of windowed mode
      static bool fullscreen_startup = false;
      if (start_fullscreen && ! fullscreen_startup && ! fullscreen)
      {
        toggled = true;
        fullscreen = true;
        fullscreen_startup = true;
      }
      if (toggled)
      {
        if (fullscreen)
        {
          SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
        }
        else
        {
          SDL_SetWindowFullscreen(window, 0);
        }
      }
    }

    int display_w, display_h;
    SDL_GL_GetDrawableSize(window, &display_w, &display_h);
    glViewport(0, 0, display_w, display_h);
    glClear(GL_COLOR_BUFFER_BIT);

    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplSDL2_NewFrame();
    ImGui::NewFrame();

    if (! ffmpeg_recording(enc) && ! ImGui::GetIO().WantCaptureKeyboard)
    {
      static bool space = false;
      if (ImGui::IsKeyPressed(ImGuiKey_Space))
      {
        if (! space)
        {
          audio_set_playing(au, ! audio_get_playing(au));
        }
        space = true;
      }
      else
      {
        space = false;
      }
      static bool backspace = false;
      if (ImGui::IsKeyPressed(ImGuiKey_Backspace))
      {
        if (! backspace)
        {
          audio_set_speed(au, -audio_get_speed(au));
        }
        backspace = true;
      }
      else
      {
        backspace = false;
      }
    }
    // advance timeline
    double output_duration = audio_get_duration(au);
    double output_time = 0;
    if (ffmpeg_recording(enc))
    {
      output_frame_count = ffmpeg_get_fps(enc) * output_duration;
      output_time = output_frame_index / ffmpeg_get_fps(enc);
      audio_set_time(au, output_time);
    }
    else
    {
      output_time = audio_get_time(au);
    }
    // FIXME use realtime FPS when not recording?
    double dt = colour_get_shutter(clr) / ffmpeg_get_fps(enc);
    double time_start = output_time;
    double time_end = time_start + dt;
    double phase_start = uniforms_get_z(un, output_time / output_duration);
    double phase_end = uniforms_get_z(un, (output_time + dt) / output_duration);

    {
      static bool f9 = false;
      if (ImGui::IsKeyPressed(ImGuiKey_F9))
      {
        if (! f9)
        {
          if (theme_alpha_new == alpha_strong)
          {
            theme_alpha_new = alpha_blend;
          }
          else
          {
            theme_alpha_new = alpha_strong;
          }
        }
        f9 = true;
      }
      else
      {
        f9 = false;
      }
    }

    {
      static bool f10 = false;
      if (ImGui::IsKeyPressed(ImGuiKey_F10))
      {
        if (! f10)
        {
          show_gui = ! show_gui;
        }
        f10 = true;
      }
      else
      {
        f10 = false;
      }
    }

    bool reload_shader = false;
    bool new_soundtrack = false;
    if (show_gui)
    {

      ImGui::SetNextWindowPos(ImVec2(10, 10 + ((display_h - 10 * 5) / 4 + 10) * 1), ImGuiCond_FirstUseEver);
      ImGui::SetNextWindowSize(ImVec2(display_w / 2 - 15, (display_h - 10 * 5) / 4), ImGuiCond_FirstUseEver);
      new_soundtrack = audio_display_early(au);

      ImGui::SetNextWindowPos(ImVec2(10, 10 + ((display_h - 10 * 5) / 4 + 10) * 2), ImGuiCond_FirstUseEver);
      ImGui::SetNextWindowSize(ImVec2(display_w / 2 - 15, (display_h - 10 * 5) / 4), ImGuiCond_FirstUseEver);
      input_display_early(in);

      ImGui::SetNextWindowPos(ImVec2(10, 10 + ((display_h - 10 * 5) / 4 + 10) * 3), ImGuiCond_FirstUseEver);
      ImGui::SetNextWindowSize(ImVec2(display_w / 2 - 15, (display_h - 10 * 5) / 4), ImGuiCond_FirstUseEver);
      colour_display_early(clr);

      bool can_record = true;
      if (! (audio_get_duration(au) > 0)) can_record = false;
      if (! input_get_count(in)) can_record = false;
      if (! colour_get_compiled(clr)) can_record = false;
      if (! ffmpeg_ready(enc)) can_record = false;

      ImGui::SetNextWindowPos(ImVec2(display_w / 2 + 5, 10 + ((display_h - 10 * 3) / 2 + 10) * 1), ImGuiCond_FirstUseEver);
      ImGui::SetNextWindowSize(ImVec2(display_w / 2 - 15, (display_h - 10 * 3) / 2), ImGuiCond_FirstUseEver);
      bool emergency_stop = false;
      bool start_recording = output_display_early(enc, input_get_width(in), can_record, colour_get_projection(clr), emergency_stop);
      static bool do_record_started = false;
      if (do_record && ! do_record_started)
      {
        if (can_record)
        {
          start_recording = true;
          do_record_started = true;
        }
        else
        {
          std::fprintf(stderr, "record error\n");
          return 1;
        }
      }
      static double recording_start_time = 0;
      if (can_record && start_recording)
      {
        record_pass = 1;
        audio_set_playing(au, false);
        audio_set_time(au, 0);
        ffmpeg_record(enc, audio_get_soundtrack(au), record_pass);
        recording_start_time = get_time();
        output_frame_index = 0;
      }
      bool still_recording = recording_progress(enc, ffmpeg_recording(enc), output_time, output_duration, recording_start_time, record_pass);
      if (ffmpeg_recording(enc) && ! still_recording)
      {
        ffmpeg_stop(enc);
        // don't quit if do_record, as it was manually cancelled
      }
      if (emergency_stop && ! ffmpeg_recording(enc))
      {
        audio_set_playing(au, false);
      }

      ImGui::SetNextWindowPos(ImVec2(10, 10 + ((display_h - 10 * 5) / 4 + 10) * 0), ImGuiCond_FirstUseEver);
      ImGui::SetNextWindowSize(ImVec2(display_w / 2 - 15, (display_h - 10 * 5) / 4), ImGuiCond_FirstUseEver);
      session_display_early(ses);

    }
    ImGui::SetNextWindowPos(ImVec2(display_w / 2 + 5, 10 + ((display_h - 10 * 3) / 2 + 10) * 0), ImGuiCond_FirstUseEver);
    ImGui::SetNextWindowSize(ImVec2(display_w / 2 - 15, (display_h - 10 * 3) / 2), ImGuiCond_FirstUseEver);
    uniforms_set_keyframes(un, input_get_count(in));
    changed |= uniforms_display(un, au, show_gui);
    if (show_gui)
    {

      new_soundtrack |= audio_display_late(au);
      input_display_late(in);
      reload_shader = colour_display_late(clr);
      output_display_late(enc);
      uniforms_display_late(un, ffmpeg_get_fps(enc), audio_get_duration(au));
      session_loaded = session_display_late(ses, au, in, clr, un, enc);

    }
    if (! ffmpeg_recording(enc))
    {
      if (colour_compile_shader(clr, reload_shader))
      {
        uniforms_set_program(un, colour_get_program(clr));
      }
    }

    ImGui::Render();

    double speed = (phase_end - phase_start) * audio_get_speed(au);
    input_set_phase(in, std::min(phase_start, phase_end), speed);

    if (input_get_count(in) && colour_get_compiled(clr))
    {

      int w = ffmpeg_get_width(enc);
      int h = ffmpeg_get_height(enc);
      output_bind_framebuffer(out);
      glViewport(0, 0, w, h);
      double diag = std::hypot(double(w), double(h));
      if (changed)
      {
        glClear(GL_COLOR_BUFFER_BIT);
        colour_draw(clr, phase_start, phase_end, time_start, time_end, input_get_flip(in), input_get_invert(in) ? input_get_invert_radius(in) : 0.0, w / diag, h / diag, input_get_count(in), input_get_layers(in), w, h, un);
        changed = false;
      }

      glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
      glViewport(0, 0, display_w, display_h);
      glClear(GL_COLOR_BUFFER_BIT);
      if (w / (double) h >= display_w / (double) display_h)
      {
        // wide
        int center_h = round((double) display_w * h / w);
        int center_y = (display_h - center_h) / 2;
        glViewport(0, center_y, display_w, center_h);
      }
      else
      {
        // tall
        int center_w = round((double) display_h * w / h);
        int center_x = (display_w - center_w) / 2;
        glViewport(center_x, 0, center_w, display_h);
      }
      colour_blit(clr);

      if (ffmpeg_recording(enc) && output_frame_count)
      {
        if (output_record(enc, output_frame_index, output_frame_count <= output_frame_index))
        {
          // FIXME display error message in GUI
          std::fprintf(stderr, "FFMPEG ERROR: could not record frame\n");
          ffmpeg_stop(enc);
          if (do_record)
          {
            return 1;
          }
        }
        output_frame_index++;
        if (output_frame_count + output_get_pipeline() <= output_frame_index)
        {
          ffmpeg_stop(enc);
          if (ffmpeg_get_advanced(enc) && ffmpeg_get_twopass(enc) && record_pass == 1)
          {
            record_pass = 2;
            audio_set_playing(au, false);
            audio_set_time(au, 0);
            ffmpeg_record(enc, audio_get_soundtrack(au), record_pass);
            output_frame_index = 0;
          }
          else
          {
            record_pass = 0;
            if (do_record)
            {
              return 0;
            }
          }
        }
      }
    }

    glViewport(0, 0, display_w, display_h);
    if (glDebugMessageCallback)
    {
      glDisable(GL_DEBUG_OUTPUT);
    }
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
    if (glDebugMessageCallback)
    {
      glEnable(GL_DEBUG_OUTPUT);
    }
    if (ffmpeg_recording(enc))
    {
      const double recording_target_fps = 20;
      static double then = 0;
      double now = get_time();
      if (now - then >= 1.0 / recording_target_fps)
      {
        then = now;
        SDL_GL_SwapWindow(window);
      }
    }
    else
    {
      SDL_GL_SwapWindow(window);
    }

    if (ffmpeg_recording(enc))
    {
      theme_hue_new = hue_red;
    }
    else if (audio_get_playing(au))
    {
      theme_hue_new = hue_green;
    }
    else if (input_get_count(in) && colour_get_compiled(clr) && audio_get_duration(au) > 0 && ffmpeg_ready(enc))
    {
      theme_hue_new = hue_blue;
    }
    else
    {
      theme_hue_new = hue_orange;
    }
    if (theme_hue_old != theme_hue_new || theme_alpha_old != theme_alpha_new)
    {
      if (show_gui)
      {
        imgui_theme_from_hue(theme_hue_old = theme_hue_new, theme_alpha_old = theme_alpha_new);
      }
    }

    if (! glDebugMessageCallback)
    {
      int err = 0;
      while ((err = glGetError()))
      {
        std::fprintf(stderr, "OpenGL error %d\n", err);
      }
    }
  }

  audio_delete(au);
  uniforms_delete(un);
  ffmpeg_delete(enc);
  output_delete(out);
  colour_delete(clr);
  input_delete(in);
  session_delete(ses);

  // cleanup and exit
  ImGui_ImplOpenGL3_Shutdown();
  ImGui_ImplSDL2_Shutdown();
  ImGui::DestroyContext();
  SDL_GL_DeleteContext(SDL_GL_GetCurrentContext());
  SDL_DestroyWindow(window);
  SDL_Quit();
  return 0;
}
