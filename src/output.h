#pragma once
// zoomasm -- zoom video assembler
// (c) 2019,2020,2021,2022,2023 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-only

#include "ffmpeg.h"

struct output;

extern struct output *output_new(void);
extern void output_delete(struct output *out);

extern void output_bind_framebuffer(const struct output *out);

extern bool output_display_early(struct ffmpeg *enc, int keyframe_width, bool can_record, int projection, bool &emergency_stop);
extern void output_display_late(struct ffmpeg *enc);
extern int output_record(struct ffmpeg *enc, int output_frame_index, bool draining);
extern int output_get_pipeline(void);
