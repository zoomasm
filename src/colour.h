#pragma once
// zoomasm -- zoom video assembler
// (c) 2019,2020,2021,2022,2023 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-only

#include <filesystem>

#include <GL/glew.h>

struct colour;

extern struct colour *colour_new(const char *version);
extern void colour_delete(struct colour *clr);

extern bool colour_get_compiled(const struct colour *clr);

extern void colour_display_early(struct colour *clr);
extern bool colour_display_late(struct colour *clr);
extern bool colour_compile_shader(struct colour *clr, bool reload);

extern void colour_draw(struct colour *clr, double phase_start, double phase_end, double time_start, double time_end, bool flip, double invert, double aspect_x, double aspect_y, int keyframe_count, int NUMBER_OF_LAYERS, int width, int height, const struct uniforms *un);
extern void colour_blit(struct colour *clr);

extern std::filesystem::path colour_get_colour(const struct colour *clr);
extern bool colour_set_colour(struct colour *clr, const std::filesystem::path &file);

extern bool colour_get_watch(const struct colour *clr);
extern bool colour_set_watch(struct colour *clr, bool watch);

extern int colour_get_samples(const struct colour *clr);
extern bool colour_set_samples(struct colour *clr, int samples);

extern double colour_get_shutter(const struct colour *clr);
extern bool colour_set_shutter(struct colour *clr, double shutter);

extern int colour_get_projection(const struct colour *clr);
extern bool colour_set_projection(struct colour *clr, int projection);

extern double colour_get_rotation_angle(const struct colour *clr);
extern bool colour_set_rotation_angle(struct colour *clr, double angle);

extern void colour_get_rotation_axis(const struct colour *clr, double *x, double *y, double *z);
extern bool colour_set_rotation_axis(struct colour *clr, double x, double y, double z);

extern GLuint colour_get_program(const struct colour *clr);
