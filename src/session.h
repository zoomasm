#pragma once
// zoomasm -- zoom video assembler
// (c) 2019,2020,2021,2022,2023 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-only

#include <string>
#include <filesystem>

struct session;

extern struct session *session_new(void);
extern void session_delete(struct session *ses);

extern void session_display_early(struct session *ses);
extern bool session_display_late(struct session *ses, struct audio *au, struct input *in, struct colour *clr, struct uniforms *un, struct ffmpeg *enc);

extern bool session_load(const std::filesystem::path &file, struct audio *au, struct input *in, struct colour *clr, struct uniforms *un, struct ffmpeg *enc);
extern bool session_save(const std::filesystem::path &file, const struct audio *au, const struct input *in, const struct colour *clr, const struct uniforms *un, struct ffmpeg *enc);

extern std::string zoomasm_version(void);

extern const char *zoomasm_source_7z;
extern bool zoomasm_source(const std::filesystem::path &file);
