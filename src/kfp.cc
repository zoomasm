// zoomasm -- zoom video assembler
// (c) 2021,2022,2023 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-only

#include "kfp.h"
#include "resource.h"

#include <cmath>
#include <sstream>
#include <string>
#include <vector>

static std::string glsl_unescape(const std::string &s)
{
  std::ostringstream o;
  for (auto p = s.begin(); p != s.end(); ++p)
  {
    switch (*p)
    {
      case '\\':
      {
        ++p;
        if (p != s.end())
        {
          switch (*p)
          {
            case '\\': o << '\\'; break;
            case ' ': o << ' '; break;
            case 't': o << '\t'; break;
            case 'r': o << '\r'; break;
            case 'n': o << '\n'; break;
            default: o << *p; break; // FIXME
          }
        }
        else
        {
          // FIXME
        }
        break;
      }
      default: o << *p; break;
    }
  }
  return o.str();
}

static double clamp(double x, double lo, double hi)
{
  return std::min(std::max(x, lo), hi);
}

static double srgb2linear(double c)
{
  c = clamp(c, 0.0, 1.0);
  const double a = 0.055;
  if (c <= 0.04045)
    return c / 12.92;
  else
    return std::pow((c + a) / (1.0 + a), 2.4);
}

struct kfp
{
  double IterationsBias;
  int ColorMethod;
  float ColorOffset; // KF currently limits this to int
  int Differences;
  bool Flat;
  std::vector<uint8_t> InteriorColor; // length is 3
  bool InverseTransition;
  float IterDiv;
  uint64_t Iterations;      // FIXME unimplemented
  uint64_t IterationsMax;   // FIXME unimplemented
  uint64_t IterationsMin;   // FIXME unimplemented
  float JitterScale;        // FIXME unimplemented
  uint32_t JitterSeed;      // FIXME unimplemented
  bool JitterShape;         // FIXME unimplemented
  bool MultiWavesEnabled;
  int MultiWavesCount;
#define MultiWavesCountMax 32
  bool MultiWavesBlend;
  std::vector<GLint> MultiWaves; // length is a multiple of 3
  std::vector<uint8_t> Palette; // length is a multiple of 3
  float PhaseColorStrength;
  bool ShowGlitches;
  bool Slopes;
  float SlopeAngle;
  float SlopePower;
  float SlopeRatio;
  bool Smooth;
  bool TextureEnabled;      // FIXME unimplemented
  std::string TextureFile;  // FIXME unimplemented
  std::vector<uint8_t> Texture; // FIXME unimplemented
  float TextureMerge;       // FIXME unimplemented
  float TexturePower;       // FIXME unimplemented
  float TextureRatio;       // FIXME unimplemented
  float ZoomLog2;
  bool sRGB;
  bool UseOpenGL;
  std::string GLSL;
};

extern struct kfp *kfp_new(const std::string &s, double IterationsBias)
{
  struct kfp *p = new kfp();
  p->IterationsBias = IterationsBias;
  // split into lines at "\r\n"
  // then split into words at ": "
  // some fields split further on "," and "\t"
  auto i = s.begin();
  while (i != s.end())
  {
    std::ostringstream key;
    while (i != s.end() && *i != ':')
    {
      key << *i;
      i++;
    }
    if (i != s.end())
    {
      i++;
    }
    if (i != s.end() && *i == ' ')
    {
      i++;
    }
    else
    {
      // format error
    }
    const std::string k = key.str();
    std::ostringstream value;
    while (i != s.end() && *i != '\r')
    {
      value << *i;
      i++;
    }
    if (i != s.end())
    {
      i++;
    }
    if (i != s.end() && *i == '\n')
    {
      i++;
    }
    else
    {
      // format error
    }
    const std::string v = value.str();
#define BEGIN if (false) { }
#define END else { }
#define INT(f) else if (k == #f) { p->f = std::atoll(v.c_str()); }
#define INT2(f,g) else if (k == #g) { p->f = std::atoll(v.c_str()); }
#define BOOL INT
#define BOOL2 INT2
#define FLOAT(f) else if (k == #f) { p->f = std::atof(v.c_str()); }
#define FLOAT2(f,g) else if (k == #g) { p->f = std::atof(v.c_str()); }
#define STRING(f) else if (k == #f) { p->f = v; }
#define INTS(f,g) else if (k == #g) { \
      auto j = v.begin(); \
      while (j != v.end()) \
      { \
        std::ostringstream number; \
        while (j != v.end() && *j != ',' && *j != '\t') \
        { \
          number << *j; \
          j++; \
        } \
        p->f.push_back(std::atoll(number.str().c_str())); \
        if (j != v.end()) \
        { \
          j++; \
        } \
        else \
        { \
          /* format error */ \
        } \
      } \
    }
    BEGIN
    INT(ColorMethod)
    FLOAT(ColorOffset)
    INT(Differences)
    BOOL(Flat)
    INTS(InteriorColor, InteriorColor)
    BOOL(InverseTransition)
    FLOAT(IterDiv)
    INT(Iterations)
    INT(IterationsMax)
    INT(IterationsMin)
    FLOAT(JitterScale)
    INT(JitterSeed)
    BOOL(JitterShape)
    BOOL2(MultiWavesEnabled, MultiColor)
    INT(MultiWavesCount)
    BOOL2(MultiWavesBlend, BlendMC)
    INTS(MultiWaves, MultiColors)
    INTS(Palette, Colors)
    FLOAT2(PhaseColorStrength, ColorPhaseStrength)
    BOOL(ShowGlitches)
    BOOL(Slopes)
    FLOAT(SlopeAngle)
    FLOAT(SlopePower)
    FLOAT(SlopeRatio)
    BOOL(Smooth)
    BOOL(TextureEnabled)
    FLOAT(TextureMerge)
    FLOAT(TexturePower)
    FLOAT(TextureRatio)
    STRING(TextureFile)
    BOOL(UseOpenGL)
    STRING(GLSL)
    BOOL2(sRGB, UseSRGB)
    END
#undef BEGIN
#undef END
#undef INT
#undef BOOL
#undef FLOAT
#undef INT2
#undef BOOL2
#undef FLOAT2
#undef INTS
  }
  while (p->InteriorColor.size() < 3)
  {
    p->InteriorColor.push_back(0);
  }
  p->MultiWavesCount = std::min(int(p->MultiWaves.size() / 3), MultiWavesCountMax);
  p->GLSL = glsl_unescape(p->GLSL);
  return p;
}

extern void kfp_delete(struct kfp *p)
{
  delete p;
}

extern std::string kfp_get_glsl(struct kfp *p)
{
  return p->GLSL;
}

// expects program to be bound
// expects active texture unit to have a texture 1D bound for the palette
extern void kfp_set_uniforms(GLuint program, const struct kfp *p)
{
  glUniform1f(glGetUniformLocation(program, "IterationsBias"), p->IterationsBias);
#define INT(f) glUniform1i(glGetUniformLocation(program, "KFP_" #f), p->f);
#define BOOL INT
#define FLOAT(f) glUniform1f(glGetUniformLocation(program, "KFP_" #f), p->f);
  INT(ColorMethod)
  FLOAT(ColorOffset)
  INT(Differences)
  BOOL(Flat)
  {
    GLfloat c[3];
    c[0] = p->InteriorColor[0] / 255.0f;
    c[1] = p->InteriorColor[1] / 255.0f;
    c[2] = p->InteriorColor[2] / 255.0f;
    if (p->sRGB)
    {
      c[0] = srgb2linear(c[0]);
      c[1] = srgb2linear(c[1]);
      c[2] = srgb2linear(c[2]);
    }
    glUniform3f(glGetUniformLocation(program, "KFP_InteriorColor"), c[2], c[1], c[0]);
  }
  BOOL(InverseTransition)
  FLOAT(IterDiv)
  INT(Iterations)
  glUniform2ui(glGetUniformLocation(program, "KFP_IterationsMax"), p->IterationsMax, 0);
  glUniform2ui(glGetUniformLocation(program, "KFP_IterationsMin"), p->IterationsMax, 0);
  FLOAT(JitterScale)
  glUniform1ui(glGetUniformLocation(program, "KFP_JitterSeed"), p->JitterSeed);
  BOOL(JitterShape)
  BOOL(MultiWavesEnabled)
  INT(MultiWavesCount)
  BOOL(MultiWavesBlend)
  glUniform3iv(glGetUniformLocation(program, "KFP_Multiwaves"), p->MultiWavesCount, &p->MultiWaves[0]);
  glTexImage1D(GL_TEXTURE_1D, 0, p->sRGB ? GL_SRGB : GL_RGB, p->Palette.size() / 3, 0, GL_BGR, GL_UNSIGNED_BYTE, &p->Palette[0]);
  glUniform1i(glGetUniformLocation(program, "KFP_Palette"), TEXTURE_PALETTE);
  FLOAT(PhaseColorStrength)
  BOOL(ShowGlitches)
  BOOL(Slopes)
  {
    double rad = 1.7453292519943295e-2 * p->SlopeAngle;
    glUniform2f(glGetUniformLocation(program, "KFP_SlopeDir"), std::cos(rad), std::sin(rad));
  }
  FLOAT(SlopePower)
  FLOAT(SlopeRatio)
  BOOL(Smooth)
  BOOL(TextureEnabled)
  FLOAT(TextureMerge)
  FLOAT(TexturePower)
  FLOAT(TextureRatio)
  BOOL(sRGB)
#undef INT
#undef BOOL
#undef FLOAT
}
