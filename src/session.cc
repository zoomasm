// zoomasm -- zoom video assembler
// (c) 2019,2020,2021,2022,2023 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-only

#include "session.h"

#include <fstream>
#include "toml.hpp"

#include "imgui.h"
#include "imfilebrowser.h"

// these are just for version information
#include <GL/glew.h>
#include <SDL2/SDL.h>
#include <zlib.h>
#include <OpenEXRConfig.h>
#include <fftw3.h>

#include <iostream>
using namespace std::string_view_literals;

#include "audio.h"
#include "input.h"
#include "colour.h"
#include "output.h"
#include "ffmpeg.h"
#include "uniform.h"

#include "resource.h"

static toml::table audio_save_session(const struct audio *au)
{
  toml::table t;
  switch (audio_get_type(au))
  {
    case audio_type_silence:
    {
      t.insert("type", "silence");
      t.insert("duration", audio_get_duration(au));
      break;
    }
    case audio_type_soundtrack:
    {
      t.insert("type", "soundtrack");
      t.insert("path", audio_get_soundtrack(au).string());
      break;
    }
  }
  t.insert("time", audio_get_time(au));
  t.insert("speed", audio_get_speed(au));
  t.insert("volume", audio_get_volume(au));
  t.insert("mute", audio_get_mute(au));
  t.insert("loop", audio_get_loop(au));
  t.insert("playing", audio_get_playing(au));
  return t;
}

static bool audio_load_session(struct audio *au, const toml::table &t)
{
  bool ok = true;
  if (t["type"].is_string() && std::string(*t["type"].as_string()) == "silence")
  {
    ok &= t["duration"].is_floating_point() && audio_set_silence(au, double(*t["duration"].as_floating_point()));
  }
  else if (t["type"].is_string() && std::string(*t["type"].as_string()) == "soundtrack")
  {
    ok &= t["path"].is_string() && audio_set_soundtrack(au, std::filesystem::path(std::string(*t["path"].as_string())));
  }
  else
  {
    ok = false;
  }
  ok &= t["time"].is_floating_point() && audio_set_time(au, double(*t["time"].as_floating_point()));
  ok &= t["speed"].is_floating_point() && audio_set_speed(au, double(*t["speed"].as_floating_point()));
  ok &= t["volume"].is_floating_point() && audio_set_volume(au, double(*t["volume"].as_floating_point()));
  ok &= t["mute"].is_boolean() && audio_set_mute(au, bool(*t["mute"].as_boolean()));
  ok &= t["loop"].is_boolean() && audio_set_loop(au, bool(*t["loop"].as_boolean()));
  ok &= t["playing"].is_boolean() && audio_set_playing(au, bool(*t["playing"].as_boolean()));
  return ok;
}

static toml::table input_save_session(const struct input *in)
{
  toml::table t;
  t.insert("input", input_get_input(in).string());
  t.insert("vram_override", input_get_vram_override(in));
  toml::array channels;
  for (auto channel : input_get_wanted_channels(in))
  {
    channels.push_back(channel);
  }
  t.insert("channels", channels);
  t.insert("reverse", input_get_reverse(in));
  t.insert("flip", input_get_flip(in));
  t.insert("invert", input_get_invert(in));
  t.insert("layers", input_get_layers(in));
  return t;
}

static bool input_load_session(struct input *in, const toml::table &t)
{
  bool ok = true;
  input_set_input(in, std::filesystem::path()); // prevent repeated loads
  ok &= t["invert"].is_boolean() && input_set_invert(in, bool(*t["invert"].as_boolean()));
  ok &= t["flip"].is_boolean() && input_set_flip(in, bool(*t["flip"].as_boolean()));
  ok &= t["reverse"].is_boolean() && input_set_reverse(in, bool(*t["reverse"].as_boolean()));
  ok &= t["vram_override"].is_boolean() && input_set_vram_override(in, bool(*t["vram_override"].as_boolean()));
  if (t["channels"].is_array() && t["channels"].as_array()->is_homogeneous(toml::node_type::string))
  {
    std::set<std::string> want_channels;
    for (auto &channel : *t["channels"].as_array())
    {
      want_channels.insert(std::string(*channel.as_string()));
    }
    ok &= input_set_wanted_channels(in, want_channels);
  }
  else
  {
    ok = false;
  }
  ok &= t["input"].is_string() && input_set_input(in, std::filesystem::path(std::string(*t["input"].as_string())));
  ok &= input_set_layers(in, t["layers"].value_or(16));
  return ok;
}

static toml::table colour_save_session(const struct colour *clr)
{
  toml::table t;
  t.insert("colour", colour_get_colour(clr).string());
  t.insert("watch", colour_get_watch(clr));
  t.insert("samples", colour_get_samples(clr));
  t.insert("shutter", colour_get_shutter(clr));
  t.insert("projection", colour_get_projection(clr) == projection_rectangular2d ? "2D" : "360");
  t.insert("angle", colour_get_rotation_angle(clr));
  if (colour_get_projection(clr) == projection_equirectangular360)
  {
    double x = 0, y = 0, z = 0;
    colour_get_rotation_axis(clr, &x, &y, &z);
    toml::array axis;
    axis.push_back(x);
    axis.push_back(y);
    axis.push_back(z);
    t.insert("axis", axis);
  }
  return t;
}

static bool colour_load_session(struct colour *clr, const toml::table &t)
{
  bool ok = true;
  ok &= t["colour"].is_string() && colour_set_colour(clr, std::filesystem::path(std::string(*t["colour"].as_string())));
  ok &= t["watch"].is_boolean() && colour_set_watch(clr, bool(*t["watch"].as_boolean()));
  ok &= t["samples"].is_integer() && colour_set_samples(clr, int64_t(*t["samples"].as_integer()));
  ok &= t["shutter"].is_floating_point() && colour_set_shutter(clr, double(*t["shutter"].as_floating_point()));
  std::string_view proj = t["projection"].value_or("2D"sv);
  if (proj == "360")
  {
    colour_set_projection(clr, projection_equirectangular360);
  }
  else
  {
    colour_set_projection(clr, projection_rectangular2d);
  }
  double a = t["angle"].value_or(0.0);
  colour_set_rotation_angle(clr, a);
  double x = t["axis"][0].value_or(0.0);
  double y = t["axis"][1].value_or(0.0);
  double z = t["axis"][2].value_or(0.0);
  colour_set_rotation_axis(clr, x, y, z);
  return ok;
}

static toml::table output_save_session(struct ffmpeg *enc)
{
  toml::table t;
  t.insert("ffmpeg", ffmpeg_get_ffmpeg(enc).string());
  t.insert("width", ffmpeg_get_width(enc));
  t.insert("height", ffmpeg_get_height(enc));
  t.insert("fps", ffmpeg_get_fps(enc));
  t.insert("advanced", ffmpeg_get_advanced(enc));
  t.insert("twopass", ffmpeg_get_twopass(enc));
  t.insert("pass1", *ffmpeg_get_pass_1(enc));
  t.insert("pass2", *ffmpeg_get_pass_2(enc));
  t.insert("output", ffmpeg_get_output(enc).string());
  t.insert("overwrite", ffmpeg_get_overwrite(enc));
  t.insert("video_crf", ffmpeg_get_video_crf(enc));
  t.insert("audio_kbps", ffmpeg_get_audio_kbps(enc));
  return t;
}

static bool output_load_session(struct ffmpeg *enc, const toml::table &t)
{
  bool ok = true;
  ok &= t["ffmpeg"].is_string() && ffmpeg_set_ffmpeg(enc, std::filesystem::path(std::string(*t["ffmpeg"].as_string())));
  ok &= t["width"].is_integer() && ffmpeg_set_width(enc, int64_t(*t["width"].as_integer()));
  ok &= t["height"].is_integer() && ffmpeg_set_height(enc, int64_t(*t["height"].as_integer()));
  ok &= t["fps"].is_floating_point() && ffmpeg_set_fps(enc, double(*t["fps"].as_floating_point()));
  bool oka = t["advanced"].is_boolean() && ffmpeg_set_advanced(enc, bool(*t["advanced"].as_boolean()));
  if (oka && ffmpeg_get_advanced(enc))
  {
    ok &= t["twopass"].is_boolean() && ffmpeg_set_twopass(enc, bool(*t["twopass"].as_boolean()));
    ok &= t["pass1"].is_string() && ffmpeg_set_pass_1(enc, std::string(*t["pass1"].as_string()));
    ok &= t["pass2"].is_string() && ffmpeg_set_pass_2(enc, std::string(*t["pass2"].as_string()));
  }
  else
  {
    ok &= t["output"].is_string() && ffmpeg_set_output(enc, std::filesystem::path(std::string(*t["output"].as_string())));
    ok &= t["overwrite"].is_boolean() && ffmpeg_set_overwrite(enc, bool(*t["overwrite"].as_boolean()));
    ok &= t["video_crf"].is_integer() && ffmpeg_set_video_crf(enc, int64_t(*t["video_crf"].as_integer()));
    ok &= t["audio_kbps"].is_floating_point() && ffmpeg_set_audio_kbps(enc, double(*t["audio_kbps"].as_floating_point()));
  }
  return ok;
}

struct session
{
  ImGui::FileBrowser load;
  ImGui::FileBrowser save;
  ImGui::FileBrowser source;
  std::filesystem::path path;
  session(void)
  : load(ImGuiFileBrowserFlags_CloseOnEsc)
  , save(ImGuiFileBrowserFlags_CloseOnEsc | ImGuiFileBrowserFlags_EnterNewFilename | ImGuiFileBrowserFlags_CreateNewDir)
  , source(ImGuiFileBrowserFlags_CloseOnEsc | ImGuiFileBrowserFlags_SelectDirectory | ImGuiFileBrowserFlags_CreateNewDir)
  {
    load.SetTitle("Load settings");
    load.SetTypeFilters({ ".toml" });
    save.SetTitle("Save settings");
    save.SetTypeFilters({ ".toml" });
    source.SetTitle("Export source code");
    source.SetTypeFilters({ ".7z" });
  }
};

extern struct session *session_new(void)
{
  return new session();
}

extern void session_delete(struct session *ses)
{
  if (ses)
  {
    delete ses;
  }
}

extern void session_display_early(struct session *ses)
{
  ImGui::Begin("Session", nullptr, ImGuiWindowFlags_HorizontalScrollbar);
  if (ImGui::Button("Save"))
  {
    ses->save.Open();
  }
  if (ImGui::IsItemHovered())
  {
    ImGui::SetTooltip("Save session to TOML text file.");
  }
  ImGui::SameLine();
  if (ImGui::Button("Load"))
  {
    ses->load.Open();
  }
  if (ImGui::IsItemHovered())
  {
    ImGui::SetTooltip("Load session from TOML text file.");
  }
  ImGui::SameLine();
  ImGui::Text("%s", ses->path.string().c_str());
  static bool version_popup = false;
  if (ImGui::Button("Version"))
  {
    version_popup = true;
  }
  if (ImGui::IsItemHovered())
  {
    ImGui::SetTooltip("Display version information.");
  }
  if (version_popup)
  {
    ImGui::OpenPopup("Version");
    ImVec2 center(ImGui::GetIO().DisplaySize.x * 0.5f, ImGui::GetIO().DisplaySize.y * 0.5f);
    ImGui::SetNextWindowPos(center, ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));
    if (ImGui::BeginPopupModal("Version", NULL, ImGuiWindowFlags_AlwaysAutoResize))
    {
      ImGui::Text("%s", zoomasm_version().c_str());
      if (ImGui::Button("Ok"))
      {
        version_popup = false;
      }
      ImGui::EndPopup();
    }
  }
  ImGui::SameLine();
  if (ImGui::Button("Source"))
  {
    ses->source.Open();
  }
  if (ImGui::IsItemHovered())
  {
    ImGui::SetTooltip("Export the source code archive for this version of zoomasm.");
  }
  ImGui::End();
}

extern bool session_display_late(struct session *ses, struct audio *au, struct input *in, struct colour *clr, struct uniforms *un, struct ffmpeg *enc)
{
  bool session_loaded = false;
  ses->save.Display();
  if (ses->save.HasSelected())
  {
    std::filesystem::path path = ses->save.GetSelected();
    ses->save.ClearSelected();
    if (session_save(path, au, in, clr, /*tl*/un, enc))
    {
      ses->path = path;
    }
    else
    {
      ses->path.clear();
    }
  }
  ses->load.Display();
  if (ses->load.HasSelected())
  {
    std::filesystem::path path = ses->load.GetSelected();
    ses->load.ClearSelected();
    if (session_load(path, au, in, clr, /*tl*/un, enc))
    {
      session_loaded = true;
      ses->path = path;
    }
    else
    {
      ses->path.clear();
    }
  }
  ses->source.Display();
  if (ses->source.HasSelected())
  {
    std::filesystem::path path = ses->source.GetSelected();
    ses->source.ClearSelected();
    if (! zoomasm_source(path / zoomasm_source_7z))
    {
      // FIXME handle error
    }
  }
  return session_loaded;
}

extern bool session_load(const std::filesystem::path &file, struct audio *au, struct input *in, struct colour *clr, struct uniforms *un, struct ffmpeg *enc)
{
  bool ok = true;
  try
  {
    toml::table t = toml::parse_file(file.string());
    ok &= t["audio"].is_table() && audio_load_session(au, *t["audio"].as_table());
    ok &= t["input"].is_table() && input_load_session(in, *t["input"].as_table());
    ok &= t["colour"].is_table() && colour_load_session(clr, *t["colour"].as_table());
    uniforms_set_program(un, colour_get_program(clr));
    uniforms_set_keyframes(un, input_get_count(in));
    ok &= t["timeline"].is_array() && uniforms_load_session(un, *t["timeline"].as_array());
    ok &= t["output"].is_table() && output_load_session(enc, *t["output"].as_table());
  }
  catch (const toml::parse_error& e)
  {
    std::cerr << "parse error:\n" << e << "\n";
    ok = false;
  }
  return ok;
}

extern bool session_save(const std::filesystem::path &file, const struct audio *au, const struct input *in, const struct colour *clr, const struct uniforms *un, struct ffmpeg *enc)
{
  bool ok = true;
  try
  {
    toml::table t;
    t.insert("version", ZOOMASM_VERSION_STRING);
    t.insert("audio", audio_save_session(au));
    t.insert("input", input_save_session(in));
    t.insert("colour", colour_save_session(clr));
    t.insert("timeline", uniforms_save_session(un));
    t.insert("output", output_save_session(enc));
    std::ofstream stream(file);
    stream << t << "\n";
  }
  catch (const std::exception &e)
  {
    std::cerr << "session save error: " << e.what() << "\n";
    ok = false;
  }
  return ok;
}
extern std::string zoomasm_version(void)
{
  std::ostringstream out;
  out << "zoomasm version " << ZOOMASM_VERSION_STRING << "\n";
#ifdef __GNUC__
  out << "g++ version " << __GNUC__ << "." << __GNUC_MINOR__ << "." << __GNUC_PATCHLEVEL__ << "\n";
#else
  out << "compiler version unknown\n";
#endif
  out << "zlib version " << zlib_version << "\n";
  out << "openexr version " << OPENEXR_VERSION_STRING << "\n";
  SDL_version compiled;
  SDL_version linked;
  SDL_VERSION(&compiled);
  SDL_GetVersion(&linked);
  out << "sdl2 version " << compiled.major << "." << compiled.minor << "." << compiled.patch;
  if (linked.major != compiled.major || linked.minor != compiled.major || linked.patch != compiled.patch)
  {
    out << " (using " << linked.major << "." << linked.minor << "." << linked.patch << ")";
  }
  out << "\n";
  out << "glew version " << GLEW_VERSION_STRING << "\n";
  out << "imgui version " << IMGUI_VERSION << " (" << IMGUI_GIT_VERSION_STRING << ")\n";
  out << "imgui-filebrowser version git (" << IMGUI_FILEBROWSER_GIT_VERSION_STRING << ")\n";
  out << "tomlplusplus version " << TOML_LANG_MAJOR << "." << TOML_LANG_MINOR << "." << TOML_LANG_PATCH << " (" << TOMLPLUSPLUS_GIT_VERSION_STRING << ")\n";
  out << "fftw version " << fftw_version << "\n";
  return out.str();
}

const char *zoomasm_source_7z = "zoomasm-" ZOOMASM_VERSION_STRING ".7z";

#if DEFAULT_LINKAGE_HAS_UNDERSCORE
extern const char  binary_zoomasm_source_7z_start[],  binary_zoomasm_source_7z_end[];
#else
extern const char _binary_zoomasm_source_7z_start[], _binary_zoomasm_source_7z_end[];
#endif

static int write_file(const char *name, const char *start, const char *end)
{
  std::printf("writing '%s'... ", name);
  std::fflush(stdout);
  FILE *file = std::fopen(name, "wxb");
  if (! file)
  {
    std::printf("FAILED\n");
    return 0;
  }
  int ok = (std::fwrite(start, end - start, 1, file) == 1);
  std::fclose(file);
  if (ok)
    std::printf("ok\n");
  else
    std::printf("FAILED\n");
  return ok;
}

extern bool zoomasm_source(const std::filesystem::path &file)
{
#if DEFAULT_LINKAGE_HAS_UNDERSCORE
  return write_file(file.string().c_str(),  binary_zoomasm_source_7z_start,  binary_zoomasm_source_7z_end);
#else
  return write_file(file.string().c_str(), _binary_zoomasm_source_7z_start, _binary_zoomasm_source_7z_end);
#endif
}
