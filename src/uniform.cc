// zoomasm -- zoom video assembler
// (c) 2021,2022,2023 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-only

#include "uniform.h"
#include "audio.h"

#include <algorithm>
#include <climits>
#include <cmath>
#include <cstring>
#include <set>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>

#include "imgui.h"
#include "imfilebrowser.h"

class invalid_data : public std::exception
{
  const char *msg;
public:
  invalid_data(const char *msg = "invalid data") : msg(msg) { }
  virtual const char *what() const noexcept override
  {
    return msg;
  }
};

enum interpolation_mode
{
  interpolation_step = 0,
  interpolation_linear = 1,
  interpolation_cubic = 2
};

const char *interpolation_mode_string[3] = { "Step", "Linear", "Cubic" };

struct uniform_type
{
  GLenum type;
  GLint size;
};

union uniform_value
{
  float float_;
  GLint int_;
  bool bool_;
  GLint sampler;
  // GL 3.0
  GLuint uint_;
  // GL 4.1
  double double_;
  // GL 4.2
  GLint image;
  GLint atomic_uint;
};

std::set<std::string> uniform_internal
{ "N0_0"
, "NF_0"
, "DEX_0"
, "DEY_0"
, "offset"
, "phases"
, "times"
, "samples"
, "projection"
, "aspect"
, "flip"
, "invert"
, "R_0"
, "G_0"
, "B_0"
, "T_0"
, "phase_range"
, "rotation" // may want to animate this later?
, "angle" // may want to animate this later?
, "NUMBER_OF_LAYERS"
, "N1_0"
, "words"
, "IterationsBias"
, "Internal_ZoomLog2"
, "Internal_Zero"
, "ImageSize"
, "KFP_Palette"
, "Z" // not allowed in shaders
};

int uniform_atoms(const GLenum type)
{
  switch (type)
  {
    case GL_FLOAT:
    case GL_INT:
    case GL_UNSIGNED_INT:
    case GL_BOOL:
    case GL_DOUBLE:
      return 1;
    case GL_FLOAT_VEC2:
    case GL_INT_VEC2:
    case GL_UNSIGNED_INT_VEC2:
    case GL_BOOL_VEC2:
    case GL_DOUBLE_VEC2:
      return 2;
    case GL_FLOAT_VEC3:
    case GL_INT_VEC3:
    case GL_UNSIGNED_INT_VEC3:
    case GL_BOOL_VEC3:
    case GL_DOUBLE_VEC3:
      return 3;
    case GL_FLOAT_VEC4:
    case GL_INT_VEC4:
    case GL_UNSIGNED_INT_VEC4:
    case GL_BOOL_VEC4:
    case GL_DOUBLE_VEC4:
      return 4;
    case GL_FLOAT_MAT2: return 2 * 2;
    case GL_FLOAT_MAT3: return 3 * 3;
    case GL_FLOAT_MAT4: return 4 * 4;
    case GL_FLOAT_MAT2x3: return 2 * 3;
    case GL_FLOAT_MAT2x4: return 2 * 4;
    case GL_FLOAT_MAT3x2: return 3 * 2;
    case GL_FLOAT_MAT3x4: return 3 * 4;
    case GL_FLOAT_MAT4x2: return 4 * 2;
    case GL_FLOAT_MAT4x3: return 4 * 3;
    // double precision requires GL 4.1
    case GL_DOUBLE_MAT2: return 2 * 2;
    case GL_DOUBLE_MAT3: return 3 * 3;
    case GL_DOUBLE_MAT4: return 4 * 4;
    case GL_DOUBLE_MAT2x3: return 2 * 3;
    case GL_DOUBLE_MAT2x4: return 2 * 4;
    case GL_DOUBLE_MAT3x2: return 3 * 2;
    case GL_DOUBLE_MAT3x4: return 3 * 4;
    case GL_DOUBLE_MAT4x2: return 4 * 2;
    case GL_DOUBLE_MAT4x3: return 4 * 3;
    // samplers
    case GL_SAMPLER_1D:
    case GL_SAMPLER_2D:
    case GL_SAMPLER_3D:
    case GL_SAMPLER_CUBE:
    case GL_SAMPLER_1D_SHADOW:
    case GL_SAMPLER_2D_SHADOW:
    case GL_SAMPLER_1D_ARRAY:
    case GL_SAMPLER_2D_ARRAY:
    case GL_SAMPLER_1D_ARRAY_SHADOW:
    case GL_SAMPLER_2D_ARRAY_SHADOW:
    case GL_SAMPLER_2D_MULTISAMPLE:
    case GL_SAMPLER_2D_MULTISAMPLE_ARRAY:
    case GL_SAMPLER_CUBE_SHADOW:
    case GL_SAMPLER_BUFFER:
    case GL_SAMPLER_2D_RECT:
    case GL_SAMPLER_2D_RECT_SHADOW:
    case GL_INT_SAMPLER_1D:
    case GL_INT_SAMPLER_2D:
    case GL_INT_SAMPLER_3D:
    case GL_INT_SAMPLER_CUBE:
    case GL_INT_SAMPLER_1D_ARRAY:
    case GL_INT_SAMPLER_2D_ARRAY:
    case GL_INT_SAMPLER_2D_MULTISAMPLE:
    case GL_INT_SAMPLER_2D_MULTISAMPLE_ARRAY:
    case GL_INT_SAMPLER_BUFFER:
    case GL_INT_SAMPLER_2D_RECT:
    case GL_UNSIGNED_INT_SAMPLER_1D:
    case GL_UNSIGNED_INT_SAMPLER_2D:
    case GL_UNSIGNED_INT_SAMPLER_3D:
    case GL_UNSIGNED_INT_SAMPLER_CUBE:
    case GL_UNSIGNED_INT_SAMPLER_1D_ARRAY:
    case GL_UNSIGNED_INT_SAMPLER_2D_ARRAY:
    case GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE:
    case GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE_ARRAY:
    case GL_UNSIGNED_INT_SAMPLER_BUFFER:
    case GL_UNSIGNED_INT_SAMPLER_2D_RECT:
    // images require GL 4.2
    case GL_IMAGE_1D:
    case GL_IMAGE_2D:
    case GL_IMAGE_3D:
    case GL_IMAGE_2D_RECT:
    case GL_IMAGE_CUBE:
    case GL_IMAGE_BUFFER:
    case GL_IMAGE_1D_ARRAY:
    case GL_IMAGE_2D_ARRAY:
    case GL_IMAGE_2D_MULTISAMPLE:
    case GL_IMAGE_2D_MULTISAMPLE_ARRAY:
    case GL_INT_IMAGE_1D:
    case GL_INT_IMAGE_2D:
    case GL_INT_IMAGE_3D:
    case GL_INT_IMAGE_2D_RECT:
    case GL_INT_IMAGE_CUBE:
    case GL_INT_IMAGE_BUFFER:
    case GL_INT_IMAGE_1D_ARRAY:
    case GL_INT_IMAGE_2D_ARRAY:
    case GL_INT_IMAGE_2D_MULTISAMPLE:
    case GL_INT_IMAGE_2D_MULTISAMPLE_ARRAY:
    case GL_UNSIGNED_INT_IMAGE_1D:
    case GL_UNSIGNED_INT_IMAGE_2D:
    case GL_UNSIGNED_INT_IMAGE_3D:
    case GL_UNSIGNED_INT_IMAGE_2D_RECT:
    case GL_UNSIGNED_INT_IMAGE_CUBE:
    case GL_UNSIGNED_INT_IMAGE_BUFFER:
    case GL_UNSIGNED_INT_IMAGE_1D_ARRAY:
    case GL_UNSIGNED_INT_IMAGE_2D_ARRAY:
    case GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE:
    case GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE_ARRAY:
    // counter requires GL 4.2
    case GL_UNSIGNED_INT_ATOMIC_COUNTER:
      return 1;
    // unsupported type
    default:
      return 1;
  }
}

GLenum uniform_atom_type(const GLenum type)
{
  switch (type)
  {
    case GL_FLOAT:
    case GL_FLOAT_VEC2:
    case GL_FLOAT_VEC3:
    case GL_FLOAT_VEC4:
    case GL_FLOAT_MAT2:
    case GL_FLOAT_MAT3:
    case GL_FLOAT_MAT4:
    case GL_FLOAT_MAT2x3:
    case GL_FLOAT_MAT2x4:
    case GL_FLOAT_MAT3x2:
    case GL_FLOAT_MAT3x4:
    case GL_FLOAT_MAT4x2:
    case GL_FLOAT_MAT4x3:
      return GL_FLOAT;
    case GL_INT:
    case GL_INT_VEC2:
    case GL_INT_VEC3:
    case GL_INT_VEC4:
      return GL_INT;
    case GL_UNSIGNED_INT:
    case GL_UNSIGNED_INT_VEC2:
    case GL_UNSIGNED_INT_VEC3:
    case GL_UNSIGNED_INT_VEC4:
      return GL_UNSIGNED_INT;
    case GL_BOOL:
    case GL_BOOL_VEC2:
    case GL_BOOL_VEC3:
    case GL_BOOL_VEC4:
      return GL_BOOL;
    case GL_DOUBLE:
    case GL_DOUBLE_VEC2:
    case GL_DOUBLE_VEC3:
    case GL_DOUBLE_VEC4:
    case GL_DOUBLE_MAT2:
    case GL_DOUBLE_MAT3:
    case GL_DOUBLE_MAT4:
    case GL_DOUBLE_MAT2x3:
    case GL_DOUBLE_MAT2x4:
    case GL_DOUBLE_MAT3x2:
    case GL_DOUBLE_MAT3x4:
    case GL_DOUBLE_MAT4x2:
    case GL_DOUBLE_MAT4x3:
      return GL_DOUBLE;
    default:
      return type;
  }
}

std::map<std::string, uniform_type> uniform_get_active(GLuint program, bool include_internal)
{
  std::map<std::string, uniform_type> result;
  GLint nactive = 0, len = 0;
  glGetProgramiv(program, GL_ACTIVE_UNIFORMS, &nactive);
  glGetProgramiv(program, GL_ACTIVE_UNIFORM_MAX_LENGTH, &len);
  GLchar *buffer = new GLchar[len];
  for (GLint index = 0; index < nactive; ++index)
  {
    uniform_type t;
    glGetActiveUniform(program, index, len, 0, &t.size, &t.type, buffer);
    std::string name(buffer);
    if (include_internal || uniform_internal.find(name) == uniform_internal.end())
    {
      result.insert(std::pair<std::string, uniform_type>(name, t));
    }
  }
  uniform_type Zt = { GL_FLOAT, 1 };
  result.insert(std::pair<std::string, uniform_type>("Z", Zt));
  delete[] buffer;
  return result;
}

std::map<std::string, GLint> uniform_get_location(GLuint program, const std::map<std::string, uniform_type> &active)
{
  std::map<std::string, GLint> result;
  for (const auto & [ name, type ] : active)
  {
    GLint loc = glGetUniformLocation(program, name.c_str());
    result.insert(std::pair<std::string, GLint>(name, loc));
  }
  return result;
}

std::pair<uniform_value, uniform_value> uniform_get_initial_range(const uniform_type &type)
{
  uniform_value minimum, maximum;
  std::memset(&minimum, 0, sizeof(minimum));
  std::memset(&maximum, 0, sizeof(maximum));
  switch (uniform_atom_type(type.type))
  {
    case GL_FLOAT:
    {
      maximum.float_ = 1;
      break;
    }
    case GL_DOUBLE:
    {
      maximum.double_ = 1;
      break;
    }
    case GL_INT:
    {
      minimum.int_ = INT_MIN;
      maximum.int_ = INT_MAX;
      break;
    }
    case GL_UNSIGNED_INT:
    {
      maximum.uint_ = UINT_MAX;
      break;
    }
    case GL_BOOL:
    {
      maximum.bool_ = 1;
      break;
    }
    default:
    {
      // TODO
    }
  }
  return std::pair<uniform_value, uniform_value>(minimum, maximum);
}

struct ident
{
  std::string name;
  int index;
  int component;
};

bool operator<(const ident &a, const ident &b)
{
  return a.name < b.name ||
    (a.name == b.name && (a.index < b.index ||
      (a.index == b.index && a.component < b.component)));
}

bool operator==(const ident &a, const ident &b)
{
  return a.name == b.name && a.index == b.index && a.component == b.component;
}

bool operator!=(const ident &a, const ident &b)
{
  return ! (a == b);
}

struct fact
{
  double fraction;
  ident id;
  interpolation_mode mode;
  GLenum atom_type;
  uniform_value value;
  bool interpolated;
};

bool fact_cmp_fraction(const fact &a, const fact &b)
{
  return a.fraction < b.fraction;
}

bool fact_cmp_ident(const fact &a, const fact &b)
{
  return a.id < b.id;
}

bool operator==(const fact &a, const fact &b)
{
  if (a.fraction != b.fraction) return false;
  if (a.id != b.id) return false;
  if (a.mode != b.mode) return false;
  if (a.atom_type != b.atom_type) return false;
  if (a.interpolated != b.interpolated) return false;
  switch (uniform_atom_type(a.atom_type))
  {
    case GL_FLOAT: return a.value.float_ == b.value.float_;
    case GL_INT: return a.value.int_ == b.value.int_;
    case GL_BOOL: return a.value.bool_ == b.value.bool_;
    case GL_UNSIGNED_INT: return a.value.uint_ == b.value.uint_;
    case GL_DOUBLE: return a.value.double_ == b.value.double_;
    default: return false; // FIXME
  }
}

void facts_sort_fraction(std::vector<fact> &db)
{
  std::sort(db.begin(), db.end(), fact_cmp_fraction);
}

void facts_sort_ident(std::vector<fact> &db)
{
  std::sort(db.begin(), db.end(), fact_cmp_ident);
}

std::vector<std::vector<fact>> facts_partition_fraction(std::vector<fact> &db)
{
  facts_sort_fraction(db);
  std::vector<std::vector<fact>> results;
  if (db.empty())
  {
    return results;
  }
  fact g = db[0];
  std::vector<fact> result;
  for (const fact &f : db)
  {
    if (f.fraction == g.fraction)
    {
      result.push_back(f);
    }
    else
    {
      facts_sort_ident(result);
      results.push_back(result);
      g = f;
      result.clear();
      result.push_back(f);
    }
  }
  facts_sort_ident(result);
  results.push_back(result);
  return results;
}

std::vector<std::vector<fact>> facts_partition_ident(std::vector<fact> &db)
{
  facts_sort_ident(db);
  std::vector<std::vector<fact>> results;
  if (db.empty())
  {
    return results;
  }
  fact g = db[0];
  std::vector<fact> result;
  for (const fact &f : db)
  {
    if (f.id == g.id)
    {
      result.push_back(f);
    }
    else
    {
      facts_sort_fraction(result);
      results.push_back(result);
      g = f;
      result.clear();
      result.push_back(f);
    }
  }
  facts_sort_fraction(result);
  results.push_back(result);
  return results;
}

std::vector<double> facts_fractions(std::vector<fact> &db)
{
  std::vector<double> result;
  for (const auto &part : facts_partition_fraction(db))
  {
    result.push_back(part[0].fraction);
  }
  return result;
}

std::vector<ident> facts_idents(std::vector<fact> &db)
{
  std::vector<ident> result;
  for (const auto &part : facts_partition_ident(db))
  {
    result.push_back(part[0].id);
  }
  return result;
}

// steps is all for one unique ident
fact facts_interpolate_ident(const std::vector<fact> &steps, double fraction)
{
  // TODO monotone cubic interpolation
  fraction = std::min(std::max(fraction, 0.0), 1.0);
  int lo = 0;
  int hi = (int) steps.size() - 1;
  // must be sorted by fraction
  assert(steps.size() >= 2);
  assert(steps[lo].fraction == 0);
  assert(steps[hi].fraction == 1);
  if (! (0 < fraction))
  {
    fact result = steps[lo];
    result.fraction = 0;
    result.interpolated = steps[lo].fraction != result.fraction;
    return result;
  }
  if (! (fraction < 1))
  {
    fact result = steps[hi];
    result.fraction = 1;
    result.interpolated = steps[hi].fraction != result.fraction;
    return result;
  }
  if (lo == hi)
  {
    // only one fact in vector: use step interpolation
    fact result = steps[lo];
    result.fraction = fraction;
    result.interpolated = steps[lo].fraction != result.fraction;
    return result;
  }
  assert(steps[lo].fraction <= fraction);
  assert(fraction <= steps[hi].fraction);
  // binary search for neighbours
  while (lo + 1 < hi)
  {
    int md = (lo + hi) / 2;
    assert(lo < md);
    assert(md < hi);
    assert(steps[lo].fraction <= steps[md].fraction);
    assert(steps[md].fraction <= steps[hi].fraction);
    if (fraction <= steps[md].fraction)
    {
      hi = md;
    }
    else if (steps[md].fraction <= fraction)
    {
      lo = md;
    }
    else
    {
      assert(! "nan fraction?");
    }
    assert(steps[lo].fraction <= fraction);
    assert(fraction <= steps[hi].fraction);
  }
  assert(lo + 1 == hi);
  if (steps[lo].fraction == fraction)
  {
    fact result = steps[lo];
    result.interpolated = false;
    return result;
  }
  if (steps[hi].fraction == fraction)
  {
    fact result = steps[hi];
    result.interpolated = false;
    return result;
  }
  enum interpolation_mode mode = steps[lo].mode;
  if (! (steps[lo].atom_type == GL_FLOAT || steps[lo].atom_type == GL_DOUBLE))
  {
    mode = interpolation_step;
  }
  switch (mode)
  {
    case interpolation_step:
    {
      fact result = steps[lo];
      result.fraction = fraction;
      result.mode = mode;
      result.interpolated = result.fraction != steps[lo].fraction;
      return result;
    }
    case interpolation_linear:
    {
      double flo = steps[lo].fraction;
      double fhi = steps[hi].fraction;
      double plo = steps[lo].atom_type == GL_FLOAT ? steps[lo].value.float_ : steps[lo].value.double_;
      double phi = steps[hi].atom_type == GL_FLOAT ? steps[hi].value.float_ : steps[hi].value.double_;
      double t = (fraction - flo) / (fhi - flo);
      t = std::min(std::max(t, 0.0), 1.0);
      double phase = plo + t * (phi - plo);
      fact result = steps[lo];
      result.fraction = fraction;
      if (steps[lo].atom_type == GL_FLOAT)
      {
        result.value.float_ = phase;
      }
      else
      {
        result.value.double_ = phase;
      }
      result.interpolated = result.fraction != steps[lo].fraction && result.fraction != steps[hi].fraction;
      return result;
    }
    case interpolation_cubic:
    {
      double tangent_lo = 0;
      if (0 < lo)
      {
        switch (steps[lo - 1].mode)
        {
          case interpolation_step:
          {
            break;
          }
          case interpolation_linear:
          {
            tangent_lo
              = ((steps[lo].atom_type == GL_FLOAT ? steps[lo].value.float_ : steps[lo].value.double_)
                - (steps[lo-1].atom_type == GL_FLOAT ? steps[lo-1].value.float_ : steps[lo-1].value.double_))
              / (steps[lo].fraction - steps[lo - 1].fraction);
            break;
          }
          case interpolation_cubic:
          {
            double d_lo
              = ((steps[lo].atom_type == GL_FLOAT ? steps[lo].value.float_ : steps[lo].value.double_)
                - (steps[lo-1].atom_type == GL_FLOAT ? steps[lo-1].value.float_ : steps[lo-1].value.double_))
              / (steps[lo].fraction - steps[lo - 1].fraction);
            double d_hi
              = ((steps[hi].atom_type == GL_FLOAT ? steps[hi].value.float_ : steps[hi].value.double_)
                - (steps[lo].atom_type == GL_FLOAT ? steps[lo].value.float_ : steps[lo].value.double_))
              / (steps[hi].fraction - steps[lo].fraction);
            if (d_lo == 0 || d_hi == 0 || (d_lo > 0) != (d_hi > 0))
            {
              tangent_lo = 0;
            }
            else
            {
              double m = (d_lo + d_hi) / 2;
              double a = m / d_lo;
              if (a < 0)
              {
                m = 0;
                a = 0;
              }
              if (a > 3)
              {
                m = 3 * d_lo;
              }
              tangent_lo = m;
            }
            break;
          }
        }
      }
      else
      {
        // secant
        tangent_lo
          = ((steps[hi].atom_type == GL_FLOAT ? steps[hi].value.float_ : steps[hi].value.double_)
           - (steps[lo].atom_type == GL_FLOAT ? steps[lo].value.float_ : steps[lo].value.double_))
          / (steps[hi].fraction - steps[lo].fraction);
      }
      double tangent_hi = 0;
      if (hi < (int) steps.size())
      {
        switch (steps[hi].mode)
        {
          case interpolation_step:
          {
            break;
          }
          case interpolation_linear:
          {
            if (hi + 1 < (int) steps.size())
            {
              tangent_hi
                = ((steps[hi+1].atom_type == GL_FLOAT ? steps[hi+1].value.float_ : steps[hi+1].value.double_)
                  - (steps[hi].atom_type == GL_FLOAT ? steps[hi].value.float_ : steps[hi].value.double_))
                / (steps[hi + 1].fraction - steps[hi].fraction);
            }
            else
            {
              tangent_hi
                = ((steps[hi].atom_type == GL_FLOAT ? steps[hi].value.float_ : steps[hi].value.double_)
                  - (steps[lo].atom_type == GL_FLOAT ? steps[lo].value.float_ : steps[lo].value.double_))
                / (steps[hi].fraction - steps[lo].fraction);
            }
            break;
          }
          case interpolation_cubic:
          {
            double d_lo
              = ((steps[hi].atom_type == GL_FLOAT ? steps[hi].value.float_ : steps[hi].value.double_)
                - (steps[lo].atom_type == GL_FLOAT ? steps[lo].value.float_ : steps[lo].value.double_))
              / (steps[hi].fraction - steps[lo].fraction);
            double d_hi = d_lo;
            if (hi + 1 < (int) steps.size())
            {
              d_hi
                = ((steps[hi+1].atom_type == GL_FLOAT ? steps[hi+1].value.float_ : steps[hi+1].value.double_)
                  - (steps[hi].atom_type == GL_FLOAT ? steps[hi].value.float_ : steps[hi].value.double_))
                / (steps[hi+1].fraction - steps[hi].fraction);
            }
            if (d_lo == 0 || d_hi == 0 || (d_lo > 0) != (d_hi > 0))
            {
              tangent_hi = 0;
            }
            else
            {
              double m = (d_lo + d_hi) / 2;
              double b = m / d_hi;
              if (b < 0)
              {
                m = 0;
                b = 0;
              }
              if (b > 3)
              {
                m = 3 * d_hi;
              }
              tangent_hi = m;
            }
          }
        }
      }
      // cubic Hermite spline
      double flo = steps[lo].fraction;
      double plo = steps[lo].atom_type == GL_FLOAT ? steps[lo].value.float_ : steps[lo].value.double_;
      double fhi = steps[hi].fraction;
      double phi = steps[hi].atom_type == GL_FLOAT ? steps[hi].value.float_ : steps[hi].value.double_;
      double t = (fraction - flo) / (fhi - flo);
      double h00 = (1 + 2 * t) * (1 - t) * (1 - t);
      double h10 = t * (1 - t) * (1 - t);
      double h01 = t * t * (3 - 2 * t);
      double h11 = t * t * (t - 1);
      t = std::min(std::max(t, 0.0), 1.0);
      tangent_lo *= (fhi - flo);
      tangent_hi *= (fhi - flo);
      double phase = h00 * plo + h10 * tangent_lo + h01 * phi + h11 * tangent_hi;
      fact result = steps[lo];
      result.fraction = fraction;
      if (result.atom_type == GL_FLOAT)
      {
        result.value.float_ = phase;
      }
      else
      {
        result.value.double_ = phase;
      }
      result.interpolated = result.fraction != steps[lo].fraction && result.fraction != steps[hi].fraction;
      return result;
    }
  }
  assert(! "interpolation mode");
}

std::vector<std::vector<fact>> facts_interpolate(std::vector<fact> &db, double cursor, double &speed)
{
  std::vector<double> fractions = facts_fractions(db);
  bool contains = false;
  for (const auto & f : fractions)
  {
    if (f == cursor)
    {
      contains = true;
      break;
    }
  }
  if (! contains)
  {
    fractions.push_back(cursor);
  }
  std::sort(fractions.begin(), fractions.end());
  std::vector<std::vector<fact>> parts = facts_partition_ident(db);
  std::vector<std::vector<fact>> results;
  for (const auto &fraction : fractions)
  {
    std::vector<fact> result;
    for (const auto &part : parts)
    {
      result.push_back(facts_interpolate_ident(part, fraction));
    }
    results.push_back(result);
  }
  for (const auto &part : parts)
  {
    ident id = { "Z", 0, 0 };
    if (part[0].id == id)
    {
      double t0 = cursor - speed / 2;
      double t1 = cursor + speed / 2;
      double Z0 = facts_interpolate_ident(part, t0).value.float_;
      double Z1 = facts_interpolate_ident(part, t1).value.float_;
      speed = (Z1 - Z0) / (t1 - t0);
    }
  }
  return results;
}

std::vector<fact> facts_discard_interpolated(const std::vector<fact> &row)
{
  std::vector<fact> result;
  for (const auto &f : row)
  {
    if (! f.interpolated)
    {
      result.push_back(f);
    }
  }
  return result;
}

std::vector<fact> facts_discard_interpolated(const std::vector<std::vector<fact>> &table)
{
  std::vector<fact> result;
  for (const auto& row : table)
  {
    for (const auto &f : row)
    {
      if (! f.interpolated)
      {
        result.push_back(f);
      }
    }
  }
  return result;
}

bool facts_display_table(std::vector<std::vector<fact>> &table, struct audio *au)
{
  double duration = audio_get_duration(au);
  int cell_id = 0;
  bool modified = false;
  if (table.empty())
  {
    return modified;
  }
  if (table[0].empty())
  {
    return modified;
  }
  if (ImGui::BeginTable("Timeline Sequencer", 1 + table[0].size()))
  {
    ImGui::PushID(cell_id++);
    ImGui::TableSetupColumn("Time", ImGuiTableColumnFlags_NoSort);
    for (const auto& f : table[0])
    {
      std::ostringstream label;
      label << f.id.name << "[" << f.id.index << "]" << "[" << f.id.component << "]"; // TODO trim
      ImGui::TableSetupColumn(label.str().c_str(), ImGuiTableColumnFlags_NoSort);
    }
    ImGui::TableHeadersRow();
    for (auto& row : table)
    {
      if (row.empty())
      {
        continue;
      }
      ImGui::TableNextRow();
      ImGui::TableNextColumn();
      ImGui::PopID();
      ImGui::PushID(cell_id++);
      double time = duration * row[0].fraction;
      bool display_only = facts_discard_interpolated(row).empty();
      if (display_only)
      {
        // cursor
        ImGui::Text("%9.3fs", time);
      }
      else
      {
        // waypoint
        char label[100];
        std::snprintf(label, 100, "%9.3fs", time);
        if (ImGui::Button(label))
        {
          audio_set_time(au, time);
        }
        if (ImGui::IsItemHovered())
        {
          ImGui::SetTooltip("Jump to time.");
        }
      }
      bool is_endpoint = row[0].fraction <= 0 || row[0].fraction >= 1;
      for (auto &f : row)
      {
        ImGui::TableNextColumn();
        ImGui::PopID();
        ImGui::PushID(cell_id++);
        if (f.interpolated)
        {
          if (ImGui::Button("+"))
          {
            f.interpolated = false;
            modified |= true;
          }
        }
        else if (! is_endpoint)
        {
          if (ImGui::Button("-"))
          {
            f.interpolated = true;
            modified |= true;
          }
        }
        else
        {
          ImGui::Button("*");
        }
        ImGui::PopID();
        ImGui::PushID(cell_id++);
        if (! f.interpolated && (f.atom_type == GL_FLOAT || f.atom_type == GL_DOUBLE))
        {
          int mode = f.mode;
          ImGui::SameLine();
          ImGui::PushItemWidth(50);
          modified |= ImGui::Combo("##Interpolation", &mode, interpolation_mode_string, IM_ARRAYSIZE(interpolation_mode_string));
          ImGui::PopItemWidth();
          f.mode = interpolation_mode(mode);
        }
        ImGui::PopID();
        ImGui::PushID(cell_id++);
        if (f.interpolated)
        {
          char label[100];
          switch (f.atom_type)
          {
            case GL_FLOAT:
            {
              snprintf(label, 100, "%.7g", f.value.float_);
              break;
            }
            case GL_DOUBLE:
            {
              snprintf(label, 100, "%.17g", f.value.double_);
              break;
            }
            case GL_INT:
            {
              snprintf(label, 100, "%d", f.value.int_);
              break;
            }
            case GL_UNSIGNED_INT:
            {
              snprintf(label, 100, "%u", f.value.uint_);
              break;
            }
            case GL_BOOL:
            {
              snprintf(label, 100, "%d", f.value.bool_);
              break;
            }
            default:
            {
              snprintf(label, 100, "%s", "?");
              break;
            }
          }
          ImGui::SameLine();
          ImGui::PushItemWidth(50);
          ImGui::Text("%s", label);
          ImGui::PopItemWidth();
        }
        else
        {
          ImGui::SameLine();
          ImGui::PushItemWidth(50);
          switch (f.atom_type)
          {
            case GL_FLOAT:
            {
              modified |= ImGui::InputFloat("##Float", &f.value.float_, 0.0f, 0.0f, "%.7g"); // FIXME range
              break;
            }
            case GL_DOUBLE:
            {
              modified |= ImGui::InputDouble("##Double", &f.value.double_, 0.0, 0.0, "%.17g"); // FIXME range
              break;
            }
            case GL_INT:
            {
              modified |= ImGui::InputInt("##Int", &f.value.int_);
              break;
            }
            case GL_UNSIGNED_INT:
            {
              int i = f.value.uint_;
              modified |= ImGui::InputInt("##UInt", &i);
              f.value.uint_ = i;
              break;
            }
            case GL_BOOL:
            {
              modified |= ImGui::Checkbox("##Bool", &f.value.bool_);
              break;
            }
            default:
            {
              ImGui::Text("?");
              break;
            }
          }
          ImGui::PopItemWidth();
        }
      }
    }
    ImGui::PopID();
    ImGui::EndTable();
  }
  return modified;
}

struct uniforms
{
  GLuint program;
  int keyframes;
  std::map<std::string, uniform_type> active;
  std::map<std::string, GLint> location;
  std::vector<fact> db;
  std::vector<fact> current;
  double speed;
  ImGui::FileBrowser *export_csv;
  ImGui::FileBrowser *import_csv;
  bool export_interpolated;
  bool export_headerless;
};

extern void uniforms_upload(const struct uniforms *u)
{
//  GLint old_program;
//  glGetIntegerv(GL_CURRENT_PROGRAM, &old_program);
//  glUseProgram(u->program);
  const bool transpose = false;
  std::map<ident, uniform_value> cooked;
  for (const auto & f : u->current)
  {
    cooked[f.id] = f.value;
  }
  for (const auto & [name, type] : u->active)
  {
    GLint loc = -1;
    try
    {
      loc = u->location.at(name);
    }
    catch (const std::out_of_range &e)
    {
      // ignore
    }
    if (loc == -1)
    {
      continue;
    }
    int nj = uniform_atoms(type.type);
    int atoms = type.size * nj;
    switch (uniform_atom_type(type.type))
    {
      case GL_FLOAT:
      {
        float *data = new float[atoms];
        for (int i = 0; i < type.size; ++i)
        {
          for (int j = 0; j < nj; ++j)
          {
            uniform_value value;
            value.float_ = 0;
            try
            {
              ident id = { name, i, j };
              value = cooked.at(id);
            }
            catch (const std::out_of_range &e)
            {
              // ignore
            }
            data[i * nj + j] = value.float_;
          }
        }
        switch (type.type)
        {
          case GL_FLOAT:        glUniform1fv(loc, type.size, data); break;
          case GL_FLOAT_VEC2:   glUniform2fv(loc, type.size, data); break;
          case GL_FLOAT_VEC3:   glUniform3fv(loc, type.size, data); break;
          case GL_FLOAT_VEC4:   glUniform4fv(loc, type.size, data); break;
          case GL_FLOAT_MAT2:   glUniformMatrix2fv(loc, type.size, transpose, data); break;
          case GL_FLOAT_MAT3:   glUniformMatrix3fv(loc, type.size, transpose, data); break;
          case GL_FLOAT_MAT4:   glUniformMatrix4fv(loc, type.size, transpose, data); break;
          case GL_FLOAT_MAT2x3: glUniformMatrix2x3fv(loc, type.size, transpose, data); break;
          case GL_FLOAT_MAT2x4: glUniformMatrix2x4fv(loc, type.size, transpose, data); break;
          case GL_FLOAT_MAT3x2: glUniformMatrix3x2fv(loc, type.size, transpose, data); break;
          case GL_FLOAT_MAT3x4: glUniformMatrix3x4fv(loc, type.size, transpose, data); break;
          case GL_FLOAT_MAT4x2: glUniformMatrix4x2fv(loc, type.size, transpose, data); break;
          case GL_FLOAT_MAT4x3: glUniformMatrix4x3fv(loc, type.size, transpose, data); break;
        }
        delete[] data;
        break;
      }

      case GL_DOUBLE:
      {
        double *data = new double[atoms];
        for (int i = 0; i < type.size; ++i)
        {
          for (int j = 0; j < nj; ++j)
          {
            uniform_value value;
            value.double_ = 0;
            try
            {
              ident id = { name, i, j };
              value = cooked.at(id);
            }
            catch (const std::out_of_range &e)
            {
              // ignore
            }
            data[i * nj + j] = value.double_;
          }
        }
        switch (type.type)
        {
          case GL_DOUBLE:        glUniform1dv(loc, type.size, data); break;
          case GL_DOUBLE_VEC2:   glUniform2dv(loc, type.size, data); break;
          case GL_DOUBLE_VEC3:   glUniform3dv(loc, type.size, data); break;
          case GL_DOUBLE_VEC4:   glUniform4dv(loc, type.size, data); break;
          case GL_DOUBLE_MAT2:   glUniformMatrix2dv(loc, type.size, transpose, data); break;
          case GL_DOUBLE_MAT3:   glUniformMatrix3dv(loc, type.size, transpose, data); break;
          case GL_DOUBLE_MAT4:   glUniformMatrix4dv(loc, type.size, transpose, data); break;
          case GL_DOUBLE_MAT2x3: glUniformMatrix2x3dv(loc, type.size, transpose, data); break;
          case GL_DOUBLE_MAT2x4: glUniformMatrix2x4dv(loc, type.size, transpose, data); break;
          case GL_DOUBLE_MAT3x2: glUniformMatrix3x2dv(loc, type.size, transpose, data); break;
          case GL_DOUBLE_MAT3x4: glUniformMatrix3x4dv(loc, type.size, transpose, data); break;
          case GL_DOUBLE_MAT4x2: glUniformMatrix4x2dv(loc, type.size, transpose, data); break;
          case GL_DOUBLE_MAT4x3: glUniformMatrix4x3dv(loc, type.size, transpose, data); break;
        }
        delete[] data;
        break;
      }

      case GL_BOOL:
      {
        GLint *data = new GLint[atoms];
        for (int i = 0; i < type.size; ++i)
        {
          for (int j = 0; j < nj; ++j)
          {
            uniform_value value;
            value.bool_ = 0;
            try
            {
              ident id = { name, i, j };
              value = cooked.at(id);
            }
            catch (const std::out_of_range &e)
            {
              // ignore
            }
            data[i * nj + j] = value.bool_;
          }
        }
        switch (type.type)
        {
          case GL_BOOL:        glUniform1iv(loc, type.size, data); break;
          case GL_BOOL_VEC2:   glUniform2iv(loc, type.size, data); break;
          case GL_BOOL_VEC3:   glUniform3iv(loc, type.size, data); break;
          case GL_BOOL_VEC4:   glUniform4iv(loc, type.size, data); break;
        }
        delete[] data;
        break;
      }

      case GL_INT:
      {
        GLint *data = new GLint[atoms];
        for (int i = 0; i < type.size; ++i)
        {
          for (int j = 0; j < nj; ++j)
          {
            uniform_value value;
            value.int_ = 0;
            try
            {
              ident id = { name, i, j };
              value = cooked.at(id);
            }
            catch (const std::out_of_range &e)
            {
              // ignore
            }
            data[i * nj + j] = value.int_;
          }
        }
        switch (type.type)
        {
          case GL_INT:        glUniform1iv(loc, type.size, data); break;
          case GL_INT_VEC2:   glUniform2iv(loc, type.size, data); break;
          case GL_INT_VEC3:   glUniform3iv(loc, type.size, data); break;
          case GL_INT_VEC4:   glUniform4iv(loc, type.size, data); break;
        }
        delete[] data;
        break;
      }

      case GL_UNSIGNED_INT:
      {
        GLuint *data = new GLuint[atoms];
        for (int i = 0; i < type.size; ++i)
        {
          for (int j = 0; j < nj; ++j)
          {
            uniform_value value;
            value.int_ = 0;
            try
            {
              ident id = { name, i, j };
              value = cooked.at(id);
            }
            catch (const std::out_of_range &e)
            {
              // ignore
            }
            data[i * nj + j] = value.int_;
          }
        }
        switch (type.type)
        {
          case GL_UNSIGNED_INT:        glUniform1uiv(loc, type.size, data); break;
          case GL_UNSIGNED_INT_VEC2:   glUniform2uiv(loc, type.size, data); break;
          case GL_UNSIGNED_INT_VEC3:   glUniform3uiv(loc, type.size, data); break;
          case GL_UNSIGNED_INT_VEC4:   glUniform4uiv(loc, type.size, data); break;
        }
        delete[] data;
        break;
      }
    }
  }
//  glUseProgram(old_program);
}

extern struct uniforms *uniforms_new(void)
{
  struct uniforms *u = new uniforms();
  u->program = 0;
  u->keyframes = 0;
  const ident zid = { "Z", 0, 0 };
  const uniform_type ztype = { GL_FLOAT, 1 };
  u->active.insert(std::pair<std::string, uniform_type>("Z", ztype));
  uniform_value value;
  std::memset(&value, 0, sizeof(value));
  fact start = { 0.0, zid, interpolation_cubic, GL_FLOAT, value, false };
  fact end   = { 1.0, zid, interpolation_cubic, GL_FLOAT, value, false };
  u->db.push_back(start);
  u->db.push_back(end);
  u->export_csv = new ImGui::FileBrowser(ImGuiFileBrowserFlags_CloseOnEsc | ImGuiFileBrowserFlags_EnterNewFilename | ImGuiFileBrowserFlags_CreateNewDir);
  u->export_csv->SetTitle("Export timeline");
  u->export_csv->SetTypeFilters({ ".csv" });
  u->import_csv = new ImGui::FileBrowser(ImGuiFileBrowserFlags_CloseOnEsc);
  u->import_csv->SetTitle("Import timeline");
  u->import_csv->SetTypeFilters({ ".csv" });
  u->export_interpolated = false;
  u->export_headerless = false;
  return u;
}

extern void uniforms_delete(struct uniforms *u)
{
  delete u->export_csv;
  delete u->import_csv;
  delete u;
}

// preserve any existing values
extern void uniforms_set_program(struct uniforms *u, GLuint program)
{
  uniform_value value;
  std::memset(&value, 0, sizeof(value));
  u->program = program;
  u->active = uniform_get_active(u->program, false);
  u->location = uniform_get_location(u->program, u->active);
  std::map<std::pair<double, ident>, fact> cooked;
  for (const auto & f : u->db)
  {
    cooked[std::pair<double, ident>(f.fraction, f.id)] = f;
  }
  for (const auto& [ name, type ] : u->active)
  {
    for (int index = 0; index < type.size; ++index)
    {
      for (int component = 0; component < uniform_atoms(type.type); ++component)
      {
        for (double fraction = 0; fraction <= 1; fraction += 1)
        {
          fact f =
            { fraction
            , { name, index, component }
            , interpolation_step
            , uniform_atom_type(type.type)
            , value
            , false
            };
          std::pair<double, ident> k(f.fraction, f.id);
          if (cooked.find(k) == cooked.end())
          {
            cooked[k] = f;
          }
        }
      }
    }
  }
  u->db.clear();
  for (const auto & [k, f] : cooked)
  {
    u->db.push_back(f);
  }
}

// update end "Z" value if it is still at default
extern void uniforms_set_keyframes(struct uniforms *u, int keyframes)
{
  ident id = { "Z", 0, 0 };
  for (auto & f : u->db)
  {
    if (f.id == id && f.fraction == 1)
    {
      if (f.value.float_ == u->keyframes)
      {
        f.value.float_ = keyframes;
      }
      break;
    }
  }
  u->keyframes = keyframes;
}

extern bool uniforms_display(struct uniforms *u, struct audio *au, bool show_gui)
{
  if (show_gui)
  {
    ImGui::Begin("Timeline", nullptr, ImGuiWindowFlags_HorizontalScrollbar);
    if (ImGui::Button("Import"))
    {
      u->import_csv->Open();
    }
    if (ImGui::IsItemHovered())
    {
      ImGui::SetTooltip("Import timeline from CSV text file.");
    }
    ImGui::SameLine();
    if (ImGui::Button("Export"))
    {
      u->export_csv->Open();
    }
    if (ImGui::IsItemHovered())
    {
      ImGui::SetTooltip("Export timeline to CSV text file.");
    }
    ImGui::SameLine();
    ImGui::Checkbox("Headerless", &u->export_headerless);
    if (ImGui::IsItemHovered())
    {
      ImGui::SetTooltip("Set this if exported CSV files should not have a header line.\nNote: the header line is required for import!");
    }
    ImGui::SameLine();
    ImGui::Checkbox("Interpolated", &u->export_interpolated);
    if (ImGui::IsItemHovered())
    {
      ImGui::SetTooltip("Export interpolated timeline (uses Output/FPS, cannot be imported).");
    }
  }
  double duration = audio_get_duration(au);
  double time = audio_get_time(au);
  double cursor = time / duration;
  if (std::isnan(cursor) || std::isinf(cursor)) cursor = 0;
  double speed = 0.01 / duration;
  if (std::isnan(speed) || std::isinf(speed)) speed = 0;
  auto table = facts_interpolate(u->db, cursor, speed);
  speed /= duration;
  if (std::isnan(speed) || std::isinf(speed)) speed = 0;
  bool modified = false;
  if (show_gui)
  {
    ImGui::Text("Speed: %.3f 2x zooms per second", speed);
    ImGui::Text("Speed: %.3f 10x zooms per second", speed * std::log10(2.0));
  }
  std::vector<fact> old;
  std::swap(old, u->current);
  for (const auto & row : table)
  {
    if (! row.empty())
    {
      if (row[0].fraction == cursor)
      {
        modified |= old != row;
        u->current = row;
        break;
      }
    }
  }
  if (show_gui)
  {
    modified |= facts_display_table(table, au);
    if (modified)
    {
      u->db = facts_discard_interpolated(table);
    }
//    uniforms_upload(u);
    ImGui::End();
  }
  return modified;
}

std::vector<ident> uniforms_parse_header(const std::string &s)
{
  std::vector<ident> result;
  std::stringstream ss(s);
  std::string field;
  if (std::getline(ss, field, ','))
  {
    if (field != "Fraction")
    {
      throw invalid_data("expected Fraction");
    }
  }
  else
  {
    throw invalid_data("expected ,");
  }
  ident last_id = { "n/a", -1, -1 };
  while (std::getline(ss, field, ','))
  {
    ident id = { "n/a", -1, -1 };
    size_t pos = 0;
    if ((pos = field.find('[')) != std::string::npos)
    {
      id.name = field.substr(0, pos);
      field.erase(0, pos + 1);
      if ((pos = field.find(']')) != std::string::npos)
      {
        id.index = std::stoi(field.substr(0, pos));
        field.erase(0, pos + 1);
        if ((pos = field.find('[')) != std::string::npos)
        {
          field.erase(0, pos + 1);
          if ((pos = field.find(']')) != std::string::npos)
          {
            id.component = std::stoi(field.substr(0, pos));
            if (id.name == "Interpolation" + last_id.name && id.index == last_id.index && id.component == last_id.component)
            {
              // don't store interpolation fields in field list
            }
            else
            {
              result.push_back(id);
            }
            last_id = id;
          }
          else
          {
            throw invalid_data("expected ]");
          }
        }
        else
        {
          throw invalid_data("expected [");
        }
      }
      else
      {
        throw invalid_data("expected ]");
      }
    }
    else
    {
      throw invalid_data("expected [");
    }
  }
  return result;
}

extern void uniforms_export_csv(std::ostream &out, struct uniforms *u, double duration)
{
  if (! u->export_headerless)
  {
    out << "Fraction";
    for (const auto & [ name, type ] : u->active)
    {
      for (int i = 0; i < type.size; ++i)
      {
        for (int j = 0; j < uniform_atoms(type.type); ++j)
        {
          out << "," << name << "[" << i << "]" << "[" << j << "]";
          out << ",Interpolation" << name << "[" << i << "]" << "[" << j << "]";
        }
      }
    }
    out << "\r\n";
  }
  double speed = 0.01 / duration;
  std::vector<std::vector<fact>> table = facts_interpolate(u->db, 0, speed);
  for (const auto &row : table)
  {
    if (row.empty())
    {
      continue;
    }
    std::map<ident, fact> cooked;
    for (const auto & f : row)
    {
      cooked[f.id] = f;
    }
    out << row[0].fraction;
    for (const auto & [ name, type ] : u->active)
    {
      for (int i = 0; i < type.size; ++i)
      {
        for (int j = 0; j < uniform_atoms(type.type); ++j)
        {
          try
          {
            ident id = { name, i, j };
            fact P = cooked.at(id);
            if (P.interpolated)
            {
              out << ",,";
            }
            else
            {
              switch (P.atom_type)
              {
                case GL_FLOAT:
                {
                  out << "," << std::defaultfloat << std::setprecision(7) << P.value.float_;
                  break;
                }
                case GL_DOUBLE:
                {
                  out << "," << std::defaultfloat << std::setprecision(17) << P.value.double_;
                  break;
                }
                case GL_INT:
                {
                  out << "," << P.value.int_;
                  break;
                }
                case GL_UNSIGNED_INT:
                {
                  out << "," << P.value.uint_;
                  break;
                }
                case GL_BOOL:
                {
                  out << "," << (P.value.bool_ ? 1 : 0);
                  break;
                }
                default:
                {
                  out << ",";
                  break;
                }
              }
              out << "," << interpolation_mode_string[P.mode];
            }
          }
          catch (const std::out_of_range &e)
          {
            out << ",,";
          }
        }
      }
    }
    out << "\r\n";
  }
}

std::vector<fact> uniforms_import_csv(std::istream &in, struct uniforms *u)
{
  std::vector<fact> csv;
  std::string line;
  getline(in, line, '\n');
  std::vector<ident> fields = uniforms_parse_header(line);
  while (! getline(in, line, '\n').eof())
  {
    size_t cr = line.find('\r');
    if (cr != std::string::npos)
    {
      line = line.substr(0, cr);
    }
    if (line == "")
    {
      continue;
    }
    size_t pos = 0;
    std::string line0 = line;
    if ((pos = line.find(',')) != std::string::npos)
    {
      double fraction = std::stof(line.substr(0, pos));
      line.erase(0, pos + 1);
      for (const auto &field : fields)
      {
        if ((pos = line.find(',')) != std::string::npos)
        {
          std::string svalue = line.substr(0, pos);
          line.erase(0, pos + 1);
          pos = line.find(',');
          if (pos == std::string::npos)
          {
            pos = line.size();
          }
          std::string ivalue = line.substr(0, pos);
          line.erase(0, pos + 1);
          if (svalue != "")
          {
            try
            {
              GLenum type = uniform_atom_type(u->active.at(field.name).type);
              enum interpolation_mode mode = interpolation_step;
              if (ivalue == "Step" || ivalue == "Linear" || ivalue == "Cubic")
              {
                if (ivalue == "Step")
                {
                  mode = interpolation_step;
                }
                else if (ivalue == "Linear")
                {
                  mode = interpolation_linear;
                }
                else if (ivalue == "Cubic")
                {
                  mode = interpolation_cubic;
                }
              }
              else
              {
                throw invalid_data("bad interpolation value");
              }
              fact f = { fraction, field, mode, type, {0}, false };
              switch (type)
              {
                case GL_FLOAT:
                {
                  f.value.float_ = std::stof(svalue);
                  break;
                }
                case GL_DOUBLE:
                {
                  f.value.double_ = std::stof(svalue);
                  break;
                }
                case GL_INT:
                {
                  f.value.int_ = std::stoi(svalue);
                  break;
                }
                case GL_UNSIGNED_INT:
                {
                  f.value.uint_ = std::stoi(svalue);
                  break;
                }
                case GL_BOOL:
                {
                  f.value.bool_ = std::stoi(svalue) != 0;
                  break;
                }
                default:
                {
                  throw invalid_data("bad uniform type");
                }
              }
              csv.push_back(f);
            }
            catch (const std::out_of_range &e)
            {
              // ignore
            }
          }
        }
        else
        {
          throw invalid_data("expected field");
        }
      }
    }
    else
    {
      throw invalid_data("expected Fraction field");
    }
  }
  if (csv.size() < 2 || csv[0].fraction != 0 || csv[csv.size() - 1].fraction != 1)
  {
    throw invalid_data("bad size or ends not 0/1");
  }
  return csv;
}

extern void uniforms_display_late(struct uniforms *u, double fps, double duration)
{
  const ident zid = { "Z", 0, 0 };
  static std::string error_string = "";
  u->export_csv->Display();
  if (u->export_csv->HasSelected())
  {
    std::filesystem::path file = u->export_csv->GetSelected();
    u->export_csv->ClearSelected();
    try
    {
      std::ofstream out(file); // FIXME handle exceptions
      out.imbue(std::locale::classic());
      if (u->export_interpolated)
      {
        int output_frame_count = fps * duration;
        if (! u->export_headerless)
        {
          out << "Frame";
          for (const auto & [ name, type ] : u->active)
          {
            for (int i = 0; i < type.size; ++i)
            {
              for (int j = 0; j < uniform_atoms(type.type); ++j)
              {
                out << "," << name << "[" << i << "]" << "[" << j << "]";
              }
            }
          }
          out << "\r\n";
        }
        std::map<ident, std::vector<fact>> parts;
        for (const auto & part : facts_partition_ident(u->db))
        {
          parts[part[0].id] = part;
        }
        for (int frame = 0; frame < output_frame_count; ++frame)
        {
          out << frame;
          double fraction = frame / (double) output_frame_count;
          for (const auto & [name, type] : u->active)
          {
            for (int i = 0; i < type.size; ++i)
            {
              for (int j = 0; j < uniform_atoms(type.type); ++j)
              {
                const ident id = { name, i, j };
                uniform_value value = facts_interpolate_ident(parts.at(id), fraction).value;
                switch (uniform_atom_type(type.type))
                {
                  case GL_FLOAT:
                  {
                    out << "," << std::defaultfloat << std::setprecision(7) << value.float_;
                    break;
                  }
                  case GL_DOUBLE:
                  {
                    out << "," << std::defaultfloat << std::setprecision(17) << value.double_;
                    break;
                  }
                  case GL_INT:
                  {
                    out << "," << value.int_;
                    break;
                  }
                  case GL_UNSIGNED_INT:
                  {
                    out << "," << value.uint_;
                    break;
                  }
                  case GL_BOOL:
                  {
                    out << "," << (value.bool_ ? 1 : 0);
                    break;
                  }
                  default:
                  {
                    out << ",?";
                    break;
                  }
                }
              }
            }
          }
          out << "\r\n";
        }
      }
      else
      {
        uniforms_export_csv(out, u, duration);
      }
    }
    catch (std::exception &e)
    {
      error_string = e.what();
    }
  }
  u->import_csv->Display();
  if (u->import_csv->HasSelected())
  {
    std::filesystem::path file = u->import_csv->GetSelected();
    u->import_csv->ClearSelected();
    try
    {
      std::ifstream in(file);
      in.imbue(std::locale::classic());
      u->db = uniforms_import_csv(in, u);
    }
    catch (std::exception &e)
    {
      error_string = e.what();
    }
  }
  if (error_string != "")
  {
    ImGui::OpenPopup("Error");
    ImVec2 center(ImGui::GetIO().DisplaySize.x * 0.5f, ImGui::GetIO().DisplaySize.y * 0.5f);
    ImGui::SetNextWindowPos(center, ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));
    if (ImGui::BeginPopupModal("Error", NULL, ImGuiWindowFlags_AlwaysAutoResize))
    {
      ImGui::Text("%s", error_string.c_str());
      if (ImGui::Button("Ok"))
      {
        error_string = "";
      }
      ImGui::EndPopup();
    }
  }
}

extern toml::array uniforms_save_session(const struct uniforms *u)
{
  toml::array r;
  for (const auto & f : u->db)
  {
    toml::table t;
    t.insert("fraction", f.fraction);
    t.insert("name", f.id.name);
    t.insert("index", f.id.index);
    t.insert("component", f.id.component);
    t.insert("interpolation", interpolation_mode_string[f.mode]);
    switch (f.atom_type)
    {
      case GL_FLOAT:
      {
        t.insert("type", "float");
        t.insert("value", f.value.float_);
        break;
      }
      case GL_DOUBLE:
      {
        t.insert("type", "double");
        t.insert("value", f.value.double_);
        break;
      }
      case GL_INT:
      {
        t.insert("type", "int");
        t.insert("value", f.value.int_);
        break;
      }
      case GL_UNSIGNED_INT:
      {
        t.insert("type", "uint");
        t.insert("value", f.value.uint_);
        break;
      }
      case GL_BOOL:
      {
        t.insert("type", "bool");
        t.insert("value", f.value.bool_ ? 1 : 0);
        break;
      }
      default:
      {
        // TODO
      }
    }
    r.push_back(t);
  }
  return r;
}

extern bool uniforms_load_session(struct uniforms *u, const toml::array &r)
{
  try
  {
    std::vector<fact> result;
    for (const auto & i : r)
    {
      if (i.is_table())
      {
        toml::table t(*i.as_table());
        
        fact f;
        if (t["fraction"].is_floating_point())
        {
          f.fraction = double(*t["fraction"].as_floating_point());
        }
        else
        {
          throw invalid_data("fraction not floating point");
        }
        if (t["name"].is_string())
        {
          f.id.name = std::string(*t["name"].as_string());
        }
        else
        {
          throw invalid_data("name not string");
        }
        if (t["index"].is_integer())
        {
          f.id.index = int64_t(*t["index"].as_integer());
        }
        else
        {
          throw invalid_data("index not integer");
        }
        if (t["component"].is_integer())
        {
          f.id.component = int64_t(*t["component"].as_integer());
        }
        else
        {
          throw invalid_data("component not integer");
        }
        if (t["interpolation"].is_string())
        {
          std::string interpolation = std::string(*t["interpolation"].as_string());
          if (interpolation == "Step")
          {
            f.mode = interpolation_step;
          }
          else if (interpolation == "Linear")
          {
            f.mode = interpolation_linear;
          }
          else if (interpolation == "Cubic")
          {
            f.mode = interpolation_cubic;
          }
          else
          {
            throw invalid_data("bad interpolation value");
          }
        }
        else
        {
          throw invalid_data("interpolation not string");
        }
        if (t["type"].is_string())
        {
          std::string type = std::string(*t["type"].as_string());
          if (type == "float")
          {
            f.atom_type = GL_FLOAT;
            if (t["value"].is_floating_point())
            {
              f.value.float_ = double(*t["value"].as_floating_point());
            }
            else
            {
              throw invalid_data("value not floating point");
            }
          }
          else if (type == "double")
          {
            f.atom_type = GL_DOUBLE;
            if (t["value"].is_floating_point())
            {
              f.value.double_ = double(*t["value"].as_floating_point());
            }
            else
            {
              throw invalid_data("value not floating point");
            }
          }
          else if (type == "int")
          {
            f.atom_type = GL_INT;
            if (t["value"].is_integer())
            {
              f.value.int_ = int64_t(*t["value"].as_integer());
            }
            else
            {
              throw invalid_data("value not integer");
            }
          }
          else if (type == "uint")
          {
            f.atom_type = GL_UNSIGNED_INT;
            if (t["value"].is_integer())
            {
              f.value.uint_ = int64_t(*t["value"].as_integer());
            }
            else
            {
              throw invalid_data("value not integer");
            }
          }
          else if (type == "bool")
          {
            f.atom_type = GL_BOOL;
            if (t["value"].is_integer())
            {
              f.value.bool_ = int64_t(*t["value"].as_integer());
            }
            else
            {
              throw invalid_data("value not integer");
            }
          }
          else
          {
            throw invalid_data("invalid type");
          }
        }
        else
        {
          throw invalid_data("type not string");
        }
        result.push_back(f);
      }
      else
      {
        throw invalid_data("table expected");
      }
    }
    u->db = result;
    return true;
  }
  catch (const invalid_data &e)
  {
    std::cerr << e.what() << std::endl;
    return false;
  }
}
  
extern double uniforms_get_z(const struct uniforms *u, double fraction)
{
  if (std::isnan(fraction) || std::isinf(fraction))
  {
    return 0;
  }
  const ident zid = { "Z", 0, 0 };
  std::vector<fact> steps;
  for (const auto & f : u->db)
  {
    if (f.id == zid)
    {
      steps.push_back(f);
    }
  }
  if (steps.empty())
  {
    return 0;
  }
  facts_sort_fraction(steps);
  return facts_interpolate_ident(steps, fraction).value.float_;
}
