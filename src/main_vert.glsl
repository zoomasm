// zoomasm -- zoom video assembler
// (c) 2019,2020,2021,2022,2023 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-only

#extension GL_ARB_explicit_uniform_location : require

layout(location = 0) in vec2 coord;

out vec2 coord_0;

void main(void)
{
  coord_0 = coord;
  gl_Position = vec4(2.0 * coord - vec2(1.0), 0.0, 1.0);
}
