#pragma once
// zoomasm -- zoom video assembler
// (c) 2019,2020,2021,2022,2023 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-only

#include <filesystem>

struct audio;

extern struct audio *audio_new(void);
extern void audio_delete(struct audio *au);

extern void audio_nrt(struct audio *au);
extern bool audio_display_early(struct audio *au);
extern bool audio_display_late(struct audio *au);

enum audio_type
{
  audio_type_silence,
  audio_type_soundtrack
};

enum audio_type audio_get_type(const struct audio *au);

extern bool audio_set_silence(struct audio * au, double duration);

extern std::filesystem::path audio_get_soundtrack(const struct audio *au);
extern bool audio_set_soundtrack(struct audio *au, const std::filesystem::path &file);

extern double audio_get_duration(const struct audio *au);

extern double audio_get_time(const struct audio *au);
extern bool audio_set_time(struct audio *au, double time);

extern double audio_get_speed(const struct audio *au);
extern bool audio_set_speed(struct audio *au, double speed);

extern double audio_get_volume(const struct audio *au);
extern bool audio_set_volume(struct audio *au, double volume);

extern bool audio_get_mute(const struct audio *au);
extern bool audio_set_mute(struct audio *au, bool loop);

extern bool audio_get_loop(const struct audio *au);
extern bool audio_set_loop(struct audio *au, bool loop);

extern bool audio_get_playing(const struct audio *au);
extern bool audio_set_playing(struct audio *au, bool playing);
