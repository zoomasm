// zoomasm -- zoom video assembler
// (c) 2019,2020,2021,2022,2023 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-only

#include "imgui.h"
#include "imfilebrowser.h"

#include <filesystem>
#include <vector>
#include <future>
#include <thread>

#include <ImfNamespace.h>
#include <ImfInputFile.h>
#include <ImfOutputFile.h>
#include <ImfHeader.h>
#include <ImfChannelList.h>
#include <ImfChannelListAttribute.h>
#include <ImfFrameBuffer.h>
#include <ImathBox.h>

#include <GL/glew.h>

#include "resource.h"
#include "utility.h"

#include "input.h"

namespace IMF = OPENEXR_IMF_NAMESPACE;
using namespace IMF;
using namespace IMATH_NAMESPACE;

struct Frame
{
  // channels available in file
  std::set<std::string> fuint_channels, fhalf_channels, ffloat_channels;
  // channels loaded from file (above intersected with want_channels)
  std::set<std::string> luint_channels, lhalf_channels, lfloat_channels;
  ssize_t width;
  ssize_t height;
  uint32_t *uint_data;
  half *half_data;
  float *float_data;
  ssize_t gpu_vram_mb_used;
  ~Frame()
  {
    delete[] uint_data;
    delete[] half_data;
    delete[] float_data;
  };
};

bool are_compatible(const Frame *a, const Frame *b)
{
  return
    a && b &&
    a->fuint_channels == b->fuint_channels &&
    a->fhalf_channels == b->fhalf_channels &&
    a->ffloat_channels == b->ffloat_channels &&
    a->width == b->width &&
    a->height == b->height;
}

static Frame *read_frame(Frame *old_buffer, const std::filesystem::path &ifilename, const std::set<std::string> &want_channels)
{
  std::string argv0 = "zoomasm";
  Frame *new_buffer = nullptr;
  if (! old_buffer)
  {
    new_buffer = new Frame();
  }
  else
  {
    new_buffer = old_buffer;
  }
  // read image header
  InputFile ifile(ifilename.string().c_str());
  const Header &head = ifile.header();
  Box2i dw = head.dataWindow();
  ssize_t fwidth = dw.max.x - dw.min.x + 1;
  ssize_t fheight = dw.max.y - dw.min.y + 1;
  std::set<std::string> fuint_channels, fhalf_channels, ffloat_channels;
  for (Header::ConstIterator i = head.begin(); i != head.end(); ++i)
  {
    const Attribute *a = &i.attribute();
    const ChannelListAttribute *ta = dynamic_cast<const ChannelListAttribute *>(a);
    if (ta)
    {
      const ChannelList &cl = ta->value();
      for (ChannelList::ConstIterator j = cl.begin(); j != cl.end(); ++j)
      {
        if (j.channel().xSampling != 1)
        {
          std::cerr << argv0 << ": error: xSampling != 1\n";
          delete new_buffer;
          return nullptr;
        }
        if (j.channel().ySampling != 1)
        {
          std::cerr << argv0 << ": error: ySampling != 1\n";
          delete new_buffer;
          return nullptr;
        }
        switch (j.channel().type)
        {
          case IMF::UINT:
            fuint_channels.insert(j.name());
            break;
          case IMF::HALF:
            fhalf_channels.insert(j.name());
            break;
          case IMF::FLOAT:
            ffloat_channels.insert(j.name());
            break;
          default:
            std::cerr << argv0 << ": error: unknown channel type " << j.channel().type << "\n";
            delete new_buffer;
            return nullptr;
        }
      }
      // allocate data
      if (! old_buffer)
      {
        new_buffer->width = fwidth;
        new_buffer->height = fheight;
        new_buffer->fuint_channels = fuint_channels;
        new_buffer->fhalf_channels = fhalf_channels;
        new_buffer->ffloat_channels = ffloat_channels;
        new_buffer->luint_channels = intersection(fuint_channels, want_channels);
        new_buffer->lhalf_channels = intersection(fhalf_channels, want_channels);
        new_buffer->lfloat_channels = intersection(ffloat_channels, want_channels);
        if (new_buffer->luint_channels.size() > 0)
        {
          new_buffer->uint_data = new uint32_t[new_buffer->luint_channels.size() * new_buffer->width * new_buffer->height];
        }
        if (new_buffer->lhalf_channels.size() > 0)
        {
          new_buffer->half_data = new half[new_buffer->lhalf_channels.size() * new_buffer->width * new_buffer->height];
        }
        if (new_buffer->lfloat_channels.size() > 0)
        {
          new_buffer->float_data = new float[new_buffer->lfloat_channels.size() * new_buffer->width * new_buffer->height];
        }
      }
      else
      {
        if (old_buffer->width != fwidth ||
            old_buffer->height != fheight ||
            old_buffer->fuint_channels != fuint_channels ||
            old_buffer->fhalf_channels != fhalf_channels ||
            old_buffer->ffloat_channels != ffloat_channels
           )
        {
          std::cerr << argv0 << " : error: incompatible files\n";
          delete new_buffer;
          return nullptr;
        }
      }
    }
  }
  // OpenEXR throws an exception if there are no channels in the framebuffer...
  if (new_buffer->luint_channels.size() + new_buffer->lhalf_channels.size() + new_buffer->lfloat_channels.size() > 0)
  {
    // read image data
    FrameBuffer ifb;
    int k = 0;
    for (auto name : new_buffer->luint_channels)
    {
      ifb.insert
        ( name.c_str()
        , Slice
          ( IMF::UINT
          , (char *) (&new_buffer->uint_data[0] + k * new_buffer->width * new_buffer->height - dw.min.x - dw.min.y * new_buffer->width)
          , sizeof(new_buffer->uint_data[0])
          , sizeof(new_buffer->uint_data[0]) * new_buffer->width
          , 1, 1
          , 0
          )
       );
      ++k;
    }
    k = 0;
    for (auto name : new_buffer->lhalf_channels)
    {
      ifb.insert
        ( name.c_str()
        , Slice
          ( IMF::HALF
          , (char *) (&new_buffer->half_data[0] + k * new_buffer->width * new_buffer->height - dw.min.x - dw.min.y * new_buffer->width)
          , sizeof(new_buffer->half_data[0])
          , sizeof(new_buffer->half_data[0]) * new_buffer->width
          , 1, 1
          , 0
          )
        );
      ++k;
    }
    k = 0;
    for (auto name : new_buffer->lfloat_channels)
    {
      ifb.insert
        ( name.c_str()
        , Slice
          ( IMF::FLOAT
          , (char *) (&new_buffer->float_data[0] + k * new_buffer->width * new_buffer->height - dw.min.x - dw.min.y * new_buffer->width)
          , sizeof(new_buffer->float_data[0])
          , sizeof(new_buffer->float_data[0]) * new_buffer->width
          , 1, 1
          , 0
          )
       );
      ++k;
    }
    ifile.setFrameBuffer(ifb);
    ifile.readPixels(dw.min.y, dw.max.y);
  }
  return new_buffer;
}

static void upload_frame(Frame *new_buffer, Frame *old_buffer, std::set<std::string> &want_channels, double phase, bool force_load, ssize_t gpu_vram_mb_total, int NUMBER_OF_LAYERS)
{
  // upload to OpenGL
  ssize_t w = new_buffer->width;
  ssize_t h = new_buffer->height;
  ssize_t z = ((ssize_t(std::floor(phase)) % NUMBER_OF_LAYERS) + NUMBER_OF_LAYERS) % NUMBER_OF_LAYERS;
  if (! old_buffer)
  {
    GLuint *zero = (GLuint *) std::calloc(NUMBER_OF_LAYERS, sizeof(*zero));
    // estimate VRAM usage
    ssize_t gpu_vram_bytes_used = 0;
    gpu_vram_bytes_used += w * h * NUMBER_OF_LAYERS * 2 * new_buffer->lhalf_channels.size();
    gpu_vram_bytes_used += w * h * NUMBER_OF_LAYERS * 4 * new_buffer->luint_channels.size();
    gpu_vram_bytes_used += w * h * NUMBER_OF_LAYERS * 4 * new_buffer->lfloat_channels.size();
    // prevent overfilling GPU VRAM (video memory) with the raw data
    if (0 < gpu_vram_mb_total && gpu_vram_mb_total < gpu_vram_bytes_used / 1024 / 1024 && ! force_load)
    {
      want_channels = std::set<std::string>();
      new_buffer->lhalf_channels = std::set<std::string>();
      new_buffer->luint_channels = std::set<std::string>();
      new_buffer->lfloat_channels = std::set<std::string>();
      gpu_vram_bytes_used = 0;
    }
    // allocate textures
    glActiveTexture(GL_TEXTURE0 + TEXTURE_R);
    if (contains(new_buffer->lfloat_channels, "R"))
    {
      glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_R32F, w, h, NUMBER_OF_LAYERS, 0, GL_RED, GL_FLOAT, 0);
    }
    else
    if (contains(new_buffer->lhalf_channels, "R"))
    {
      glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_R16F, w, h, NUMBER_OF_LAYERS, 0, GL_RED, GL_HALF_FLOAT, 0);
    }
    else
    {
      glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_R16F, 1, 1, NUMBER_OF_LAYERS, 0, GL_RED, GL_HALF_FLOAT, zero);
    }
    glActiveTexture(GL_TEXTURE0 + TEXTURE_G);
    if (contains(new_buffer->lfloat_channels, "G"))
    {
      glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_R32F, w, h, NUMBER_OF_LAYERS, 0, GL_RED, GL_FLOAT, 0);
    }
    else
    if (contains(new_buffer->lhalf_channels, "G"))
    {
      glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_R16F, w, h, NUMBER_OF_LAYERS, 0, GL_RED, GL_HALF_FLOAT, 0);
    }
    else
    {
      glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_R16F, 1, 1, NUMBER_OF_LAYERS, 0, GL_RED, GL_HALF_FLOAT, zero);
    }
    glActiveTexture(GL_TEXTURE0 + TEXTURE_B);
    if (contains(new_buffer->lfloat_channels, "B"))
    {
      glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_R32F, w, h, NUMBER_OF_LAYERS, 0, GL_RED, GL_FLOAT, 0);
    }
    else
    if (contains(new_buffer->lhalf_channels, "B"))
    {
      glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_R16F, w, h, NUMBER_OF_LAYERS, 0, GL_RED, GL_HALF_FLOAT, 0);
    }
    else
    {
      glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_R16F, 1, 1, NUMBER_OF_LAYERS, 0, GL_RED, GL_HALF_FLOAT, zero);
    }
    glActiveTexture(GL_TEXTURE0 + TEXTURE_N0);
    if (contains(new_buffer->luint_channels, "N"))
    {
      glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_R32UI, w, h, NUMBER_OF_LAYERS, 0, GL_RED_INTEGER, GL_UNSIGNED_INT, 0);
    }
    else
    {
      glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_R32UI, 1, 1, NUMBER_OF_LAYERS, 0, GL_RED_INTEGER, GL_UNSIGNED_INT, zero);
    }
    glActiveTexture(GL_TEXTURE0 + TEXTURE_NF);
    if (contains(new_buffer->lfloat_channels, "NF"))
    {
      glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_R32F, w, h, NUMBER_OF_LAYERS, 0, GL_RED, GL_FLOAT, 0);
    }
    else
    {
      glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_R32F, 1, 1, NUMBER_OF_LAYERS, 0, GL_RED, GL_FLOAT, zero);
    }
    glActiveTexture(GL_TEXTURE0 + TEXTURE_T);
    if (contains(new_buffer->lfloat_channels, "T"))
    {
      glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_R32F, w, h, NUMBER_OF_LAYERS, 0, GL_RED, GL_FLOAT, 0);
    }
    else
    {
      glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_R32F, 1, 1, NUMBER_OF_LAYERS, 0, GL_RED, GL_FLOAT, zero);
    }
    glActiveTexture(GL_TEXTURE0 + TEXTURE_DEX);
    if (contains(new_buffer->lfloat_channels, "DEX"))
    {
      glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_R32F, w, h, NUMBER_OF_LAYERS, 0, GL_RED, GL_FLOAT, 0);
    }
    else
    {
      glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_R32F, 1, 1, NUMBER_OF_LAYERS, 0, GL_RED, GL_FLOAT, zero);
    }
    glActiveTexture(GL_TEXTURE0 + TEXTURE_DEY);
    if (contains(new_buffer->lfloat_channels, "DEY"))
    {
      glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_R32F, w, h, NUMBER_OF_LAYERS, 0, GL_RED, GL_FLOAT, 0);
    }
    else
    {
      glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_R32F, 1, 1, NUMBER_OF_LAYERS, 0, GL_RED, GL_FLOAT, zero);
    }
    new_buffer->gpu_vram_mb_used = gpu_vram_bytes_used / 1024 / 1024;
    std::free(zero);
  }
  // upload textures
  int k = 0;
  for (auto name : new_buffer->lhalf_channels)
  {
    if (name == "R")
    {
      glActiveTexture(GL_TEXTURE0 + TEXTURE_R);
      glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, z % NUMBER_OF_LAYERS, w, h, 1, GL_RED, GL_HALF_FLOAT, new_buffer->half_data + k * w * h);
    }
    else if (name == "G")
    {
      glActiveTexture(GL_TEXTURE0 + TEXTURE_G);
      glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, z % NUMBER_OF_LAYERS, w, h, 1, GL_RED, GL_HALF_FLOAT, new_buffer->half_data + k * w * h);
    }
    else if (name == "B")
    {
      glActiveTexture(GL_TEXTURE0 + TEXTURE_B);
      glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, z % NUMBER_OF_LAYERS, w, h, 1, GL_RED, GL_HALF_FLOAT, new_buffer->half_data + k * w * h);
    }
    ++k;
  }
  k = 0;
  for (auto name : new_buffer->luint_channels)
  {
    if (name == "N")
    {
      glActiveTexture(GL_TEXTURE0 + TEXTURE_N0);
      glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, z % NUMBER_OF_LAYERS, w, h, 1, GL_RED_INTEGER, GL_UNSIGNED_INT, new_buffer->uint_data + k * w * h);
    }
    ++k;
  }
  k = 0;
  for (auto name : new_buffer->lfloat_channels)
  {
    if (name == "R")
    {
      glActiveTexture(GL_TEXTURE0 + TEXTURE_R);
      glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, z % NUMBER_OF_LAYERS, w, h, 1, GL_RED, GL_FLOAT, new_buffer->float_data + k * w * h);
    }
    else if (name == "G")
    {
      glActiveTexture(GL_TEXTURE0 + TEXTURE_G);
      glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, z % NUMBER_OF_LAYERS, w, h, 1, GL_RED, GL_FLOAT, new_buffer->float_data + k * w * h);
    }
    else if (name == "B")
    {
      glActiveTexture(GL_TEXTURE0 + TEXTURE_B);
      glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, z % NUMBER_OF_LAYERS, w, h, 1, GL_RED, GL_FLOAT, new_buffer->float_data + k * w * h);
    }
    if (name == "NF")
    {
      glActiveTexture(GL_TEXTURE0 + TEXTURE_NF);
      glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, z % NUMBER_OF_LAYERS, w, h, 1, GL_RED, GL_FLOAT, new_buffer->float_data + k * w * h);
    }
    if (name == "T")
    {
      glActiveTexture(GL_TEXTURE0 + TEXTURE_T);
      glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, z % NUMBER_OF_LAYERS, w, h, 1, GL_RED, GL_FLOAT, new_buffer->float_data + k * w * h);
    }
    else if (name == "DEX")
    {
      glActiveTexture(GL_TEXTURE0 + TEXTURE_DEX);
      glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, z % NUMBER_OF_LAYERS, w, h, 1, GL_RED, GL_FLOAT, new_buffer->float_data + k * w * h);
    }
    else if (name == "DEY")
    {
      glActiveTexture(GL_TEXTURE0 + TEXTURE_DEY);
      glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, z % NUMBER_OF_LAYERS, w, h, 1, GL_RED, GL_FLOAT, new_buffer->float_data + k * w * h);
    }
    ++k;
  }
}

static Frame *read_frames(Frame *frameset, const std::vector<std::filesystem::path> &keyframe_filenames, double phase, int &keyframe_index, bool reverse_keyframes, std::set<std::string> &want_channels, bool force_load, ssize_t gpu_vram_mb_total, int NUMBER_OF_LAYERS)
{
  // big jump, reset frameset entirely
  int keyframe_count = keyframe_filenames.size();
  keyframe_index = std::floor(phase);
  for (int i = 0; i < NUMBER_OF_LAYERS; ++i)
  {
    int keyframe_number =
      ( reverse_keyframes
      ? keyframe_count - 1 - keyframe_index
      : keyframe_index
      );
    if (0 <= keyframe_number && keyframe_number < keyframe_count)
    {
      Frame *new_frameset = read_frame(frameset, keyframe_filenames[keyframe_number], want_channels);
      if (! new_frameset)
      {
        frameset = nullptr;
        return nullptr;
      }
      upload_frame(new_frameset, frameset, want_channels, keyframe_index, force_load, gpu_vram_mb_total, NUMBER_OF_LAYERS);
      frameset = new_frameset;
    }
    keyframe_index++;
  }
  return frameset;
}

struct speculated
{
  int keyframe_index;
  std::set<std::string> want_channels;
  Frame *new_frameset;
};

struct input
{
  bool reverse_keyframes;
  bool flip_keyframes;
  bool invert_zoom;
  GLuint Rt, Gt, Bt, Nt, NFt, Tt, DEXt, DEYt; // texture array objects
  Frame *frameset;
  double phase;
  int keyframe_index;
  std::vector<std::filesystem::path> keyframe_filenames;
  ImGui::FileBrowser keyframe_directory;
  std::filesystem::path keyframe_dir;
  ssize_t gpu_vram_mb_total;
  bool force_load;
  std::set<std::string> want_channels;
  int NUMBER_OF_LAYERS;
  std::future<speculated> future;
  input()
  : reverse_keyframes(true)
  , flip_keyframes(false)
  , invert_zoom(false)
  , Rt(0), Gt(0), Bt(0), Nt(0), NFt(0), Tt(0), DEXt(0), DEYt(0)
  , frameset(nullptr)
  , phase(0)
  , keyframe_index(0)
  , keyframe_filenames()
  , keyframe_directory(ImGuiFileBrowserFlags_SelectDirectory | ImGuiFileBrowserFlags_CloseOnEsc)
  , keyframe_dir()
  , gpu_vram_mb_total(0)
  , force_load(false)
  , want_channels()
  , NUMBER_OF_LAYERS(16)
  , future()
  {
  }
};

extern struct input *input_new(void)
{
  struct input *in = new input();
  if (! in)
  {
    return nullptr;
  }
  unsigned int nthreads = std::thread::hardware_concurrency();
  if (nthreads == 0)
  {
    nthreads = 1;
  }
  IMF::setGlobalThreadCount(nthreads);
  in->reverse_keyframes = true;
  in->flip_keyframes = false;
  in->invert_zoom = false;
  in->frameset = nullptr;
  std::vector<GLuint *> ts = { &in->Rt, &in->Gt, &in->Bt, &in->Nt, &in->NFt, &in->Tt, &in->DEXt, &in->DEYt };
  std::vector<int> us = { TEXTURE_R, TEXTURE_G, TEXTURE_B, TEXTURE_N0, TEXTURE_NF, TEXTURE_T, TEXTURE_DEX, TEXTURE_DEY };
  for (size_t i = 0; i < ts.size(); ++i)
  {
    glGenTextures(1, ts[i]);
    glActiveTexture(GL_TEXTURE0 + us[i]);
    glBindTexture(GL_TEXTURE_2D_ARRAY, *ts[i]);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  }
  GLint gpu_vram_kb = 0;
  if (gpu_vram_kb == 0)
  {
    // https://www.khronos.org/registry/OpenGL/extensions/ATI/ATI_meminfo.txt
    GLint gpu_texture_vram[4] = { 0, 0, 0, 0 };
    glGetIntegerv(GL_TEXTURE_FREE_MEMORY_ATI, &gpu_texture_vram[0]);
    gpu_vram_kb = gpu_texture_vram[0];
  }
  if (gpu_vram_kb == 0)
  {
    // http://developer.download.nvidia.com/opengl/specs/GL_NVX_gpu_memory_info.txt
    glGetIntegerv(GL_GPU_MEMORY_INFO_TOTAL_AVAILABLE_MEMORY_NVX, &gpu_vram_kb);
  }
  in->gpu_vram_mb_total = gpu_vram_kb / 1024;
  in->keyframe_index = -in->NUMBER_OF_LAYERS;
  in->keyframe_directory.SetTitle("EXR Directory");
  in->keyframe_directory.SetTypeFilters({ ".exr" });
  in->force_load = false;
  std::vector<std::string> known_channels = { "R", "G", "B", "N", "NF", "T", "DEX", "DEY" };
  in->want_channels.insert(known_channels.begin(), known_channels.end());
  return in;
}

extern void input_delete(struct input *in)
{
  if (! in)
  {
    return;
  }
  if (in->frameset)
  {
    delete in->frameset;
    in->frameset = nullptr;
  }
  std::vector<GLuint *> ts = { &in->Rt, &in->Gt, &in->Bt, &in->Nt, &in->NFt, &in->Tt, &in->DEXt, &in->DEYt };
  std::vector<int> us = { TEXTURE_R, TEXTURE_G, TEXTURE_B, TEXTURE_N0, TEXTURE_NF, TEXTURE_T, TEXTURE_DEX, TEXTURE_DEY };
  for (size_t i = 0; i < ts.size(); ++i)
  {
    glActiveTexture(GL_TEXTURE0 + us[i]);
    glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
    glDeleteTextures(1, ts[i]);
    *ts[i] = 0;
  }
  delete in;
}

extern int input_get_count(const struct input *in)
{
  if (in)
  {
    return in->keyframe_filenames.size();
  }
  return 0;
}

extern int input_get_width(const struct input *in)
{
  if (in && in->frameset)
  {
    return in->frameset->width;
  }
  return 0;
}

extern int input_get_height(const struct input *in)
{
  if (in && in->frameset)
  {
    return in->frameset->height;
  }
  return 0;
}

extern bool input_get_invert_zoom(const struct input *in)
{
  if (in)
  {
    return in->invert_zoom;
  }
  return false;
}

extern double input_get_invert_radius(const struct input *in)
{
  return in->NUMBER_OF_LAYERS - 1;
}

static void speculate(struct input *in, int keyframe_index)
{
  // speculatively load next keyframe
  int keyframe_count = input_get_count(in);
  int keyframe_number =
    ( in->reverse_keyframes
    ? keyframe_count - 1 - keyframe_index
    : keyframe_index
    );
  if (0 <= keyframe_number && keyframe_number < keyframe_count)
  {
    Frame *old_frameset = in->frameset;
    std::filesystem::path filename = in->keyframe_filenames[keyframe_number];
    std::set<string> want_channels = in->want_channels;
    in->future = std::async(std::launch::async, [keyframe_index, old_frameset, filename, want_channels]{
      return speculated{keyframe_index, want_channels, read_frame(old_frameset, filename, want_channels)};
    });
  }
}

extern int input_set_phase(struct input *in, double phase, double speed)
{
  changed |= (phase != in->phase);
  int keyframe_count = input_get_count(in);
  int NUMBER_OF_LAYERS = in->NUMBER_OF_LAYERS;
  if (keyframe_count)
  {
    int frameset_low = in->keyframe_index - NUMBER_OF_LAYERS; // inclusive
    int frameset_high = in->keyframe_index; // exclusive
    if (phase + NUMBER_OF_LAYERS < frameset_low || frameset_high <= phase - NUMBER_OF_LAYERS)
    {
      // big jump, read all
      if (in->future.valid())
      {
        speculated sp = in->future.get();
        in->frameset = sp.new_frameset;
        // ignore it, it probably wasn't for us
      }
      if (! (in->frameset = read_frames(in->frameset, in->keyframe_filenames, phase, in->keyframe_index, in->reverse_keyframes, in->want_channels, in->force_load, in->gpu_vram_mb_total, NUMBER_OF_LAYERS)))
      {
        return 1;
      }
      // speculatively read next frame
      if (speed > 0)
      {
        speculate(in, in->keyframe_index);
      }
      else if (speed < 0)
      {
        speculate(in, in->keyframe_index - 1 - NUMBER_OF_LAYERS);
      }
    }
    else if (frameset_high - NUMBER_OF_LAYERS <= phase)
    {
      // read frames, forwards motion
      bool uploaded = false;
      while (std::floor(phase) > in->keyframe_index - NUMBER_OF_LAYERS)
      {
        uploaded = false;
        if (in->future.valid())
        {
          speculated sp = in->future.get();
          in->frameset = sp.new_frameset;
          if (sp.keyframe_index == in->keyframe_index && sp.want_channels == in->want_channels)
          {
            upload_frame(sp.new_frameset, in->frameset, in->want_channels, in->keyframe_index, in->force_load, in->gpu_vram_mb_total, in->NUMBER_OF_LAYERS);
            in->frameset = sp.new_frameset;
            uploaded = true;
          }
          else
          {
            // ignore it, it wasn't for us
          }
        }
        if (! uploaded)
        {
          int keyframe_index = in->keyframe_index;
          int keyframe_number =
            ( in->reverse_keyframes
            ? keyframe_count - 1 - keyframe_index
            : keyframe_index
            );
          if (0 <= keyframe_number && keyframe_number < keyframe_count)
          {
            Frame *new_frameset = read_frame(in->frameset, in->keyframe_filenames[keyframe_number], in->want_channels);
            if (! new_frameset)
            {
              in->frameset = nullptr;
              return 1;
            }
            upload_frame(new_frameset, in->frameset, in->want_channels, in->keyframe_index, in->force_load, in->gpu_vram_mb_total, NUMBER_OF_LAYERS);
            in->frameset = new_frameset;
            uploaded = true;
          }
        }
        ++in->keyframe_index;
      }
      // speculatively load next frame
      if (uploaded)
      {
        if (speed > 0)
        {
          speculate(in, in->keyframe_index);
        }
      }
    }
    else if (phase < frameset_low)
    {
      bool uploaded = false;
      while (phase < in->keyframe_index - NUMBER_OF_LAYERS)
      {
        // read frame, backwards motion
        --in->keyframe_index;
        uploaded = false;
        if (in->future.valid())
        {
          speculated sp = in->future.get();
          in->frameset = sp.new_frameset;
          if (sp.keyframe_index == in->keyframe_index - NUMBER_OF_LAYERS && sp.want_channels == in->want_channels)
          {
            upload_frame(sp.new_frameset, in->frameset, in->want_channels, in->keyframe_index, in->force_load, in->gpu_vram_mb_total, NUMBER_OF_LAYERS);
            in->frameset = sp.new_frameset;
            uploaded = true;
          }
          else
          {
            // ignore it, it wasn't for us
          }
        }
        if (! uploaded)
        {
          int keyframe_index = in->keyframe_index - NUMBER_OF_LAYERS;
          int keyframe_number =
            ( in->reverse_keyframes
            ? keyframe_count - 1 - keyframe_index
            : keyframe_index
            );
          if (0 <= keyframe_number && keyframe_number < keyframe_count)
          {
            Frame *new_frameset = read_frame(in->frameset, in->keyframe_filenames[keyframe_number], in->want_channels);
            if (! new_frameset)
            {
              in->frameset = nullptr;
              return 1;
            }
            upload_frame(new_frameset, in->frameset, in->want_channels, in->keyframe_index, in->force_load, in->gpu_vram_mb_total, NUMBER_OF_LAYERS);
            in->frameset = new_frameset;
            uploaded = true;
          }
        }
      }
      // speculatively load next frame
      if (uploaded)
      {
        if (speed < 0)
        {
          speculate(in, in->keyframe_index - 1 - NUMBER_OF_LAYERS);
        }
      }
    }
    in->phase = phase;
    return 0;
  }
  return 1;
}

extern void input_reload(struct input *in)
{
  changed = true;
  if (in->future.valid())
  {
    speculated sp = in->future.get();
    in->frameset = sp.new_frameset;
    // ignore it, it probably wasn't for us
  }
  if (in->frameset)
  {
    delete in->frameset;
    in->frameset = nullptr;
  }
  if (input_get_count(in))
  {
    in->frameset = read_frames(in->frameset, in->keyframe_filenames, in->phase, in->keyframe_index, in->reverse_keyframes, in->want_channels, in->force_load, in->gpu_vram_mb_total, in->NUMBER_OF_LAYERS);
  }
}

extern void input_display_early(struct input *in)
{
  ImGui::Begin("Input", nullptr, ImGuiWindowFlags_HorizontalScrollbar);
  if (! input_get_count(in))
  {
    ImGui::PushStyleColor(ImGuiCol_Button, button_red);
  }
  if (ImGui::Button("Input"))
  {
    in->keyframe_directory.Open();
  }
  if (ImGui::IsItemHovered())
  {
    ImGui::SetTooltip("Select directory containing exponential strip keyframes (*.exr).");
  }
  if (! input_get_count(in))
  {
    ImGui::PopStyleColor(1);
  }
  ImGui::SameLine();
  ImGui::Text("%s", in->keyframe_dir.string().c_str());
  ImGui::Text("Found %d strips at %dx%d", input_get_count(in), input_get_width(in), input_get_height(in));
  ssize_t gpu_vram_mb_used = 0;
  std::vector<std::string> known_channels = { "R", "G", "B", "N", "NF", "T", "DEX", "DEY" };
  std::set<std::string> file_channels;
  std::set<std::string> want_channels_old = in->want_channels;
  if (in->frameset)
  {
    gpu_vram_mb_used = in->frameset->gpu_vram_mb_used;
    ImGui::Text("Channels:");
    file_channels.insert(in->frameset->fhalf_channels.begin(), in->frameset->fhalf_channels.end());
    file_channels.insert(in->frameset->fuint_channels.begin(), in->frameset->fuint_channels.end());
    file_channels.insert(in->frameset->ffloat_channels.begin(), in->frameset->ffloat_channels.end());
    for (auto channel : known_channels)
    {
      if (contains(file_channels, channel))
      {
        bool want = contains(in->want_channels, channel);
        ImGui::SameLine();
        int old_want = want;
        if (ImGui::Checkbox(channel.c_str(), &want))
        {
          changed |= (old_want != want);
          if (want)
          {
            in->want_channels.insert(channel);
          }
          else
          {
            in->want_channels.erase(channel);
          }
        }
        if (ImGui::IsItemHovered())
        {
          ImGui::SetTooltip("Use %s channel for colouring", channel.c_str());
        }
      }
    }
  }
  ImGui::Text("GPU VRAM (video memory) usage: %d / %d MB", (int) gpu_vram_mb_used, (int) in->gpu_vram_mb_total);
  if (in->gpu_vram_mb_total > 0)
  {
    ImGui::SameLine();
    bool old = in->force_load;
    if (ImGui::Checkbox("Override", &in->force_load))
    {
      changed |= (old != in->force_load);
    }
    if (ImGui::IsItemHovered())
    {
      ImGui::SetTooltip("Force loading channels even if detected video memory limit would be exceeded.");
    }
  }
  bool old = in->reverse_keyframes;
  if (ImGui::Checkbox("Reverse", &in->reverse_keyframes) || want_channels_old != in->want_channels)
  {
    changed |= (old != in->reverse_keyframes);
    if (input_get_count(in))
    {
      input_reload(in);
    }
  }
  if (ImGui::IsItemHovered())
  {
    ImGui::SetTooltip("Reverse order of keyframes (for zoom out sequence).");
  }
  ImGui::SameLine();
  old = in->flip_keyframes;
  if (ImGui::Checkbox("Flip", &in->flip_keyframes))
  {
    changed |= (old != in->flip_keyframes);
  }
  if (ImGui::IsItemHovered())
  {
    ImGui::SetTooltip("Flip keyframes vertically.");
  }
  ImGui::SameLine();
  old = in->invert_zoom;
  if (ImGui::Checkbox("Invert", &in->invert_zoom))
  {
    changed |= (old != in->invert_zoom);
  }
  if (ImGui::IsItemHovered())
  {
    ImGui::SetTooltip("Invert zoom animation (1/r) (for special effects).");
  }
  ImGui::SameLine();
  ImGui::PushItemWidth(100);
  int layers = input_get_layers(in);
  if (ImGui::InputInt("Layers", &layers))
  {
    input_set_layers(in, layers);
  }
  if (ImGui::IsItemHovered())
  {
    ImGui::SetTooltip("Number of keyframes to load into video memory\n(higher needs more but can reduce pole artifacts).");
  }
  ImGui::PopItemWidth();
  ImGui::End();
}

extern bool input_set_input(struct input *in, const std::filesystem::path &dir)
{
  // scan directory for *.exr files
  in->keyframe_dir = dir;
  in->keyframe_filenames.clear();
  if (! dir.empty())
  {
    try
    {
      for(auto& p : std::filesystem::directory_iterator(in->keyframe_dir))
      {
        if (ends_with(p.path().string(), ".exr"))
        {
          in->keyframe_filenames.push_back(p.path());
        }
      }
      std::sort(in->keyframe_filenames.begin(), in->keyframe_filenames.end());
    }
    catch (std::exception &e)
    {
      // probably directory does not exist or other error, give up
      in->keyframe_filenames.clear();
    }
  }
  input_reload(in);
  return in->frameset;
}

extern void input_display_late(struct input *in)
{
  in->keyframe_directory.Display();
  if (in->keyframe_directory.HasSelected())
  {
    input_set_input(in, in->keyframe_directory.GetSelected());
    in->keyframe_directory.ClearSelected();
  }
}

extern std::filesystem::path input_get_input(const struct input *in)
{
  return in->keyframe_dir;
}

extern bool input_set_wanted_channels(struct input *in, const std::set<std::string> &channels)
{
  if (in->want_channels != channels)
  {
    in->want_channels = channels;
    input_reload(in);
  }
  return true;
}

extern std::set<std::string> input_get_wanted_channels(const struct input *in)
{
  return in->want_channels;
}

extern bool input_set_vram_override(struct input *in, bool vram_override)
{
  if (in->force_load != vram_override)
  {
    in->force_load = vram_override;
    input_reload(in);
  }
  return true;
}

extern bool input_get_vram_override(const struct input *in)
{
  return in->force_load;
}

extern bool input_set_reverse(struct input *in, bool reverse)
{
  if (in->reverse_keyframes != reverse)
  {
    in->reverse_keyframes = reverse;
    input_reload(in);
  }
  return true;
}

extern bool input_get_reverse(const struct input *in)
{
  return in->reverse_keyframes;
}

extern bool input_set_flip(struct input *in, bool flip)
{
  changed |= (flip != in->flip_keyframes);
  in->flip_keyframes = flip;
  return true;
}

extern bool input_get_flip(const struct input *in)
{
  return in->flip_keyframes;
}

extern bool input_set_invert(struct input *in, bool invert)
{
  changed |= (invert != in->invert_zoom);
  in->invert_zoom = invert;
  return true;
}

extern bool input_get_invert(const struct input *in)
{
  return in->invert_zoom;
}

extern bool input_set_layers(struct input *in, int layers)
{
  if (in->NUMBER_OF_LAYERS != layers)
  {
    in->NUMBER_OF_LAYERS = layers;
    input_reload(in);
  }
  return true;
}

extern int input_get_layers(const struct input *in)
{
  return in->NUMBER_OF_LAYERS;
}
