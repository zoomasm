// zoomasm -- zoom video assembler
// (c) 2019,2020,2021,2022,2023 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-only

#include "colour.h"

#include "imgui.h"
#include "imfilebrowser.h"

#include <cmath>
#include <fstream>

#include <GL/glew.h>

#include "resource.h"
#include "utility.h"
#include "kfp.h"
#include "uniform.h"
#include "utility.h"

extern const char *main_frag;
extern const char *main_vert;

static const char* projection_presets[] = { "2D", "360" };

struct colour
{
  ImGui::FileBrowser shader_browser;
  std::filesystem::path shader_filename;
  std::string shader_source;
  std::filesystem::file_time_type shader_mtime;
  double mtime_check_time;
  bool watch;
  GLuint vao;
  GLuint vbo;
  GLuint p_blit;
  GLuint p_interpolate;
  int samples_per_pixel;
  float shutter_speed_percent;
  int projection;
  float angle;
  float rotation[3];
  char *version;
  bool need_genmipmap;
  kfp *kf_palette;
  GLuint texture_palette;
  colour()
  : shader_browser(ImGuiFileBrowserFlags_CloseOnEsc)
  , shader_filename()
  , shader_source()
  , shader_mtime()
  , mtime_check_time(0)
  , watch(false)
  , vao(0)
  , vbo(0)
  , p_blit(0)
  , p_interpolate(0)
  , samples_per_pixel(1)
  , shutter_speed_percent(50)
  , projection(projection_rectangular2d)
  , angle(0)
  , version(nullptr)
  , need_genmipmap(true)
  , kf_palette(nullptr)
  , texture_palette(0)
  {
    rotation[0] = 1;
    rotation[1] = 0;
    rotation[2] = 0;
  }
};

static ImGuiTextBuffer shader_log;

static bool debug_program(GLuint program) {
  GLint status = 0;
  glGetProgramiv(program, GL_LINK_STATUS, &status);
  GLint length = 0;
  glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
  char *info = 0;
  if (length) {
    info = (char *)malloc(length + 1);
    info[0] = 0;
    glGetProgramInfoLog(program, length, 0, info);
    info[length] = 0;
  }
  if ((info && info[0]) || ! status) {
    shader_log.appendf("\nlink info:\n%s", info ? info : "(no info log)\n");
  }
  if (info) {
    free(info);
  }
  return status;
}

static bool debug_shader(GLuint shader, GLenum type) {
  GLint status = 0;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
  GLint length = 0;
  glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
  char *info = 0;
  if (length) {
    info = (char *)malloc(length + 1);
    info[0] = 0;
    glGetShaderInfoLog(shader, length, 0, info);
    info[length] = 0;
  }
  if ((info && info[0]) || ! status) {
    const char *type_str = "unknown";
    switch (type) {
      case GL_VERTEX_SHADER: type_str = "vertex"; break;
      case GL_FRAGMENT_SHADER: type_str = "fragment"; break;
    }
    shader_log.appendf("\n%s info:\n%s", type_str, info ? info : "(no info log)\n");
  }
  if (info) {
    free(info);
  }
  return status;
}

static std::string read_file(std::ifstream& in) {
  std::ostringstream sstr;
  sstr << in.rdbuf();
  return sstr.str();
}

// shader
static const GLint u_N0 = 0;
static const GLint u_NF = 1;
static const GLint u_DEX = 2;
static const GLint u_DEY = 3;
static const GLint u_offset = 4;
static const GLint u_phase = 5;
static const GLint u_time = 6;
static const GLint u_samples = 7;
static const GLint u_projection = 8;
static const GLint u_aspect = 9;
static const GLint u_flip = 10;
static const GLint u_invert = 11;
static const GLint u_R = 12;
static const GLint u_G = 13;
static const GLint u_B = 14;
static const GLint u_T = 15;
static const GLint u_phase_range = 16;
static const GLint u_rotation = 17;
static const GLint u_angle = 18;
static const GLint u_NUMBER_OF_LAYERS = 19;
static const GLint u_N1 = 20;
static const GLint u_words = 21;
static const GLint u_IterationsBias = 22;

extern void colour_draw
  ( struct colour *clr
  , double phase_start
  , double phase_end
  , double time_start
  , double time_end
  , bool flip
  , double invert
  , double aspect_x
  , double aspect_y
  , int keyframe_count
  , int NUMBER_OF_LAYERS
  , int width
  , int height
  , const struct uniforms *un
  )
{
  glUseProgram(clr->p_interpolate);
  glUniform1i(u_R, TEXTURE_R);
  glUniform1i(u_G, TEXTURE_G);
  glUniform1i(u_B, TEXTURE_B);
  glUniform1i(u_N0, TEXTURE_N0);
  glUniform1i(u_N1, TEXTURE_N1);
  glUniform1i(u_NF, TEXTURE_NF);
  glUniform1i(u_T, TEXTURE_T);
  glUniform1i(u_DEX, TEXTURE_DEX);
  glUniform1i(u_DEY, TEXTURE_DEY);
  glUniform2f(u_aspect, aspect_x, aspect_y);
  int offset = std::floor(std::min(phase_start, phase_end));
  glUniform1i(u_offset, offset);
  glUniform2f(u_phase, phase_start - offset, phase_end - offset);
  glUniform2f(u_time, time_start, time_end);
  glUniform1i(u_samples, clr->samples_per_pixel);
  glUniform1i(u_flip, flip);
  glUniform1f(u_invert, invert);
  glUniform2i(u_phase_range, 0, keyframe_count);
  glUniform1i(u_projection, clr->projection);
  float x = clr->rotation[0];
  float y = clr->rotation[1];
  float z = clr->rotation[2];
  float t = clr->angle / 360.0 * 3.141592653589793; // angle/2
  float c = std::cos(t);
  float s = std::sin(t) / std::sqrt(x * x + y * y + z * z);
  if (std::isinf(s) || std::isnan(s))
  {
    c = 1;
    s = 0;
  }
  float q[4] = { c, s * x, s * y, s * z };
  glUniform4fv(u_rotation, 1, &q[0]);
  glUniform1f(u_angle, clr->angle / 360.0);
  glUniform1f(u_NUMBER_OF_LAYERS, NUMBER_OF_LAYERS);
  if (clr->kf_palette)
  {
    glActiveTexture(GL_TEXTURE0 + TEXTURE_PALETTE);
    kfp_set_uniforms(clr->p_interpolate, clr->kf_palette);
  }
  else
  {
    glUniform1i(glGetUniformLocation(clr->p_interpolate, "KFP_Palette"), TEXTURE_PALETTE);
  }
  glUniform2i(glGetUniformLocation(clr->p_interpolate, "ImageSize"), width, height);
  glUniform1f(glGetUniformLocation(clr->p_interpolate, "Internal_ZoomLog2"), phase_start);
  uniforms_upload(un);
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
  clr->need_genmipmap = true;
}

extern void colour_blit(struct colour *clr)
{
  glUseProgram(clr->p_blit);
  glActiveTexture(GL_TEXTURE0 + TEXTURE_OUTPUT_RGB);
  if (clr->need_genmipmap)
  {
    glGenerateMipmap(GL_TEXTURE_2D);
    clr->need_genmipmap = false;
  }
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

static GLuint vertex_fragment_shader(const char *version, const char *vert, const char *frag, const char *frag2 = nullptr)
{
  shader_log.clear();
  bool ok = true;
  GLuint program = glCreateProgram();
  {
    GLuint shader = glCreateShader(GL_VERTEX_SHADER);
    const char *sources[] = { version, "\n", vert };
    glShaderSource(shader, 3, sources, 0);
    glCompileShader(shader);
    ok &= debug_shader(shader, GL_VERTEX_SHADER);
    glAttachShader(program, shader);
    glDeleteShader(shader);
  }
  {
    GLuint shader = glCreateShader(GL_FRAGMENT_SHADER);
    const char *sources[] = { version, "\n", frag, frag2 };
    glShaderSource(shader, frag2 ? 4 : 3, sources, 0);
    glCompileShader(shader);
    ok &= debug_shader(shader, GL_FRAGMENT_SHADER);
    glAttachShader(program, shader);
    glDeleteShader(shader);
  }
  glLinkProgram(program);
  ok &= debug_program(program);
  if (! ok)
  {
    glDeleteProgram(program);
    program = 0;
  }
  return program;
}

extern struct colour *colour_new(const char *version)
{
  struct colour *clr = new colour();
  if (! clr)
  {
    return nullptr;
  }
  clr->watch = false;
  clr->shader_browser.SetTitle("Colouring Shader");
  clr->shader_browser.SetTypeFilters({ ".glsl", ".kfp", ".kfr" });
  // vertex data
  glGenVertexArrays(1, &clr->vao);
  glBindVertexArray(clr->vao);
  glGenBuffers(1, &clr->vbo);
  glBindBuffer(GL_ARRAY_BUFFER, clr->vbo);
  float vertices[] = { 0, 0, 1, 0, 0, 1, 1, 1 };
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(0);
  // compile shaders
  clr->version = mystrdup(version);
  clr->p_interpolate = 0;
  clr->p_blit = vertex_fragment_shader
    ( version
    , "\n"
      "out vec2 c;\n"
      "void main(void) {\n"
      "  switch (gl_VertexID) {\n"
      "  case 0:\n"
      "    c = vec2(0.0, 0.0);\n"
      "    gl_Position = vec4(-1.0, -1.0, 0.0, 1.0);\n"
      "    break;\n"
      "  case 1:\n"
      "    c = vec2(1.0, 0.0);\n"
      "    gl_Position = vec4( 1.0, -1.0, 0.0, 1.0);\n"
      "    break;\n"
      "  case 2:\n"
      "    c = vec2(0.0, 1.0);\n"
      "    gl_Position = vec4(-1.0,  1.0, 0.0, 1.0);\n"
      "    break;\n"
      "  case 3:\n"
      "    c = vec2(1.0, 1.0);\n"
      "    gl_Position = vec4( 1.0,  1.0, 0.0, 1.0);\n"
      "    break;\n"
      "  }\n"
      "}\n"
    , "\n"
      "in vec2 c;\n"
      "layout(location = 0, index = 0) out vec4 colour;\n"
      "uniform sampler2D t;\n"
      "void main(void) { colour = texture(t, vec2(c.x, 1.0 - c.y)); }\n"
    );
  if (! clr->p_blit)
  {
    // FIXME should display an error message in the GUI,
    // but nothing will work so not too bad to give up now...
    std::fprintf(stderr, "%s\n", shader_log.begin());
    std::abort();
  }
  glUseProgram(clr->p_blit);
  glUniform1i(glGetUniformLocation(clr->p_blit, "t"), TEXTURE_OUTPUT_RGB);
  glUseProgram(0);
  shader_log.clear();
  shader_log.append("No shader selected");
  clr->samples_per_pixel = 1;
  clr->shutter_speed_percent = 50;
  clr->mtime_check_time = 0;
  glGenTextures(1, &clr->texture_palette);
  glActiveTexture(GL_TEXTURE0 + TEXTURE_PALETTE);
  glBindTexture(GL_TEXTURE_1D, clr->texture_palette);
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  GLuint zero = 0;
  glTexImage1D(GL_TEXTURE_1D, 0, GL_RGB, 1, 0, GL_BGR, GL_UNSIGNED_BYTE, &zero);
  return clr;
}

extern void colour_delete(struct colour *clr)
{
  if (! clr)
  {
    return;
  }
  if (clr->kf_palette)
  {
    kfp_delete(clr->kf_palette);
    clr->kf_palette = nullptr;
  }
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  if (clr->vbo)
  {
    glDeleteBuffers(1, &clr->vbo);
    clr->vbo = 0;
  }
  glBindVertexArray(0);
  if (clr->vao)
  {
    glDeleteVertexArrays(1, &clr->vao);
    clr->vao = 0;
  }
  glUseProgram(0);
  if (clr->p_blit)
  {
    glDeleteProgram(clr->p_blit);
    clr->p_blit = 0;
  }
  if (clr->p_interpolate)
  {
    glDeleteProgram(clr->p_interpolate);
    clr->p_interpolate = 0;
  }
  if (clr->version)
  {
    std::free(clr->version);
    clr->version = nullptr;
  }
  if (clr->texture_palette)
  {
    glDeleteTextures(1, &clr->texture_palette);
    clr->texture_palette = 0;
  }
  delete clr;
}

extern void colour_display_early(struct colour *clr)
{
  ImGui::Begin("Colour", nullptr, ImGuiWindowFlags_HorizontalScrollbar);
  if (! colour_get_compiled(clr))
  {
    ImGui::PushStyleColor(ImGuiCol_Button, button_red);
  }
  if (ImGui::Button("Colour"))
  {
    clr->shader_browser.Open();
  }
  if (ImGui::IsItemHovered())
  {
    ImGui::SetTooltip("Select GLSL shader fragment for colouring raw data.");
  }
  if (! colour_get_compiled(clr))
  {
    ImGui::PopStyleColor(1);
  }
  ImGui::SameLine();
  ImGui::Checkbox("Watch", &clr->watch);
  if (ImGui::IsItemHovered())
  {
    ImGui::SetTooltip("Watch file for changes and automatically reload.");
  }
  ImGui::SameLine();
  ImGui::Text("%s", clr->shader_filename.string().c_str());
  if (clr->shader_filename.empty())
  {
    ImGui::Text("No shader selected");
  }
  else
  {
    ImGui::Text("Shader %s compiled", clr->p_interpolate ? "is" : "is NOT");
    if (*shader_log.begin())
    {
      ImGui::TextUnformatted(shader_log.begin(), shader_log.end());
    }
  }
  ImGui::PushItemWidth(100);
  int old_samples_per_pixel = clr->samples_per_pixel;
  if (ImGui::InputInt("Samples per pixel", &clr->samples_per_pixel))
  {
    changed |= (old_samples_per_pixel != clr->samples_per_pixel);
  }
  if (ImGui::IsItemHovered())
  {
    ImGui::SetTooltip("Higher values increase quality but are harder on the GPU.");
  }
  clr->samples_per_pixel = std::max(1, clr->samples_per_pixel);
  float old_shutter_speed_percent = clr->shutter_speed_percent;
  if (ImGui::InputFloat("Shutter speed (percent)", &clr->shutter_speed_percent, 0, 0, "%.1f%%"))
  {
    changed |= (old_shutter_speed_percent != clr->shutter_speed_percent);
  }
  if (ImGui::IsItemHovered())
  {
    ImGui::SetTooltip("Percentage of frame time that the camera shutter is open for, for motion blur.");
  }
  clr->shutter_speed_percent = std::min(std::max(0.0f, clr->shutter_speed_percent), 100.0f);
  int old_projection = clr->projection;
  if (ImGui::Combo("Projection", &clr->projection, projection_presets, IM_ARRAYSIZE(projection_presets)))
  {
    changed |= (old_projection != clr->projection);
  }
  if (ImGui::IsItemHovered())
  {
    ImGui::SetTooltip("Choose projection for mapping keyframes to images.");
  }
  ImGui::PopItemWidth();
  ImGui::PushItemWidth(300);
  float old_angle = clr->angle;
  if (ImGui::SliderFloat("Angle", &clr->angle, -360.0f, 360.0f, "%.3f"))
  {
    changed |= (old_angle != clr->angle);
  }
  if (ImGui::IsItemHovered())
  {
    ImGui::SetTooltip("Rotation angle.");
  }
  if (clr->projection == projection_equirectangular360)
  {
    float old_rotation[3] = { clr->rotation[0], clr->rotation[1], clr->rotation[2] };
    if (ImGui::SliderFloat3("Axis", &clr->rotation[0], -1.0f, 1.0f, "%.6f"))
    {
      changed |= (old_rotation[0] != clr->rotation[0]);
      changed |= (old_rotation[1] != clr->rotation[1]);
      changed |= (old_rotation[2] != clr->rotation[2]);
      colour_set_rotation_axis(clr, clr->rotation[0], clr->rotation[1], clr->rotation[2]);
    }
    if (ImGui::IsItemHovered())
    {
      ImGui::SetTooltip("Rotation axis.");
    }
  }
  ImGui::PopItemWidth();
  ImGui::End();
}

extern bool colour_display_late(struct colour *clr)
{
  bool reload = false;
  clr->shader_browser.Display();
  if (clr->shader_browser.HasSelected())
  {
    clr->shader_filename = clr->shader_browser.GetSelected();
    clr->shader_browser.ClearSelected();
    if (! clr->shader_filename.empty())
    {
      try
      {
        clr->mtime_check_time = get_time();
        clr->shader_mtime = std::filesystem::last_write_time(clr->shader_filename);
        reload = true;
      }
      catch (const std::exception &e)
      {
        // probably file vanished or permission error
        shader_log.clear();
        shader_log.append("ERROR: %s\n", e.what());
      }
    }
  }
  return reload;
}

extern bool colour_compile_shader(struct colour *clr, bool reload)
{
  bool recompiled = false;
  // FIXME rate limiting?
  if (! clr->shader_filename.empty() && ! reload && clr->watch)
  {
    double check_time = get_time();
    double update_hz = 10;
    if (clr->mtime_check_time + 1.0 / update_hz <= check_time)
    {
      clr->mtime_check_time = check_time;
      try
      {
        std::filesystem::file_time_type then = clr->shader_mtime;
        std::filesystem::file_time_type now = std::filesystem::last_write_time(clr->shader_filename);
        if (now > then)
        {
          clr->shader_mtime = now;
          reload = true;
        }
      }
      catch (const std::exception &e)
      {
        // probably watched file is missing, maybe temporarily, ignore
        (void) e;
      }
    }
  }
  if (reload)
  {
    try
    {
      std::ifstream shader_file(clr->shader_filename, std::ios_base::binary);
      if (shader_file.is_open())
      {
        clr->shader_source = read_file(shader_file);
        if (clr->kf_palette)
        {
          kfp_delete(clr->kf_palette);
          clr->kf_palette = nullptr;
        }
        if (clr->shader_filename.extension() == ".kfp" || clr->shader_filename.extension() == ".kfr")
        {
          clr->kf_palette = kfp_new(clr->shader_source, 1024); // FIXME hardcoded
        }
        GLuint p_interpolate_new = vertex_fragment_shader
          ( clr->version
          , main_vert
          , main_frag
          , clr->kf_palette ? kfp_get_glsl(clr->kf_palette).c_str() : clr->shader_source.c_str()
          );
        if (p_interpolate_new)
        {
          if (clr->p_interpolate)
          {
            glDeleteProgram(clr->p_interpolate);
            clr->p_interpolate = 0;
          }
          clr->p_interpolate = p_interpolate_new;
          changed = true;
          recompiled = true;
        }
        else
        {
          if (clr->p_interpolate)
          {
            shader_log.append("WARNING: using previously compiled shader\n");
          }
        }
      }
      else
      {
        shader_log.clear();
        shader_log.append("ERROR: could not open shader file\n");
        if (clr->p_interpolate)
        {
          shader_log.append("WARNING: using previously compiled shader\n");
        }
      }
    }
    catch (const std::exception &e)
    {
      shader_log.clear();
      shader_log.append("ERROR: %s\n", e.what());
    }
  }
  return recompiled;
}

extern bool colour_get_compiled(const struct colour *clr)
{
  return clr->p_interpolate;
}

extern std::filesystem::path colour_get_colour(const struct colour *clr)
{
  return clr->shader_filename;
}

extern bool colour_set_colour(struct colour *clr, const std::filesystem::path &file)
{
  changed = true;
  clr->shader_filename = file;
  colour_compile_shader(clr, true);
  return colour_get_compiled(clr);
}

extern bool colour_get_watch(const struct colour *clr)
{
  return clr->watch;
}

extern bool colour_set_watch(struct colour *clr, bool watch)
{
  clr->watch = watch;
  return true;
}

extern int colour_get_samples(const struct colour *clr)
{
  return clr->samples_per_pixel;
}

extern bool colour_set_samples(struct colour *clr, int samples)
{
  changed |= (samples != clr->samples_per_pixel);
  clr->samples_per_pixel = std::max(1, samples);
  return true;
}

extern double colour_get_shutter(const struct colour *clr)
{
  return clr->shutter_speed_percent / 100.0;
}

extern bool colour_set_shutter(struct colour *clr, double shutter)
{
  changed |= (100.0 * shutter != clr->shutter_speed_percent);
  clr->shutter_speed_percent = std::min(std::max(0.0, shutter * 100.0), 100.0);
  return true;
}

extern int colour_get_projection(const struct colour *clr)
{
  return clr->projection;
}

extern bool colour_set_projection(struct colour *clr, int projection)
{
  changed |= (clr->projection != !!projection);
  clr->projection = !!projection;
  return true;
}

extern void colour_get_rotation_axis(const struct colour *clr, double *x, double *y, double *z)
{
  if (x) *x = clr->rotation[0];
  if (y) *y = clr->rotation[1];
  if (z) *z = clr->rotation[2];
}

extern bool colour_set_rotation_axis(struct colour *clr, double x, double y, double z)
{
  changed |= (clr->rotation[0] != x || clr->rotation[1] != y || clr->rotation[2] != z);
  clr->rotation[0] = x;
  clr->rotation[1] = y;
  clr->rotation[2] = z;
  return true;
}

extern double colour_get_rotation_angle(const struct colour *clr)
{
  return clr->angle;
}

extern bool colour_set_rotation_angle(struct colour *clr, double angle)
{
  changed |= (clr->angle != angle);
  clr->angle = angle;
  return true;
}

extern GLuint colour_get_program(const struct colour *clr)
{
  return clr->p_interpolate;
}
