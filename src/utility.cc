// zoomasm -- zoom video assembler
// (c) 2019,2020,2021,2022,2023 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-only

#include "utility.h"

#include <algorithm>
#include <cmath>
#include <cstring>
#include <cstdlib>
#include <iomanip>

#include <SDL2/SDL.h>

#if __cplusplus >= 202002L
#define CPlusPlus20
#endif

extern bool starts_with(const std::string &haystack, const std::string &needle)
{
#ifdef CPlusPlus20
  return haystack.starts_with(needle);
#else
  // https://stackoverflow.com/a/40441240
  return haystack.rfind(needle, 0) == 0;
#endif
}

extern bool ends_with(const std::string &haystack, const std::string &needle)
{
#ifdef CPlusPlus20
  return haystack.ends_with(needle);
#else
  // https://stackoverflow.com/a/42844629
  return haystack.size() >= needle.size() && 0 == haystack.compare(haystack.size() - needle.size(), needle.size(), needle);
#endif
}

extern bool contains(const std::set<std::string> &haystack, const std::string &needle)
{
#ifdef CPlusPlus20
  return haystack.contains(needle);
#else
  // https://stackoverflow.com/a/1701083
  return haystack.find(needle) != haystack.end();
#endif
}

extern std::set<std::string> intersection(const std::set<std::string> &s1, const std::set<std::string> &s2)
{
  // https://stackoverflow.com/a/13448094
  std::set<std::string> s3;
  std::set_intersection(s1.begin(), s1.end(), s2.begin(), s2.end(), std::inserter(s3,s3.begin()));
  return s3;
}

extern char *mystrdup(const char* s)
{
  size_t slen = std::strlen(s);
  char* result = (char *) std::malloc(slen + 1);
  if (result == NULL)
  {
    return NULL;
  }
  std::memcpy(result, s, slen + 1);
  return result;
}

extern std::istream& getline(std::istream& is, std::string& t)
{
  // https://stackoverflow.com/a/6089413

    t.clear();

    // The characters in the stream are read one-by-one using a std::streambuf.
    // That is faster than reading them one-by-one using the std::istream.
    // Code that uses streambuf this way must be guarded by a sentry object.
    // The sentry object performs various tasks,
    // such as thread synchronization and updating the stream state.

    std::istream::sentry se(is, true);
    std::streambuf* sb = is.rdbuf();

    for(;;) {
        int c = sb->sbumpc();
        switch (c) {
        case '\n':
            return is;
        case '\r':
            if(sb->sgetc() == '\n')
                sb->sbumpc();
            return is;
        case std::streambuf::traits_type::eof():
            // Also handle the case when the last line has no line ending
            if(t.empty())
                is.setstate(std::ios::eofbit);
            return is;
        default:
            t += (char)c;
        }
    }
}

extern void seconds_to_timespec(double time, int timespec[4])
{
  int hours = std::floor(time / 60.0 / 60.0);
  int minutes = std::floor((time - 60.0 * 60.0 * hours) / 60.0);
  int seconds = std::floor(time - 60.0 * 60.0 * hours - 60.0 * minutes);
  int milliseconds = std::round(1000.0 * (time - 60.0 * 60.0 * hours - 60.0 * minutes - seconds));
  timespec[0] = hours;
  timespec[1] = minutes;
  timespec[2] = seconds;
  timespec[3] = milliseconds;
}

extern double timespec_to_seconds(const int timespec[4])
{
  int hours = timespec[0];
  int minutes = timespec[1];
  int seconds = timespec[2];
  int milliseconds = timespec[3];
  return ((((hours * 60.0) + minutes) * 60.0) + seconds) + milliseconds / 1000.0;
}

extern std::string format_timespec(const int timespec[4])
{
  std::ostringstream s;
  if (timespec[0])
  {
    s <<                                      timespec[0] << ':'
      << std::setw(2) << std::setfill('0') << timespec[1] << ':'
      << std::setw(2) << std::setfill('0') << timespec[2] << '.'
      << std::setw(3) << std::setfill('0') << timespec[3];
  }
  else if (timespec[1])
  {
    s <<                                      timespec[1] << ':'
      << std::setw(2) << std::setfill('0') << timespec[2] << '.'
      << std::setw(3) << std::setfill('0') << timespec[3];
  }
  else
  {
    s <<                                      timespec[2] << '.'
      << std::setw(3) << std::setfill('0') << timespec[3];
  }
  return s.str();
}

extern std::string format_time(double time)
{
  int timespec[4] = { 0, 0, 0, 0 };
  seconds_to_timespec(time, timespec);
  return format_timespec(timespec);
}

extern double get_time()
{
  // Debian Bullseye's SDL2 is not new enough
  // FIXME: this will wrap after ~49 days
  return SDL_GetTicks/*64*/() / 1000.0;
}
