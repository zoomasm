# zoomasm -- zoom video assembler
# (c) 2019,2020,2021,2022,2023 Claude Heiland-Allen
# SPDX-License-Identifier: AGPL-3.0-only

VERSION ?= $(shell test -d .git && git describe --always --dirty=+ || (cat VERSION.txt | head -n 1))
DATE ?= $(shell test -d .git && date --iso || (cat VERSION.txt | tail -n+1 | head -n 1))

SYSTEM ?= posix
include $(SYSTEM).mk

SOURCE := $(shell cat INDEX.txt)

# blank line above

EXTRA_LIBS ?=

default: zoomasm$(exe)

help:
	@echo "For POSIX systems: make"
	@echo "To cross-compile for Windows 32bit: make SYSTEM=windows-i686"
	@echo "To cross-compile for Windows 64bit: make SYSTEM=windows-x86_64"

clean:
	-rm -r zoomasm-source.7z build

VERSION.txt:
	echo "$(VERSION)" > VERSION.txt
	echo "$(DATE)" >> VERSION.txt
	touch -c -d '@0' VERSION.txt

zoomasm$(exe): build/$(target)/zoomasm$(exe)
	cp -avf $< $@

INCLUDE = \
-I../imgui \
-I../imgui/backends \
-I../imgui/misc/cpp \
-I../imgui-filebrowser \
-I../tomlplusplus \

OBJECTS = \
build/$(target)/audio.o \
build/$(target)/colour.o \
build/$(target)/ffmpeg.o \
build/$(target)/input.o \
build/$(target)/kfp.o \
build/$(target)/main.o \
build/$(target)/main_frag.glsl.o \
build/$(target)/main_vert.glsl.o \
build/$(target)/output.o \
build/$(target)/process.o \
build/$(target)/session.o \
build/$(target)/uniform.o \
build/$(target)/utility.o \
build/$(target)/imgui.o \
build/$(target)/imgui_draw.o \
build/$(target)/imgui_tables.o \
build/$(target)/imgui_widgets.o \
build/$(target)/imgui_impl_sdl2.o \
build/$(target)/imgui_impl_opengl3.o \
build/$(target)/imgui_stdlib.o \
$(glew) \

# blank line above

PRESETS = \
presets/binary-decomposition.glsl \
presets/cycling-waves.glsl \
presets/multiwave.glsl \
presets/rainbow-fringe.glsl \
presets/rgb-passthrough.glsl \
presets/text-overlay.glsl \

# blank line above

EMBEDSOURCE = -Wl,--format=binary -Wl,zoomasm-source.7z -Wl,--format=default

VERSIONS = \
-DZOOMASM_VERSION_STRING="\"$(VERSION)\"" \
-DIMGUI_GIT_VERSION_STRING="\"$(shell cd ../imgui && git describe --tags --always)\"" \
-DIMGUI_FILEBROWSER_GIT_VERSION_STRING="\"$(shell cd ../imgui-filebrowser && git describe --tags --always)\"" \
-DTOMLPLUSPLUS_GIT_VERSION_STRING="\"$(shell cd ../tomlplusplus && git describe --tags --always)\"" \
-DGLEW_VERSION_STRING="\"2.2.0\"" \

compile = $(cxx) -std=c++17 -Wall -Wextra -pedantic -Wno-deprecated -O3 $(INCLUDE) $(VERSIONS) -c -o

link = $(cxx) -std=c++17 -Wall -Wextra -pedantic -Wno-deprecated -O3 $(EMBEDSOURCE) -o

zoomasm-source.7z: $(SOURCE) build/common/stamp
	-rm -rf "zoomasm-source.7z" "build/common/zoomasm-$(VERSION)"
	mkdir "build/common/zoomasm-$(VERSION)"
	cat INDEX.txt |	cpio -pdv "build/common/zoomasm-$(VERSION)"
	( cd build/common && 7zr a -t7z -m0=lzma -mx=9 -mfb=64 -md=32m -ms=on "../../zoomasm-source.7z" "zoomasm-$(VERSION)/" )

build/common/stamp:
	mkdir -p build/common && touch build/common/stamp

build/common/main_frag.glsl.c: src/main_frag.glsl src/s2c.sh build/common/stamp
	bash src/s2c.sh main_frag < $< > $@

build/common/main_vert.glsl.c: src/main_vert.glsl src/s2c.sh build/common/stamp
	bash src/s2c.sh main_vert < $< > $@

build/$(target)/stamp:
	mkdir -p build/$(target) && touch build/$(target)/stamp

build/$(target)/audio.o: src/audio.cc src/audio.h src/resource.h src/ringbuffer.h ../imgui/imgui.h build/$(target)/stamp
	$(compile) $@ $<

build/$(target)/colour.o: src/colour.cc src/colour.h src/kfp.h src/resource.h src/uniform.h src/utility.h ../imgui/imgui.h ../imgui-filebrowser/imfilebrowser.h build/common/main_frag.glsl.c build/common/main_vert.glsl.c build/$(target)/stamp
	$(compile) $@ $<

build/$(target)/ffmpeg.o: src/ffmpeg.cc src/ffmpeg.h src/process.h src/resource.h src/utility.h ../imgui/imgui.h build/$(target)/stamp
	$(compile) $@ $<

build/$(target)/input.o: src/input.cc src/input.h src/resource.h src/utility.h ../imgui/imgui.h ../imgui-filebrowser/imfilebrowser.h build/$(target)/stamp
	$(compile) $@ $<

build/$(target)/kfp.o: src/kfp.cc src/kfp.h src/resource.h build/$(target)/stamp
	$(compile) $@ $<

build/$(target)/main.o: src/main.cc src/audio.h src/colour.h src/ffmpeg.h src/input.h src/output.h src/resource.h src/session.h src/uniform.h src/utility.h ../imgui/imgui.h ../imgui/backends/imgui_impl_sdl2.h ../imgui/backends/imgui_impl_opengl3.h ../imgui-filebrowser/imfilebrowser.h build/$(target)/stamp
	$(compile) $@ $<

build/$(target)/main_frag.glsl.o: build/common/main_frag.glsl.c build/$(target)/stamp
	$(compile) $@ $<

build/$(target)/main_vert.glsl.o: build/common/main_vert.glsl.c build/$(target)/stamp
	$(compile) $@ $<

build/$(target)/output.o: src/output.cc src/output.h src/ffmpeg.h src/resource.h src/utility.h ../imgui/imgui.h ../imgui/misc/cpp/imgui_stdlib.h ../imgui-filebrowser/imfilebrowser.h build/$(target)/stamp
	$(compile) $@ $<

build/$(target)/process.o: src/process_$(os).cc src/process.h src/utility.h build/$(target)/stamp
	$(compile) $@ $<

build/$(target)/session.o: src/session.cc src/session.h src/audio.h src/colour.h src/ffmpeg.h src/input.h src/output.h src/resource.h src/uniform.h ../imgui/imgui.h ../imgui-filebrowser/imfilebrowser.h ../tomlplusplus/toml.hpp build/$(target)/stamp
	$(compile) $@ $<

build/$(target)/uniform.o: src/uniform.cc src/uniform.h src/audio.h ../imgui/imgui.h ../imgui-filebrowser/imfilebrowser.h ../tomlplusplus/toml.hpp build/$(target)/stamp
	$(compile) $@ $<

build/$(target)/utility.o: src/utility.cc src/utility.h build/$(target)/stamp
	$(compile) $@ $<

build/$(target)/imgui.o: ../imgui/imgui.cpp ../imgui/imgui.h ../imgui/imgui_internal.h build/$(target)/stamp
	$(compile) $@ $<

build/$(target)/imgui_draw.o: ../imgui/imgui_draw.cpp ../imgui/imgui.h ../imgui/imgui_internal.h ../imgui/imstb_rectpack.h ../imgui/imstb_truetype.h ../imgui/misc/freetype/imgui_freetype.h build/$(target)/stamp
	$(compile) $@ $<

build/$(target)/imgui_tables.o: ../imgui/imgui_tables.cpp ../imgui/imgui.h ../imgui/imgui_internal.h build/$(target)/stamp
	$(compile) $@ $<

build/$(target)/imgui_widgets.o: ../imgui/imgui_widgets.cpp ../imgui/imgui.h ../imgui/imgui_internal.h ../imgui/imstb_textedit.h build/$(target)/stamp
	$(compile) $@ $<

build/$(target)/imgui_impl_sdl2.o: ../imgui/backends/imgui_impl_sdl2.cpp ../imgui/backends/imgui_impl_sdl2.h ../imgui/imgui.h ../imgui/imgui_internal.h build/$(target)/stamp
	$(compile) $@ $<

build/$(target)/imgui_impl_opengl3.o: ../imgui/backends/imgui_impl_opengl3.cpp ../imgui/backends/imgui_impl_opengl3.h ../imgui/backends/imgui_impl_opengl3_loader.h ../imgui/imgui.h ../imgui/imgui_internal.h build/$(target)/stamp
	$(compile) $@ $<

build/$(target)/imgui_stdlib.o: ../imgui/misc/cpp/imgui_stdlib.cpp ../imgui/misc/cpp/imgui_stdlib.h ../imgui/imgui.h ../imgui/imgui_internal.h build/$(target)/stamp
	$(compile) $@ $<

build/$(target)/glew.o: src/glew.cc build/$(target)/stamp
	$(compile) $@ $<

build/$(target)/zoomasm$(exe): $(OBJECTS) $(glew) | zoomasm-source.7z
	$(link) $@ $^ $(libs)

.SUFFIXES:
.PHONY: default help clean VERSION.txt
