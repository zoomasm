# zoomasm -- zoom video assembler
# (c) 2019,2020,2021,2022,2023 Claude Heiland-Allen
# SPDX-License-Identifier: AGPL-3.0-only

prefix ?=
target = posix
os = posix
exe =
cxx = g++ -fopenmp `pkg-config --cflags glew sdl2 OpenEXR zlib fftw3`
libs = -fopenmp `pkg-config --libs glew sdl2 OpenEXR zlib fftw3` -pthread -ldl -lm
glew =
