# zoomasm -- zoom video assembler
# (c) 2019,2020,2021,2022,2023 Claude Heiland-Allen
# SPDX-License-Identifier: AGPL-3.0-only

prefix ?= $(HOME)/opt/windows/posix/i686
sdl2_CFLAGS := $(shell $(prefix)/bin/sdl2-config --cflags)
sdl2_LDFLAGS := $(shell $(prefix)/bin/sdl2-config --static-libs)
target = windows-i686
os = win32
exe = .i686.exe
cxx = i686-w64-mingw32-g++-posix -Wno-cast-function-type -D__USE_MINGW_ANSI_STDIO=1 -D_FILE_OFFSET_BITS=64 -DGLEW_STATIC -DDEFAULT_LINKAGE_HAS_UNDERSCORE=1 -I$(prefix)/include -I$(prefix)/include/Imath -I$(prefix)/include/OpenEXR $(sdl2_CFLAGS)
libs = -static-libgcc -static-libstdc++ -Wl,-subsystem,windows -Wl,--stack,67108864 -L$(prefix)/lib -lfftw3 -lmingw32 -lOpenEXR-3_1 -lOpenEXRCore-3_1 -lOpenEXRUtil-3_1 -lIex-3_1 -lImath-3_1 -lIlmThread-3_1 -lz -lopengl32 -lgdi32 -limm32 -Wl,-Bstatic -lpthread -Wl,-Bdynamic $(sdl2_LDFLAGS)
glew = build/$(target)/glew.o
